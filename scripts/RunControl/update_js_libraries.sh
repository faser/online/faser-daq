#!/bin/bash

JSDIR="daqControl/static/js/libs"
CSSDIR="daqControl/static/css/libs"

mkdir -p daqControl/static/js/libs
mkdir -p daqControl/static/css/libs

declare -a js=(
    "https://cdnjs.cloudflare.com/ajax/libs/vue/3.4.21/vue.global.js"
    "https://cdnjs.cloudflare.com/ajax/libs/vue/3.4.21/vue.global.prod.min.js"
    "https://cdnjs.cloudflare.com/ajax/libs/vuetify/3.5.10/vuetify.js"
    "https://cdnjs.cloudflare.com/ajax/libs/vuetify/3.5.10/vuetify.min.js"
    "https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.7.2/socket.io.js"
    "https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.7.2/socket.io.min.js"
    "https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"
    "https://cdnjs.cloudflare.com/ajax/libs/plotly.js/2.30.1/plotly-cartesian.min.js"
)

declare -a css=(
    "https://cdnjs.cloudflare.com/ajax/libs/vuetify/3.5.10/vuetify.css"
    "https://cdnjs.cloudflare.com/ajax/libs/vuetify/3.5.10/vuetify.min.css"
)


for i in "${js[@]}"
do
    curl -o $JSDIR/$(basename $i) $i
done 

for i in "${css[@]}"
do
    curl -o $CSSDIR/$(basename $i) $i
done





