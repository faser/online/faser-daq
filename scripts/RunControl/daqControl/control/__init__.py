#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from flask import Blueprint

control = Blueprint("control", __name__)

from . import routes
