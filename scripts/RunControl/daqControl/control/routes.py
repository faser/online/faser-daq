#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

import json
from flask import request, current_app
from . import control
import logging
from ..dhManager import DhManager
from ..auth.utils import only_localhost_scripts

logger = logging.getLogger("daqControl")


@control.route("/executeCommand", methods=["POST"])
@only_localhost_scripts
def execute_command():
    dhMan: DhManager = current_app.extensions["daqControl"].dhManager
    inputJson: dict = request.get_json()  # type: ignore
    try : 
        command = inputJson["command"]
        configName = inputJson["configName"]
        node = inputJson["node"]
        args = inputJson.get("args", None)
    except KeyError as e  : 
        return f"Missing required key in request : {e.args[0]}", 400
    
        
    if configName not in dhMan:
        logger.debug(f"Creating DaqHandler for {configName}")
        dhMan.add_daqHandler(configName)
    try:
        response = dhMan[configName].execute_command(command, node, args)
        if not response[1]: 
            logger.error(f"Command {command} for {configName} returned {json.dumps(response[0], indent=4)}")
    except KeyError as e:
        raise e
    return response
