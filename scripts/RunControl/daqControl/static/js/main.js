const { createApp } = Vue
const { createVuetify } = Vuetify
axios.defaults.timeout = 45000; // 45 minute

const snackBarLevel = {
  ERROR: "ERROR",
  WARNING: "WARNING",
  INFO: "INFO",
  SUCCESS: "SUCCESS"
}

const stateColors = {
  not_added: "grey",
  added: "brown",
  booted: "yellow",
  ready: "blue",
  running: "green",
  paused: "orange",
  error: "red",
  READY: "blue",
  RUN: "green",
  DOWN: "grey lighten-1",
  IN_TRANSITION: "blue-grey lighten-3",
  ERROR: "red",
  PAUSED: "orange"
}


const socketio = io();

const vuetify = createVuetify()
const app = createApp({
  data() {
    return {
      singleRunMode: false,
      configState: null,
      selectedConfig: null,
      configRunType: null,
      configRunComment: null,
      fsm_rules: null,
      externalScripts: [],

      selectedNode: null,
      drawer: true,
      expertMode: false,
      tree: null,
      states: null,
      configs: null,
      whoInterlocked: null,
      runTypes: ['Test', 'TestBeam', 'Calibration', 'Cosmics', 'Physics'],
      loading: {
        'config': false,
        'login_out': false,
        'interlock': false,
        'INITIALISE': false,
        'START': false,
        'STOP': false,
        'SHUTDOWN': false,
        'PAUSE': false,
        'RESUME': false,
        'ECR': false,
        'include_exclude': false,
        'add': false,
        'boot': false,
        'configure': false,
        'start': false,
        'stop': false,
        'unconfigure': false,
        'shutdown': false,
        'remove': false
      },
      snackbar: { "open": false, 'title': '', 'body': '', 'color': '' },
      lostConnection: false,
      user: null,
      controlButtons: {
        "actions": [['INITIALISE', 'success'], ['START', 'success'], ['STOP', 'red'], ['SHUTDOWN', 'red'], ['PAUSE', 'warning'], ['RESUME', 'success'], ['ECR', 'success']],
        "commands": [['add', 'success'], ['boot', 'success'], ['configure', 'success'], ['start', 'success'], ['stop', 'red'], ['unconfigure', 'success'], ['shutdown', 'red'], ['remove', 'success']]
      },
      dialogs: {
        "start": false,
        "stop": false,
        "shutdown_warning": false,
        "lost_connection": false
      },
      sequencerExpanded: false,
      sequencerState: {},
      lastSequencerState: {},

    }
  },

  delimiters: ["[[", "]]"],
  async mounted() {
    axios.get("/status").then(r => {
      this.user = r.data['userinfo'];
      this.singleRunMode = r.data['singleRunMode'];
      if (this.singleRunMode && r.data['selectedConfig'] !== null) {
        this.select_config(r.data['selectedConfig'], initiator = false);
      }
      this.sequencerState = r.data["sequencerState"]
      this.lastSequencerState = r.data["lastSequencerState"]

      this.start_listeners();
    }).catch(e => {
      this.create_snackbar('', e, snackBarLevel.ERROR);
    });
    axios.get("/configs").then(r => {
      this.configs = r.data;
    })
  },
  methods: {
    stopSequencer() {
      axios.post("/stopSequencer").then((response) => {
        if (response.data.status == "error") {
          this.createSnackbar("error", response.data.data)
        }
      })
    },
    startSequence(configName, seqNumber, stepNumber) {
      // function called when pressing the "START" button in the sequencer dialog window
      this.sequencerExpanded = false;
      axios
        .post("/startSequencer", {
          configName: configName,
          startStep: stepNumber,
          seqNumber: seqNumber
        })
        .then((response) => {
          if (response.data["status"] != "success") {
            this.createSnackbar(color = "error", text = "Error : Oops, something bad happened")
          }
        })
        .catch((e) => this.createSnackbar(color = "error", text = e));

    },
    start_listeners() {
      socketio.on("disconnect", () => {
        if (this.loading["login_out"] === false) // doesn't toggle if the user is logging in/out.
          this.dialogs['lost_connection'] = true;
      });


      socketio.on('emit_snackbar', (data) => {
        this.create_snackbar(data['title'], data['body'], data['level']);
      });

      socketio.on('node_state_change', (data) => {
        // console.log("node_state_change emitted");
        // console.log(data);
        this.states = data
      });

      socketio.on('config_state_change', (data) => {
        this.configState = data;
      });

      socketio.on('changed_interlock', (data) => {
        this.whoInterlocked = data;
      });

      socketio.on('run_info_change', (data) => {
        // for run comment and run type
        this.configRunType = data['runType'];
        this.configRunComment = data['runComment'];
      }
      )
      socketio.on('tree_change', (data) => {
        this.states = data['states']
        this.selectedConfig = data['configName'];
        this.tree = data['tree'];
        this.fsm_rules = data['fsm_rules'];
        this.externalScripts = data['externalScripts'];
      });

      if (this.singleRunMode) {
        socketio.on('config_change', (newConfigName) => {
          this.select_config(newConfigName, initiator = false);
        }
        )
      }

      socketio.on("sequencerStateChange", (newState) => {
        if (Object.keys(newState) != 0) {
          this.sequencerState = newState
          this.lastSequencerState = newState
        }
        else {
          this.sequencerState = {}
        }
      })
    },

    reload_page() {
      location.reload();
    },

    async start_stop_run(action) {
      // action can be or start or stop
      this.loading[action] = true;
      this.dialogs[action.toLowerCase()] = false;
      axios.post("/executeAction", { "action": action, "configName": this.selectedConfig, "args": { "runType": this.configRunType, "runComment": this.configRunComment } }).then(r => {
        if (r.status !== 200) {
          this.create_snackbar(r.statusText, r.action["trace"], snackBarLevel.ERROR);
        }
        else {
          if (action === "START" && typeof (r.data) === 'string') {
            this.create_snackbar('Error', r, snackBarLevel.ERROR);
          }
        }
      }).catch(e => {
        this.create_snackbar('', e, snackBarLevel.ERROR);
      }).finally(() => {
        this.loading[action] = false;
      });
    },

    execute_action(action) {
      if (action === "START") {
        this.dialogs['start'] = true;
      }
      else if (action === "STOP") {
        this.dialogs['stop'] = true;
      }
      else {
        this.loading[action] = true;

        if (action === "SHUTDOWN" && !["READY", "ERROR"].includes(this.configState) && !this.dialogs['shutdown_warning']) {
          this.dialogs['shutdown_warning'] = true;
          this.loading[action] = false;
          return
        }
        this.dialogs['shutdown_warning'] = false;
        axios.post("/executeAction", { "action": action, "configName": this.selectedConfig }).then(r => {
          if (r.status !== 200) {
            this.create_snackbar(r.statusText, r.statusText, snackBarLevel.ERROR);
          }
        }).catch(e => {
          this.create_snackbar('Unhandled error', e, snackBarLevel.ERROR);
        }).finally(() => {
          this.loading[action] = false;
        });
      }
    },
    execute_command(command) {
      this.loading[command] = true;
      axios.post("/executeCommand", { "node": this.selectedNode.name, "command": command, "configName": this.selectedConfig }).then(r => {
        const r_command = r.data;
        if (r.status !== 200) {
          this.create_snackbar(r.statusText, r_command["trace"], snackBarLevel.ERROR);
        }
      }).catch(e => {
        this.create_snackbar('', e, snackBarLevel.ERROR);
      }).finally(() => {
        this.loading[command] = false;
      });
    },
    selectNode(node) {
      this.selectedNode = node;
    },
    select_config(configName, initiator = false) {
      this.loading['config'] = true;
      axios.get("/selectConfig", { params: { "configName": configName, "initiator": initiator } }).then(r_change => {
        const data_change = r_change.data;
        if (typeof data_change === 'string') {
          this.create_snackbar('Error', data_change, snackBarLevel.ERROR);
          this.loading['config'] = false;
          return;
        }
        this.states = data_change['states']
        this.selectedConfig = data_change['configName'];
        this.tree = data_change['tree'];
        this.fsm_rules = data_change['fsm_rules'];
        this.externalScripts = data_change['externalScripts'];

        if (this.selectedConfig !== null) {
          socketio.emit('leave', { 'room': this.selectedConfig })
        }
        socketio.emit('join', { 'room': this.selectedConfig });

        axios.get("/configStatus", { params: { "configName": configName } }).then(r_status => {
          const data_stats = r_status.data;
          this.configState = data_stats['configState'];
          this.configRunType = data_stats['configRunType'];
          this.configRunComment = data_stats['configRunComment'];
          this.whoInterlocked = data_stats['whoInterlocked'];
        }
        )
          .catch(e => {
            this.loading['config'] = false;
          }).finally(() => {
            this.loading['config'] = false;
          });
      })
    },

    // nodeName is a string
    toggle_include_exclude(nodeName) {
      const included = this.states[nodeName]['included'] == false;
      axios.post("/executeCommand", { "command": included ? "exclude" : "include", "configName": this.selectedConfig, "node": nodeName }).then(r => {
        if (r.status !== 200) {
          this.create_snackbar('', r.statusText, snackBarLevel.ERROR);
        }
      }).catch(e => {
        this.create_snackbar('', e, snackBarLevel.ERROR);
      });


    },


    create_snackbar(title, body, level) {
      switch (level) {
        case snackBarLevel.INFO: this.snackbar['color'] = 'default'; break;
        case snackBarLevel.WARNING: this.snackbar['color'] = 'orange'; break;
        case snackBarLevel.ERROR: this.snackbar['color'] = 'red'; break;
        case snackBarLevel.SUCCESS: this.snackbar['color'] = 'green'; break;
        default: this.snackbar['color'] = 'default'; break;
      }
      this.snackbar['title'] = title;
      this.snackbar['body'] = body;
      this.snackbar['open'] = true;
    },
    login() {
      this.loading['login_out'] = true;
      window.location.assign("/login");
    },
    logout() {
      this.loading['login_out'] = true;
      window.location.assign("/logout");
    },
    async interlock(action) {
      this.loading['interlock'] = true;
      let route = "/lock"
      if (action === "release") {
        route = "/unlock"
      }
      axios.post(route, { 'ID': this.selectedConfig, 'user': this.user }).then(r => {
        if (r.status !== 200) {
          this.create_snackbar('', r.statusText, snackBarLevel.ERROR);
        }
      }).catch(e => {
        this.create_snackbar('', e, snackBarLevel.ERROR);
      }).finally(() => {
        this.loading['interlock'] = false;
      });
    },
    node_color_from_module(nodeName) {
      return stateColors[this.states[nodeName]['state']];
    },

    state_color(state) {
      return stateColors[state];
    },
    enabled_action(action) {
      if (this.fsm_rules === null || this.fsm_rules[this.configState] === undefined) {
        return false;
      }
      else {
        return this.fsm_rules[this.configState].includes(action);
      }

    },
    enabled_command(command) {
      const nodeState = this.states[this.selectedNode.name]['state'];
      if (this.fsm_rules === null || this.fsm_rules[nodeState] === undefined) {
        return false;
      }
      else {
        return this.fsm_rules[nodeState].includes(command);
      }
    },
  },
  watch: {

  },
  computed: {
    isSequencer: function () {
      return Object.keys(this.sequencerState).length !== 0;
    },
    highestStatus() {
      highestStat = 0;
      for (let node in this.states) {
        if (this.states[node]['status'] > highestStat) {
          highestStat = this.states[node]['status']
        }
      }
      return highestStat;
    },

    logged() {
      if (this.user !== null) {
        if (this.user != "not_logged") {
          return true;
        }
      }
      return false;
    },
    histogramLink() {
      return `${window.location.protocol}//${window.location.hostname}:8050`
    },

    interlocked() {
      if (this.user !== null) {
        return this.whoInterlocked == this.user;
      }
      return false;
    },
    start_dialog() {
      return this.dialogs['start'] && this.interlocked;
    },
    stop_dialog() {
      return this.dialogs['stop'] && this.interlocked;
    },
    shutdown_warning_dialog() {
      return this.dialogs['shutdown_warning'] && this.interlocked;
    },

    sequencer_dialog() {
      return this.sequencerExpanded && this.interlocked;
    },


    unlocked() {
      return {

        // 'selectConfig': ((!this.singleRunMode && this.logged && !this.interlocked) || (this.singleRunMode && this.logged && this.interlocked)),
        'selectConfig': ((this.logged && !this.singleRunMode && !this.interlocked) || (this.singleRunMode && this.logged && this.interlocked && this.configState == "DOWN")) || (this.interlocked && this.logged && this.singleRunMode && this.selectedConfig == null),
        // 'selectConfig': true,
        'include_exclude': this.interlocked && this.configState === "DOWN",
        // 'interlock_btn': this.logged && this.selectedConfig !== null && (!this.whoInterlocked || this.interlocked),
        'interlock_btn': (this.logged && (!this.whoInterlocked || this.interlocked) && (!this.interlocked || this.whoInterlocked) && (this.singleRunMode || this.selectedConfig !== null)),
        'sequencer_btn': this.singleRunMode && ((this.interlocked && this.configState === "DOWN") || (this.interlocked && this.isSequencer)),

      }
    }

  },
});

app.use(vuetify)
app.component("log_panel", {
  delimiters: ["[[", "]]"],
  props: ["externalScripts", "configName"],
  data: function () {
    return {
      logs: []
    };
  },
  mounted() {
    axios.get("/logs").then((r) => {
      this.logs = r.data;

      socketio.on('log_emit', (data) => {
        this.logs.push(data);
      });
      this.$nextTick(() => {
        this.$refs.anchor.scrollIntoView();
      })
    }).catch(e => {
      console.error(e);
    });
  },
  methods: {
    level_color(level) {
      switch (level) {
        case "ERROR": return "red";
        case "WARNING": return "orange";
        case "INFO": return "blue";
        case "SUCCESS": return "green";
        default: return "grey";
      }
    },
    open_full_log(process) {
      window.open(`/log/${this.configName}/${process}`, "_blank")
    }
  },
  template: /*html*/`
  <v-card tile elevation="1" class="mx-4 my-4">
  <v-card-title> LOG 
  <v-btn v-if ="externalScripts.length !== 0" variant="text" color="blue" append-icon="mdi-menu-down">
  scripts
  <v-menu activator="parent">
  <v-list>
    <v-list-item v-for="es in externalScripts" @click="open_full_log(es)">
      <v-list-item-title> [[ es ]] </v-list-item-title>
    </v-list-item>
  </v-list>
</v-menu>
  </v-btn> 
  </v-card-title>
    <v-card-text class="log-container">
      <div v-if="logs.length !== 0 " class="my-1" density="compact" v-for="log in logs" :key="log.timestamp">
      [ [[ log.timestamp ]] ] - [[ log.logger ]] - 
        <v-chip size="small"  :color="level_color(log.level)">
        [[ log.level]]
        </v-chip>
        -  <span style="white-space: pre;"> [[ log.message ]] </span>
      </div>
      <div v-else >
      LOADING...
      </div>
      <div id="anchor" ref="anchor" style="overflow-anchor:auto; height:1px;"> </div>
    </v-card-text>
  </v-card>
  `,
});

app.component("tree-node", {
  delimiters: ["[[", "]]"],
  props: {
    node: {
      type: Object,
      required: true
    },
    selectedNode: {
      type: Object,
      default: null
    }
  },
  data: function () {
    return {
      isExpanded: true,
    };
  },
  mounted() {

  },
  methods: {
    selectNode() {
      // this.isExpanded = !this.isExpanded;
      if (this.isSelected) {
        this.$emit('node-selected', null);
      } else {
        this.$emit('node-selected', this.node);
      }

    }
  },
  computed: {
    isSelected() {
      return this.node === this.selectedNode;
    }
  },

  template: /*html*/`
  <div>
  <div class="d-flex align-center node" :class="{ 'node-selected' : isSelected }"  @click.stop="selectNode">
  <v-icon :class="!node.children ? 'hidden' : ''" :icon="isExpanded ? 'mdi-menu-down' : 'mdi-menu-right'" color="black" @click.stop="isExpanded = !isExpanded"></v-icon>
  <slot name="item" v-bind="node">
    [[ node.name ]]
  </slot>
  </div>
  <div v-show="node.children && isExpanded" class="child-nodes">
    <tree-node v-for="(child, index) in node.children" :key="index" :node="child"
    :selected-node="selectedNode" @node-selected="$emit('node-selected', $event)" 
    >
    <template v-slot:item="slotProps">
    <slot name="item" v-bind="slotProps"></slot>
    </template>
    </tree-node>
  </div>
</div>
  `,
});


app.component("info-panel", {
  delimiters: ["[[", "]]"],
  props: {
    selectedNode: {
      type: Object,
      required: true
    },
    configName: {
      type: String,
      required: true
    }
  },
  data: function () {
    return {
      data: [],
    };
  },
  mounted() {
  },
  methods: {
    open_full_log() {
      window.open(`/log/${this.configName}/${this.selectedNode.name}`, "_blank")
    },
    open_live_log() {
      axios.get("/liveLogURL", { params: { "configName": this.configName, "processName": this.selectedNode.name } }).then(r => {
        window.open(r.data, "_blank", "width=800, height=500")
      })
    },

  },
  computed: {

  },

  template: /*html*/`
  <v-card tile elevation="1" class="mx-4 my-4">
  <v-card-title> INFO : [[ selectedNode.name ]] 
    <v-btn variant="text" color="blue" append-icon="mdi-menu-down">
    log
    <v-menu activator="parent">
    <v-list>
      <v-list-item @click="open_live_log">
        <v-list-item-title> Live log </v-list-item-title>
      </v-list-item>
      <v-list-item @click="open_full_log">
        <v-list-item-title> Full log </v-list-item-title>
      </v-list-item>
    </v-list>
  </v-menu>
    </v-btn>
   </v-card-title>
  <v-card-text style="max-height:500px;" class="py-2 scrollable">
        <faser-metrics-table :module="selectedNode" />
  </v-card-text>
  </v-card>
  `,
});

app.component("faser-metrics-table", {
  delimiters: ["[[", "]]"],
  props: {
    module: {
      type: Object,
      required: true
    },
  },
  data: function () {
    return {
      data: [],
      interval: 1000, // 1 second
      c_met: null,
    };
  },
  mounted() {
    this.startMetricsStream();

  },
  beforeUnmount() {
    this.stopMetricsStream();
  },
  watch: {
    module: {
      handler: function () {
        this.stopMetricsStream();
        this.startMetricsStream();
      },
      deep: true
    }
  },
  methods: {
    stopMetricsStream() {
      if (this.c_met !== null) {
        this.c_met.close();
      }
    },
    startMetricsStream() {
      // start event stream
      this.c_met = new EventSource(`/metrics/stream/${this.module.name}`);
      this.c_met.onmessage = (e) => {
        this.data = JSON.parse(e.data.replace(/'/g, '"'));
      }
    },


  },
  computed: {

  },

  template: /*html*/`
    <v-table v-if="data.length != 0" density="compact">
      <thead>
        <tr>
          <th class="text-left">Key</th>
          <th class="text-left">Value</th>
          <th class="text-left">Time</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="item in data" :key="item.key">
          <td>[[ item.key ]]</td>
          <td>[[ item.value ]]</td>
          <td>[[ item.time ]]</td>
        </tr>
      </tbody>
  </v-table>
  <div v-else class="text-center flex align-center" style="height: 100%"> <h2>No data to display </h2> </div>
  `,
});

app.component("faser-monitoring-panel", {
  delimiters: ["[[", "]]"],
  props: ["configState"],
  data: function () {
    return {
      interval: 1, // second
      c_met: null,
      categories: ["Physics", "Calibration", "TLBMonitoring"],

      runNumber: "-",
      runStart: "-",
      events: {
        "Physics": "-",
        "Calibration": "-",
        "TLBMonitoring": "-"
      },
      latest: {
        "Physics": "-",
        "Calibration": "-",
        "TLBMonitoring": "-"
      },
    }

  },
  mounted() {
    Plotly.newPlot("plot", [{
      x: [],
      y: [],
      name: 'Physics',
      mode: 'lines',
      line: { color: '#80CAF6' }
    }, {
      x: [],
      y: [],
      name: 'Calibration',
      mode: 'lines',
      line: { color: '#DF56F1' },
      visible: 'legendonly'
    }, {
      x: [],
      y: [],
      name: 'TLBMonitoring',
      mode: 'lines',
      line: { color: '#4D92E9' },
      visible: 'legendonly'

    }],
      {
        height: 300,
        margin: { l: 30, r: 10, b: 0, t: 10, pad: 0 },
        font: { size: 13 },
        xaxis: {
          showticklabels: false,
          autorange: true,
          rangeslider: {},
        },
        showlegend: true,
        legend: {
          x: 0.5,
          xanchor: 'center',
          y: 1,
          "orientation": "h"
        }
      },
      { responsive: true, displayModeBar: false, showTips: false }
    );

  },
  beforeUnmount() {
  },
  watch: {
    configState: {
      handler: function (newState, oldState) {
        if ((newState == "READY") || (newState == "RUN")) {
          // initialise
          this.stop_monitoring_stream();
          this.create_monitoring_stream();
        }
        else if (newState == "DOWN") {
          this.stop_monitoring_stream();
        }
      },
      deep: true
    }
  },
  methods: {
    stop_monitoring_stream() {
      if (this.c_met !== null) {
        this.c_met.close();
      }
    },
    create_monitoring_stream() {
      this.c_met = new EventSource(`/monitoring/stream`);
      this.c_met.onmessage = (e) => {
        const r = JSON.parse(e.data.replace(/'/g, '"'));
        const data = r['graph'];
        const other = r['other'];
        this.runNumber = other["runNumber"];
        this.runStart = other["runStart"];
        const lastInd = data["Physics"][1].length - 1;
        for (let el of this.categories) {
          this.events[el] = other[`Events_${el}`];
          this.latest[el] = data[el][1][lastInd];
        }

        Plotly.extendTraces("plot", {
          x: [data["Physics"][0].map(e => Date(e * 1000)), data["Calibration"][0].map(e => Date(e * 1000)), data["TLBMonitoring"][0].map(e => Date(e * 1000))],
          y: [data["Physics"][1], data["Calibration"][1], data["TLBMonitoring"][1]]
        }, [0, 1, 2], 60 * 15)
      }
    }
  },
  computed: {

  },

  template: /*html*/`
<v-card tile elevation="1" class="mx-4 my-4" style="height:500px;">
<v-card-title> MONITORING 
  </v-card-title>
<v-card-text class="d-flex-col justify-center mb-3 py-2">
<div class="d-flex justify-center">
  <v-sheet color="blue-lighten-5" elevation="1" class="metrics-panel" rounded>
    <h4 class="text-center">Run</h4>
    <div class="text-body-2">
      <div>
        <strong class="text-blue text-darken-4">Number: </strong> [[ runNumber ]]
      </div>
      <div>
        <strong class="text-blue text-darken-4">Starting time:</strong> [[ runStart ]]
      </div>
    </div>
  </v-sheet>
  <v-sheet color="blue-lighten-5" v-for="cat in categories" :key="cat" elevation="1" class="lighten-5 metrics-panel" rounded>
    <h4 class="text-center"> [[cat]]</h4>
    <div class="text-body-2">
      <div>
        <strong class="text-blue text-darken-4">Events: </strong> [[ events[cat] ]] 
      </div>
      <div>
        <strong class="text-blue text-darken-4">Rate: </strong> [[ latest[cat] ]] Hz
      </div>
    </div>
  </v-sheet>
</div>
  <div class="my-4" id="plot">
  </div>
</v-card-text>
</v-card>
`,
});

app.component("sequencer_dialog", {
  delimiters: ["[[", "]]"],
  props: {
    "sequencer_state": Object,
    "last_sequencer_state": Object,
  },
  data: function () {
    return {
      dialog: true,
      selected_config: "",
      seqNumber: 0,
      stepNumber: 2,
      loadingConfig: false,
      configFile: null,
      steps: [],
      listConfigs: [],
      expandedSteps: false,
      errorLoadedConfigMessage: "",
      stopping: false
    };
  },
  mounted() {
    if (Object.keys(this.sequencer_state).length == 0) {
      console.log("Getting the configs from the correct folder")
      axios.get("/getSequencerConfigs").then((response) => {
        this.listConfigs = response.data
      })
    }
    else {
      this.loadSequencerConfig(this.sequencer_state.sequenceName.replace(".json", ""))
      this.expandedSteps = true;
    }
  },
  methods: {
    emit_stop() {
      this.$emit("stop_seq")
      this.stopping = true
    },
    loadSequencerConfig(selected_config) {
      axios.get("/loadSequencerConfig", { params: { "sequencerConfigName": selected_config } }).then((response) => {
        if (response.data.status == "error") {
          this.errorLoadedConfigMessage = response.data.data;
          console.log("Error, with the following message : ", response.data.data);
        }
        else {
          this.errorLoadedConfigMessage = "";
          this.steps = response.data.data.steps;
          this.stepNumber = 1 // preventing a bug from the slider in the sequencer rcgui dialog 
          this.configFile = response.data.data.config;
        }
        this.loadingConfig = false;
      })
    }
  },

  watch: {
    selected_config: {
      handler: function () {
        this.loadingConfig = true;
        this.loadSequencerConfig(this.selected_config);
      }
    }

  },
  template: /*html*/`
    <v-dialog v-model="dialog" width="700" persistent>
  <v-card>  
    <v-card-title>
          <span class="text-h5"> Sequencer : [[ Object.keys(this.sequencer_state).length == 0 ? '' : this.sequencer_state.sequenceName ]] </span>
    </v-card-title>
 

    <v-card-text>
      <v-row>
        <v-col>
          <span v-if ="Object.keys(last_sequencer_state).length != 0" class="text-caption">
            Last seq info : [[ this.last_sequencer_state.sequenceName]], 
            seqNumber : [[this.last_sequencer_state.seqNumber]], 
            step :  [[this.last_sequencer_state.stepNumber]] / [[this.last_sequencer_state.totalStepsNumber]]
          </span>
          </v-col>
      </v-row >

     <div v-if="Object.keys(sequencer_state).length == 0">
      <v-row>
        <v-col>
          <v-select v-model="selected_config" :items="listConfigs" label="Select sequence configuration file" variant="outlined"
            density="compact" :hide-details="errorLoadedConfigMessage === ''" disable-lookup :readonly="loadingConfig" :loading="loadingConfig"
            :error="errorLoadedConfigMessage !== ''" :error-messages="errorLoadedConfigMessage"
            >
            </v-select>
        </v-col>
      </v-row>
      <template v-if="errorLoadedConfigMessage === ''">
      <v-row>
      <v-col cols="6">
      <v-text-field label="Sequence number" v-model="seqNumber" variant="outlined" density="compact" hide-details></v-text-field>
      </v-col>
      <v-spacer></v-spacer>
      </v-row>
      <v-row v-if="selected_config !== ''" justify="space-around">
        <v-col cols="12">
          <v-slider class="align-center" v-model="stepNumber" hide-details step="1" :min="1" :max="steps.length"
            label="Step number" thumb-label>
            <template v-slot:append>
              <v-text-field v-model="stepNumber" class="mt-0 pt-0" hide-details single-line type="number"
                style="width: 60px" :min="1" :max="steps.length"></v-text-field>
            </template>
          </v-slider>
        </v-col>
      </v-row>
      </template>
      </div> 
    </v-card-text>
    <v-expand-transition>
      <div v-if="expandedSteps">
        <v-card-text style="overflow:auto; white-space: nowrap;">
          <v-virtual-scroll :bunched="10" :items="steps" height="200" item-height="26">
            <template v-slot:default="{ item, index }">
              <div :key="index" :class=" index+1 === sequencer_state.stepNumber ? 'd-flex text-body-2 green lighten-2 py-2' : 'd-flex text-body-2 py-2' ">
                <span class="mx-2">[[index+1]] : </span>
                <span> [[ item.startcomment ]] </span>
              </div>
            </template>
          </v-virtual-scroll>
        </v-card-text>
      </div>
    </v-expand-transition>
    <v-card-actions>
    <v-btn variant="flat" @click="$emit('close_seq')">close</v-btn>
    <v-spacer></v-spacer>
        <template v-if="Object.keys(sequencer_state).length == 0">
        <v-btn variant="flat" :disabled="selected_config==''" color="success"
          @click="$emit('start_seq', selected_config, seqNumber, stepNumber)">Start</v-btn>
      </template>
      <template v-else>
        <v-btn  variant="flat" :disabled="stopping" color="error" @click="emit_stop">[[ stopping ? "STOPPING..." :  "STOP"]]</v-btn>
      </template>
      <v-btn variant="text" @click="expandedSteps = !expandedSteps" v-if="errorLoadedConfigMessage === ''">
        [[ expandedSteps ? 'hide steps' : 'show steps' ]]
      </v-btn>
    </v-card-actions>
  </v-card>
</v-dialog>
  `,
});

app.mount("#app")