#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from datetime import timedelta
import json
import importlib.resources
import os
from .utils import redis_wrappers as rw

import redis
from .utils.configs import validate_serverconfig
from os import environ as env
from .dhManager import DhManager


class Flask_daqControl:

    def __init__(self, configPath, app=None):
        self.configPath = configPath
        try:
            with open(self.configPath, "r") as f:
                config = json.load(f)
        except Exception as e:
            exit(f"Error loading configuration file: {e}")

        with importlib.resources.open_text("daqControl", "serverconfiguration.schema") as f:
            schema = json.load(f)

        validate_serverconfig(config, schema)

        config["DAQ_CONFIG_DIR"] = env["DAQ_CONFIG_DIR"]
        config["DAQ_SCRIPT_DIR"] = env["DAQ_SCRIPT_DIR"]
        self.config = config

        self.r_rcstates = redis.Redis(
            host="localhost",
            port=6379,
            db=config["redis_rcstates_db"],
            charset="utf-8",
            decode_responses=True,
        )

        self.r_metrics = redis.Redis(
            host="localhost",
            port=6379,
            db=config["redis_metrics_db"],
            charset="utf-8",
            decode_responses=True,
        )
        self.dhManager = DhManager(
            config["DAQ_CONFIG_DIR"],
            self.r_rcstates,
            single_run_mode=config["single_run_mode"],
            redis_metrics_db=self.r_metrics,
        )
        for configName in set(
            [*rw.get_active_configs(self.r_rcstates), rw.get_selected_config(self.r_rcstates)]
        ):
            if configName is not None:
                self.dhManager.add_daqHandler(configName)
        # if app is not None:
        #     self.init_app(app,socketio)

    def init_app(self, app, socketio):

        app.config["PERMANENT_SESSION_LIFETIME"] = timedelta(
            minutes=self.config["timeout_session_expiration_mins"]
        )
        app.config["daqControl"] = self.config
        app.config["SECRET_KEY"] = os.urandom(24)

        app.extensions["daqControl"] = self
