#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

import json
import os
from flask import Flask
from flask_cors import CORS
import urllib3
from .utils.custom_logging import setup_logging
from flask_socketio import SocketIO, join_room, leave_room

from .flask_daqControl import Flask_daqControl
from .statechecker import stateChecker


socketio = SocketIO()


@socketio.on("join")
def on_join(data):
    """
    When a user changes config, it will register it in a specific room, so only events related to the config are captured.
    """
    configName = data["room"]
    join_room(configName)


@socketio.on("leave")
def on_leave(data):
    configName = data["room"]
    leave_room(configName)


def create_app(configPath: str, port, localOnly=False, debug=True):
    flask_daq = Flask_daqControl(configPath)
    setup_logging(socketio, flask_daq.config["serverlog_location_name"])

    app = Flask(__name__)
    CORS(app)
    app.debug = debug
    flask_daq.init_app(app, socketio)

    influxDB = {}
    mattermost_hook = ""
    if os.access("/etc/faser-secrets.json", os.R_OK):
        influxDB = json.load(open("/etc/faser-secrets.json"))
        mattermost_hook = influxDB.get("MATTERMOST", "")
        urllib3.disable_warnings()

    app.config["daqControl"].update(
        {
            "run_user": "FASER",  # for run service
            "run_pw": "HelloThere",
            "local_only": localOnly,
            "influxDB": influxDB,
            "mattermost_hook": mattermost_hook,
            "port": port,
        }
    )

    print("")
    print("--- SERVER CONFIGURATION ---")
    print(f"LOCAL ONLY : {app.config['daqControl']['local_only']}")
    print(f"SINGLE RUN MODE : {app.config['daqControl']['single_run_mode']}")
    print(f"AUTOMATIC RESTART : {app.config['daqControl']['automatic_restart']}")
    print(f"SSO ENABLED : {app.config['daqControl']['SSO_enabled']}")
    print("----------------------------")
    print("")

    from .main import main as main_bp
    from .control import control as ctrl_bp
    from .auth import auth as auth_bp
    from .custom import custom as custom_bp

    app.register_blueprint(ctrl_bp)
    app.register_blueprint(auth_bp)
    app.register_blueprint(custom_bp)
    app.register_blueprint(main_bp)
    socketio.init_app(app, cors_allowed_origin="*")
    socketio.start_background_task(stateChecker, app=app)
    return app
