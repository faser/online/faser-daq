#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

import os
import socket
import subprocess
from ..utils.constants import Action
import requests
from flask import current_app
import logging
from ..utils.mattermost import message


logger = logging.getLogger("daqControl")

run_user = "FASER"
run_pw = "HelloThere"


class TimeoutException(Exception):
    pass


def post_influxDB_mattermost(
    action: str, configName, runComment, runType, runNumber, physicsEvents=None
):
    """
    Post start, stop and shutdown information on influxDB and (combined run), post a message on mattermost
    """
    CONFIG = current_app.config["daqControl"]
    influxDB = CONFIG.get("influxDB", None)
    runData = ""
    hostname = socket.gethostname()
    if action == Action.START:
        runData = f'runStatus,host={hostname} state="Started",comment="{runComment}",runType="{runType}",runNumber={runNumber}'
    elif action == Action.STOP:
        runData = f'runStatus,host={hostname} state="Stopped",comment="{runComment}",runType="{runType}",runNumber={runNumber},physicsEvents={physicsEvents}'
    elif action == Action.SHUTDOWN:
        runData = f'runStatus,host={hostname} state="Shutdown",comment="Run was shutdown",runNumber={runNumber},physicsEvents={physicsEvents}'
    try:

        r = requests.post(
            f'https://dbod-faser-influx-prod.cern.ch:8080/write?db={influxDB["INFLUXDB"]}',
            auth=(influxDB["INFLUXUSER"], influxDB["INFLUXPW"]),
            data=runData,
            verify=False,
            timeout=10,
        )
        if r.status_code != 204:
            logger.error(f"Failed to post end of run information to influxdb: {r.text}")
    except requests.exceptions.ConnectionError:
        logger.error("Could not connect to influxDB")
    except requests.exceptions.Timeout:
        logger.error("InfluxDB request timed out")

    if configName.startswith("combined"):
        if action == Action.STOP:
            message(f"Run {runNumber} stopped\nOperator: {runComment}")
        elif action == Action.START:
            message(f"Run {runNumber} was started\nOperator: {runComment}")
        elif action == Action.SHUTDOWN:
            message(f"Run {runNumber} was shutdown ")


def send_start_to_runservice(runType: str, runComment: str, reqDict: dict, app=None):
    """
    Parameters
    ----------
    - runType : type of the run
    - runComment : run comment
    - reqDict : dictionnary of all the run service-related info

    Return
    ------
    - Run Number provided by the run service
    """

    # r_states = current_app.extensions["daqControl"].r_rcstates
    if app is not None : 
        dhMan = app.extensions["daqControl"].dhManager
        CONFIG = app.config["daqControl"]
    else : 
        app = current_app
        dhMan = current_app.extensions["daqControl"].dhManager
        CONFIG =current_app.config["daqControl"]
    
    with app.app_context() :
        version = subprocess.check_output(["git", "rev-parse", "HEAD"]).decode("utf-8").strip()
        seqnumber = reqDict.get("seqnumber", None)
        seqstep = reqDict.get("seqstep", 0)
        seqsubstep = reqDict.get("seqsubstep", 0)
        configName = dhMan.selectedConfig
        config = dhMan[configName].config
        detList = dhMan[configName].detectorList()
        runNumber = None

        rsURL = "http://faser-runnumber.web.cern.ch/NewRunNumber"
        try:
            res = requests.post(
                rsURL,
                auth=(run_user, run_pw),
                json={
                    "version": version,
                    "type": runType,
                    "username": os.getenv("USER"),
                    "startcomment": runComment,
                    "seqnumber": seqnumber,
                    "seqstep": seqstep,
                    "seqsubstep": seqsubstep,
                    "detectors": detList,
                    "configName": configName,
                    "configuration": config,
                },
                timeout=10,
            )
            if res.status_code != 201:
                logger.error(f"Failed to get run number: {res.text}")
                return f"Error: Failed to get run number: {res.text}"  # type: ignore
            else:
                try:
                    runNumber = int(res.text)
                except ValueError:
                    logger.error(f"Failed to get run number: {res.text}")
                    return f"Error: Failed to get run number: {res.text}"  # type: ignore
        except requests.exceptions.ConnectionError:
            logger.error("Could not connect to run service")
            return "Error: Could not connect to run service"  # type: ignore

        if CONFIG.get("influxDB", None) is not None:
            post_influxDB_mattermost(Action.START, configName, runComment, runType, runNumber)
        return runNumber


def send_stop_to_runservice(runComment, runType, runNumber, forceShutdown=False, app=None):
    """
    Parameters
    ----------
    - runType : type of the run
    - runComment : run comment
    - runNumber : run number
    - forceShutdown : if the function is used for a "SHUTDOWN" root command.
    """

    if app is not None : 
        dhMan = app.extensions["daqControl"].dhManager
        CONFIG = app.config["daqControl"]
    else : 
        app = current_app
        dhMan = current_app.extensions["daqControl"].dhManager
        CONFIG =current_app.config["daqControl"]
    with app.app_context():
        runinfo = {}
        configName = dhMan.selectedConfig
        runinfo["eventCounts"] = getEventCounts()
        physicsEvents = runinfo["eventCounts"]["Events_sent_Physics"]  # for influxDB

        if forceShutdown:
            runComment = "Run was shut down"
        stopMsg = {"endcomment": runComment, "type": runType, "runinfo": runinfo}
        rsURL = f"http://faser-runnumber.web.cern.ch/AddRunInfo/{runNumber}"
        try:
            res = requests.post(rsURL, auth=(run_user, run_pw), json=stopMsg, timeout=10)

            if res.status_code != 200:
                logger.error(
                    f"Failed to register end of run information: {res.text}",
                )

        except requests.exceptions.ConnectionError:
            logger.error(
                "Could not connect to run service",
            )

        except requests.exceptions.Timeout:
            logger.error(
                "Run service request timed out",
            )

        if CONFIG.get("influxDB", None) is not None:
            if forceShutdown:
                post_influxDB_mattermost(
                    Action.SHUTDOWN, configName, runComment, runType, runNumber, physicsEvents
                )
            else:
                post_influxDB_mattermost(
                    Action.STOP, configName, runComment, runType, runNumber, physicsEvents
                )


def getEventCounts():
    r_metrics = current_app.extensions["daqControl"].r_metrics
    types = ["Physics", "Calibration", "TLBMonitoring"]
    values = {}
    for rateType in types:
        name = "Events_sent_" + rateType
        eventbuilderModule = r_metrics.keys("eventbuilder*")[0]
        value = r_metrics.hget(eventbuilderModule, name)
        value = value.split(":")
        values[name] = int(value[1])
    return values
