#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from flask import Blueprint

custom = Blueprint("custom", __name__)

from . import monitoring
from . import faser_control
from . import sequencer_routes
