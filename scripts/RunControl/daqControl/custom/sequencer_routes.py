#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from glob import glob
import os, sys

import redis
from . import custom
from flask import request, current_app
import logging
import json
from os import environ as env

sys.path.append(os.path.join(env["DAQ_SCRIPT_DIR"], "Sequencer"))
import sequencer

logger = logging.getLogger("daqControl")


@custom.route("/addLog", methods=["POST"])
def add_log():
    data = request.get_json()
    level: str = data["level"]
    source: str = data["source"]
    message: str = data["message"]

    if level.lower() == "debug":
        logger.debug(f"{source} : {message}")
    elif level.lower() == "info":
        logger.info(f"{source} : {message}")
    elif level.lower() == "warning":
        logger.warning(f"{source} : {message}")
    elif level.lower() == "error":
        logger.error(f"{source} : {message}")
    elif level.lower() == "critical":
        logger.critical(f"{source} : {message}")
    else:
        logger.error(f"Invalid log level for log : {data}")
    return "OK", 200


@custom.route("/sequencerUpdate", methods=["POST"])
def sequencer_update():
    socketio = current_app.extensions["socketio"]
    data = request.get_json()
    socketio.emit("sequencerStateChange", data)
    return "OK"


@custom.route("/getSequencerConfigs", methods=["GET"])
def getSequencerConfigs():
    files = sorted(glob(os.path.join(env["DAQ_SEQUENCE_CONFIG_DIR"], "*.json")))
    # keeping only the filename instead of full path
    files = [os.path.basename(file).replace(".json", "") for file in files]
    return files


@custom.route("/loadSequencerConfig", methods=["GET"])
def loadSequencerConfig():
    sequencerConfigName: str = request.args.get("sequencerConfigName", "")
    sequencerConfigName = sequencerConfigName + ".json"
    response = {}
    try:
        config, steps = sequencer.load_steps(sequencerConfigName)
    except (RuntimeError, FileNotFoundError, KeyError) as err:
        logger.error(f"An error occured when loading the sequencer config: {err}")
        response["status"] = "error"
        response["data"] = repr(err)
    except json.decoder.JSONDecodeError as e:
        logger.error(f"An error occured when parsing the JSON file: {e}")
        response["status"] = "error"
        response["data"] = f"An error occured when parsing the JSON file: {e}"
    else:
        response["status"] = "success"
        response["data"] = {"config": config, "steps": steps}
    return response


@custom.route("/startSequencer", methods=["POST"])
def startSequencer():
    socketio = current_app.extensions["socketio"]
    requestData = request.get_json()
    configName = requestData["configName"]
    startStep = requestData["startStep"]
    seqNumber = requestData["seqNumber"]
    # NOTE: The sequence number should be given by the runNumber -> will be overwritten by the sequencer
    configName+=".json"
    argsSequencer = ["-S", seqNumber, "-s", startStep, configName]  # sequence Number  # startStep
    socketio.start_background_task(sequencer.main, argsSequencer, socketio, logger)
    logger.info(f"Starting sequencer with args : {argsSequencer}")
    return {"status": "success"}


@custom.route("/stopSequencer", methods=["POST"])
def stopSequencer():
    r_rcstates: redis.Redis = current_app.extensions["daqControl"].r_rcstates
    r_rcstates.set("stopSequencer", "True")
    logger.info("Stopping sequencer")
    return {"status": "success"}
