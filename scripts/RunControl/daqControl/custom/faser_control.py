#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from time import sleep
import logging
from flask import request, current_app
import json

from . import custom
from ..main.utils import emit_snackbar
from ..utils.constants import Command, Action, ConfigState, SocketioEvent, SnackbarLevel
from ..utils import redis_wrappers as rw
from .utils import send_start_to_runservice, send_stop_to_runservice
from ..auth.utils import only_localhost_scripts


logger = logging.getLogger("daqControl")


@custom.route("/executeAction", methods=["POST"])  # type: ignore
@only_localhost_scripts
def execute_action(data=None, app=None):
    """Actions are composed of a sequence of commands but can also include other steps such as checking a specific condition, waiting for a specific event, etc."""
    if app is None: 
        dhMan = current_app.extensions["daqControl"].dhManager
        CONFIG = current_app.config["daqControl"]
        r_rcstates = current_app.extensions["daqControl"].r_rcstates
        socketio = current_app.extensions["socketio"]
    else : 
        dhMan = app.extensions["daqControl"].dhManager
        CONFIG = app.config["daqControl"]
        r_rcstates = app.extensions["daqControl"].r_rcstates
        socketio = app.extensions["socketio"]

    inputJson = data  # type: ignore
    if data is None:  # called from a request
        inputJson: dict = request.get_json()  # type: ignore
    action: str = inputJson["action"]
    configName: str = inputJson.get("configName", dhMan.selectedConfig)
    inputArgs: dict = inputJson.get("args", {})
    trace = []
    args = None  # output args
    fsm_steps = []

    if data is None and dhMan[configName].autoRestarting :
        dhMan[configName].abortAutoRestart = True
        

    previousState = dhMan[configName].configState

    if configName not in dhMan:
        logger.debug(f"Creating DaqHandler for {configName}")
        dhMan.add_daqHandler(configName)

    dhMan[configName].inTransition = True  # acts like a lock for the daqHandler
    dhMan[configName].configState = ConfigState.IN_TRANSITION
    socketio.emit(SocketioEvent.CONFIG_STATE_CHANGE, ConfigState.IN_TRANSITION, to=configName)
    logger.debug(f"Action {action} for {configName}")
    if action == Action.INITIALISE:
        fsm_steps = [Command.ADD, Command.CONFIGURE]
        try:
            treeChange = dhMan[configName].reload_config()
            if treeChange:
                socketio.emit(
                    SocketioEvent.TREE_CHANGE,
                    {
                        "configName": configName,
                        "tree": dhMan[configName].tree,
                        "states": dhMan[configName].getStates() ,
                        "fsm_rules": dhMan[configName].get_fsm(),
                        "externalScripts": dhMan[configName].externalScripts,
                    },
                    to=configName
                )
            
            dhMan[configName].frozenConfig = dhMan[configName].config
            dhMan[configName].frozenTree = dhMan[configName].tree
            r = dhMan[configName].start_scripts()
        except Exception as e:
            logger.error(f"Error initialising {configName} : {e}")
            emit_snackbar("Warning", str(e), SnackbarLevel.WARNING)
        else:
            if len(r) != 0:
                rw.set_log_path(r_rcstates, configName, r)
        dhMan[configName].frozenTree = dhMan[configName].tree

    elif action == Action.START:
        fsm_steps = [Command.START]
        newRunType = inputArgs.get("runType", "Test")
        newRunComment = inputArgs.get("runComment", "Test")[:500]

        # run service
        newRunNumber = 1_000_000_000

        if not CONFIG["local_only"]:
            try:
                newRunNumber = send_start_to_runservice(
                    runType=newRunType,
                    runComment=newRunComment,
                    reqDict=inputArgs,
                    app=app,
                )
                newRunNumber = int(newRunNumber)
            except ValueError:
                logger.error(f"Error getting run number: {newRunNumber}")
                dhMan[configName].configState = previousState
                dhMan[configName].inTransition = False 
                socketio.emit(SocketioEvent.CONFIG_STATE_CHANGE, previousState, to=configName)
                return newRunNumber  # run service returned an error message instead of a run number.

        dhMan[configName].runNumber = newRunNumber
        dhMan[configName].runType = newRunType
        dhMan[configName].runComment = newRunComment

        socketio.emit(
            SocketioEvent.RUN_INFO_CHANGE,
            {"runType": newRunType, "runComment": newRunComment},
            to=configName,
        )

        dhMan[configName].runNumber = newRunNumber
        args = newRunNumber

    elif action == Action.STOP:
        fsm_steps = [Command.STOP]
        newRunType = inputArgs.get("runType", "Test")
        newRunComment = inputArgs.get("runComment", "Test")[:500]

        # run service

        if not CONFIG["local_only"]:
            send_stop_to_runservice(
                runType=newRunType, runComment=newRunComment, runNumber=dhMan[configName].runNumber, app=app
            )

        dhMan[configName].runType = newRunType
        dhMan[configName].runComment = newRunComment
        socketio.emit(
            SocketioEvent.RUN_INFO_CHANGE,
            {"runType": newRunType, "runComment": newRunComment},
            to=configName,
        )

    elif action == Action.SHUTDOWN:
        fsm_steps = [Command.UNCONFIGURE, Command.SHUTDOWN, Command.REMOVE]
        if previousState not in [ConfigState.READY, ConfigState.ERROR, ConfigState.IN_TRANSITION]:
            dhMan[configName].runComment = "Run was shutdown"
            socketio.emit(
                SocketioEvent.RUN_INFO_CHANGE,
                {"runType": dhMan[configName].runType, "runComment": dhMan[configName].runComment},
                to=configName,
            )
            if not CONFIG["local_only"] :
                send_stop_to_runservice(
                    runType=dhMan[configName].runType,
                    runComment=dhMan[configName].runComment,
                    runNumber=dhMan[configName].runNumber,
                    forceShutdown=True,
                    app=app,
                )
        try : 
            dhMan[configName].stop_scripts()
        except Exception as e : 
            logger.warning(f"While stopping scripts : {e}")

    elif action == Action.PAUSE:
        fsm_steps = [Command.DISABLE_TRIGGER]

    elif action == Action.RESUME:
        fsm_steps = [Command.ENABLE_TRIGGER, Command.RESUME]

    elif action == Action.ECR:
        fsm_steps = [Command.ECR]
    try:
        for step in fsm_steps:
            rv = dhMan[configName].execute_command(step, arg=args)
            if not rv[1]:
                logger.warning(f"Command {step} returned {json.dumps(rv[0])}")
            trace.append((step, rv))
            if step == Command.ADD and len(rv[0]) != 0:  # length condition is just to be sure
                rw.set_log_path(r_rcstates, configName, rv[0])
            sleep(2)  # give time to modules to update its state
    except Exception as e:
        logger.error(f"Error executing action {action} for {configName} : {e}")
        dhMan[configName].configState = previousState
    else:
        
        next_state = {
            Action.INITIALISE: (
                
                ConfigState.READY
                if len(dhMan[configName].bad_modules()[1]) == 0
                else ConfigState.ERROR
            ),
            Action.START: ConfigState.RUN,
            Action.STOP: ConfigState.READY,
            Action.SHUTDOWN: ConfigState.DOWN,
            Action.PAUSE: ConfigState.PAUSED,
            Action.RESUME: ConfigState.RUN,
            Action.ECR: ConfigState.PAUSED,
        }[action]
        dhMan[configName].configState = next_state
        socketio.emit(SocketioEvent.CONFIG_STATE_CHANGE, next_state, to=configName)

    dhMan[configName].inTransition = False
    return {"trace": trace, "args": args}
