#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from datetime import datetime
import logging
import time
from . import custom
from flask import Response, current_app, request
import redis
from ..utils.constants import ConfigState


logger = logging.getLogger("daqControl")


@custom.route("/metrics/stream/<string:module>")
def metrics_module(module: str):
    """
    Gets the informations about a module in the redis database
    Returns a stream objects {"key": ..., "value": ..., "time": ... (string)} as a stream.
    """
    refreshSec = 1
    r_metrics = current_app.extensions["daqControl"].r_metrics 
    def event_stream():
        while True:
            keys = list(r_metrics.hkeys(module))  # type: ignore
            if keys == []:
                yield f"data: {[]}\n\n"
                time.sleep(refreshSec)
                continue
            keys.sort()
            vals = list(r_metrics.hmget(module, keys=keys))  # type: ignore
            info = [
                {
                    "key": key,
                    "value": val.split(":")[1],
                    "time": datetime.fromtimestamp(
                        int(float(val.split(":")[0]))
                    ).strftime("%a %d/%m/%y %H:%M:%S "),
                }
                for key, val in zip(keys, vals)
            ]
            yield f"data: {info}\n\n"
            time.sleep(refreshSec)

    return Response(event_stream(), mimetype="text/event-stream")


@custom.route("/monitoring/stream")
def monitoring_stream():
    """
    Returns a stream of the monitoring data (Physics, Calibration, Monitoring )
    """
    r_metrics = current_app.extensions["daqControl"].r_metrics 

    def split_data(data):
        timestamps = []
        values = []
        for d in data:
            a = d.split(":")
            timestamps.append(float(a[0]))
            if float(a[1]) < 0 : 
                a[1] = 0
            values.append(float(a[1]))
        return [timestamps, values]

    def format_run_start(runStart: float):
        return (
            datetime.fromtimestamp(runStart).strftime("%d/%m %H:%M:%S")
            if runStart != 0
            else "-"
        )

    def stream():
        
        sourceModule = "eventbuilder01"

        data = {"graph": {}, "other": {}}
        types = ["Physics", "Calibration", "TLBMonitoring"]
        for t in types:
            data["graph"][t] = split_data(
                r_metrics.lrange(f"History:{sourceModule}_Event_rate_{t}", -10, -1)
            )
            data["other"]["Events_" + t] = str(
                r_metrics.hget(sourceModule, f"Events_sent_{t}")
            ).split(":")[1]
        # activeConfig = dhMan.activeConfigs[0] # should be a list with only one element since FASER works in single run mode.

        data["other"].update(
            {
                "runNumber": str(r_metrics.hget(sourceModule, "RunNumber")).split(":")[
                    1
                ],
                "runStart": format_run_start(
                    float(str(r_metrics.hget(sourceModule, "RunStart")).split(":")[1])
                ),
            }
        )
        yield f"data: {data}\n\n"
        time.sleep(1)
        while True:
            for t in types:
                data["graph"][t] = split_data(
                    r_metrics.lrange(f"History:eventbuilder01_Event_rate_{t}", -1, -1)
                )
                data["other"]["Events_" + t] = str(
                    r_metrics.hget(sourceModule, f"Events_sent_{t}")
                ).split(":")[1]

            data["other"].update(
                {
                    "runNumber": str(r_metrics.hget(sourceModule, "RunNumber")).split(
                        ":"
                    )[1],
                    "runStart": format_run_start(
                        float(
                            str(r_metrics.hget(sourceModule, "RunStart")).split(":")[1]
                        )
                    ),
                }
            )
            yield f"data: {data}\n\n"
            time.sleep(1)

    return Response(stream(), mimetype="text/event-stream")


@custom.route("/globalState", methods=["GET"])
def api_state(): 
    rv = {}
    dhMan = current_app.extensions["daqControl"].dhManager
    CONFIG = current_app.config["daqControl"]
    r_metrics:redis.Redis = current_app.extensions["daqControl"].r_metrics

    loadedConfig = dhMan.selectedConfig
    configState = dhMan[loadedConfig].configState
    runOngoing = configState == ConfigState.RUN
    whoInterlocked = dhMan[loadedConfig].whoInterlocked 
    badModules = dhMan[loadedConfig].bad_modules()[1]
    localOnly  = CONFIG["local_only"]
    runType = dhMan[loadedConfig].runType
    runComment = dhMan[loadedConfig].runComment
    runNumber = dhMan[loadedConfig].runNumber
    try : 
        runStart = float(str(r_metrics.hget("eventbuilder01", "RunStart")).split(":")[1])

    except ValueError : 
        runStart = 0
    
    rv.update({
        "loadedConfig": loadedConfig,
        "runState": configState,
        "runOngoing": runOngoing,
        "whoInterlocked": whoInterlocked,
        "crashedM": badModules,
        "localOnly": localOnly,
        "runType": runType,
        "runComment": runComment,
        "runNumber": runNumber,
        "runStart": runStart
    })

    return rv


@custom.route("/moduleInfo", methods=["GET"])
def get_modules_infos(): 
    r_metrics = current_app.extensions["daqControl"].r_metrics
    module = request.args["module"]
    r = r_metrics.hgetall(module)
    return {k : v.split(":")[1] for k, v in r.items()}

 
    