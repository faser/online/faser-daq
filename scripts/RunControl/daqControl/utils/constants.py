#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#


class SnackbarLevel:
    """
    Avaible types for GUI snackbars
    """

    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"
    SUCCESS = "SUCCESS"


class SocketioEvent:
    EMIT_SNACKBAR = "emit_snackbar"
    CHANGED_INTERLOCK = "changed_interlock"
    CONFIG_STATE_CHANGE = "config_state_change"
    NODE_STATE_CHANGE = "node_state_change"
    EMIT_LOG = "log_emit"
    RUN_INFO_CHANGE = "run_info_change"
    CONFIG_CHANGE = "config_change"

    TREE_CHANGE = "tree_change"


class Command:
    ADD = "add"
    BOOT = "boot"
    REMOVE = "remove"
    CONFIGURE = "configure"
    UNCONFIGURE = "unconfigure"
    START = "start"
    STOP = "stop"
    PAUSE = "pause"
    SHUTDOWN = "shutdown"
    INCLUDE = "include"
    EXCLUDE = "exclude"
    RESUME = "resume"

    # FASER specific
    DISABLE_TRIGGER = "disableTrigger"
    ENABLE_TRIGGER = "enableTrigger"
    ECR = "ECR"


class Action:
    """
    General actions defined for FASER
    """

    INITIALISE = "INITIALISE"
    SHUTDOWN = "SHUTDOWN"
    START = "START"
    STOP = "STOP"
    PAUSE = "PAUSE"
    RESUME = "RESUME"
    ECR = "ECR"


class NodeState:
    """
    Possible states of a node
    """

    NOT_ADDED = "not_added"
    ADDED = "added"
    BOOTED = "booted"
    READY = "ready"
    RUNNING = "running"
    PAUSED = "paused"
    ERROR = "error"


class ConfigState:
    """
    Possible general states of the Run Control GUI.
    """

    DOWN = "DOWN"
    READY = "READY"
    RUN = "RUN"
    PAUSED = "PAUSED"
    IN_TRANSITION = "IN TRANSITION"
    ERROR = "ERROR"


class StatusFlag:
    """
    Possible status of a node
    """

    OK = 0
    WARNING = 1
    ERROR = 2


class RedisName:
    """
    Hardcoded redis variables names
    """

    WHO_INTERLOCKED = "whoInterlocked"
    CONFIG_STATE = "configState"
    LOG_PATH = "logPaths"
    RUN_NUMBER = "runNumber"
    CONFIG = "config"
    TREE = "tree"

    RUN_TYPE = "runType"
    RUN_COMMENT = "runComment"

    # in metrics DB
    MODULE_STATUS = "Status"
