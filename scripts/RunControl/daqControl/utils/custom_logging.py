#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

import logging
from .constants import SocketioEvent
from flask_socketio import SocketIO
from logging.config import dictConfig
import json
import datetime as dt

LOG_RECORD_BUILTIN_ATTRS = {
    "args",
    "asctime",
    "created",
    "exc_info",
    "exc_text",
    "filename",
    "funcName",
    "levelname",
    "levelno",
    "lineno",
    "module",
    "msecs",
    "message",
    "msg",
    "name",
    "pathname",
    "process",
    "processName",
    "relativeCreated",
    "stack_info",
    "thread",
    "threadName",
    "taskName",
}


class SocketIOHandler(logging.Handler):
    def __init__(self, socketio: SocketIO, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.socketio = socketio

    def emit(self, record):
        msg = self.format(record)
        self.socketio.emit(SocketioEvent.EMIT_LOG, json.loads(msg))
        # super().emit(record)


class JSONFormatter(logging.Formatter):
    def __init__(self, *, fmt_keys):
        super().__init__()
        self.fmt_keys = fmt_keys if fmt_keys is not None else {}

    def format(self, record: logging.LogRecord) -> str:
        message = self._prepare_log_dict(record)
        return json.dumps(message, default=str)

    def _prepare_log_dict(self, record: logging.LogRecord):
        always_fields = {
            "message": record.getMessage(),
            "timestamp": dt.datetime.fromtimestamp(record.created, tz=dt.timezone.utc).isoformat(),
        }
        if record.exc_info is not None:
            always_fields["exc_info"] = self.formatException(record.exc_info)

        if record.stack_info is not None:
            always_fields["stack_info"] = self.formatStack(record.stack_info)

        message = {
            key: (
                msg_val
                if (msg_val := always_fields.pop(val, None)) is not None
                else getattr(record, val)
            )
            for key, val in self.fmt_keys.items()
        }
        message.update(always_fields)

        for key, val in record.__dict__.items():
            if key not in LOG_RECORD_BUILTIN_ATTRS:
                message[key] = val

        return message


def setup_logging(socketio: SocketIO, log_path: str) -> None:
    dictConfig(
        {
            "version": 1,
            "formatters": {
                "simple": {
                    "format": "[%(asctime)s] [%(levelname)s] [%(name)s] [%(pathname)s-l.%(lineno)d] - %(message)s"
                },
                "json": {
                    "()": "daqControl.utils.custom_logging.JSONFormatter",
                    "fmt_keys": {
                        "level": "levelname",
                        "message": "message",
                        "timestamp": "timestamp",
                        "logger": "name",
                    },
                },
            },
            "handlers": {
                "file": {
                    "class": "logging.handlers.RotatingFileHandler",
                    "filename": log_path,
                    "maxBytes": 1000000,
                    "backupCount": 0,
                    "level": "INFO",
                    "formatter": "json",
                },
                "socketio": {
                    "()": "daqControl.utils.custom_logging.SocketIOHandler",
                    "socketio": socketio,
                    "level": "INFO",
                    "formatter": "json",
                },
                "socketio_flask": {
                    "()": "daqControl.utils.custom_logging.SocketIOHandler",
                    "socketio": socketio,
                    "level": "ERROR",
                    "formatter": "json",
                },
                "console": {
                    "class": "logging.StreamHandler",
                    # "stream": "ext://flask.logging.wsgi_errors_stream",
                    "stream": "ext://sys.stdout",
                    "formatter": "simple",
                    "level": "DEBUG",
                },
            },
            "loggers": {
                "wsgi": {
                    "level": "DEBUG",
                    "handlers": ["console", "socketio_flask"],
                    "propagate": False,
                },
                # "root": {"level": "DEBUG", "handlers": ["console", "file", "socketio"]},
                "daqControl": {
                    "level": "DEBUG",
                    "handlers": ["console", "file", "socketio"],
                    "propagate": True,
                },
            },
        }
    )
