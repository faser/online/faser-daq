#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from typing import Union
from redis import Redis
from .constants import RedisName, ConfigState


def format_key(ID: str, name: str) -> str:
    """Format the key for the redis database
    Args:
        ID: the ID of the configuration (the configuration name)
        name: the name of the key

    Returns:
        str: the formatted key
    """
    return f"{ID}:{name}"


def who_interlocked(r: Redis, ID: str):
    """If there is no interlock on that config, it will return None"""
    return r.get(format_key(ID, RedisName.WHO_INTERLOCKED))


def set_interlock(r: Redis, ID: str, who: str, durationSec: int):
    """Interlock the configuration <ID> with user <who> for <durationSec> amount of time."""
    r.set(format_key(ID, RedisName.WHO_INTERLOCKED), who, ex=durationSec)


def remove_interlock(r: Redis, ID: str):
    r.delete(format_key(ID, RedisName.WHO_INTERLOCKED))


def extend_interlock(r: Redis, ID: str, durationSec: int):
    """Extends the interlock for a given amount of time"""
    r.expire(format_key(ID, RedisName.WHO_INTERLOCKED), durationSec)


def get_run_type(r: Redis, ID: str):
    return r.get(format_key(ID, RedisName.RUN_TYPE)) or ""


def set_run_type(r: Redis, ID: str, runType: str):
    r.set(format_key(ID, RedisName.RUN_TYPE), runType)


def get_run_comment(r: Redis, ID: str):
    return r.get(format_key(ID, RedisName.RUN_COMMENT)) or ""


def set_run_comment(r: Redis, ID: str, runComment: str):
    r.set(format_key(ID, RedisName.RUN_COMMENT), runComment)


def get_config_state(r: Redis, ID: str):
    return r.get(format_key(ID, RedisName.CONFIG_STATE)) or ConfigState.DOWN


def set_config_state(r: Redis, ID: str, state: str):
    r.set(format_key(ID, RedisName.CONFIG_STATE), state)


def get_active_configs(r: Redis) -> list[str]:
    """Returns all the active configurations (i.e. the ones that are not in the DOWN state)"""
    keys = r.keys("*:configState")
    states = r.mget(keys)  # type: ignore
    return [keys[i].split(":")[0] for i, state in enumerate(states) if state != ConfigState.DOWN]  # type: ignore


def set_log_path(r: Redis, ID: str, logList: list):
    """Set the log paths stored in logList
    logList is of the form :
    [
        (<module_name>, (<host>, <logPath>)),
        ...
    ]
    """
    r.hset(
        format_key(ID, RedisName.LOG_PATH),
        mapping={val[0]: val[1][1] for val in logList},
    )


def get_log_path(r: Redis, config: str, moduleOrscript: str):
    """Return the log path for a specified script or module"""
    return r.hget(format_key(config, RedisName.LOG_PATH), moduleOrscript)


def get_run_number(r: Redis, ID: str):
    return r.get(format_key(ID, RedisName.RUN_NUMBER))


def set_run_number(r: Redis, ID: str, runNumber: int):
    r.set(format_key(ID, RedisName.RUN_NUMBER), runNumber)


def get_selected_config(r: Redis) -> Union[str, None]:
    return r.get("selectedConfig")  # type: ignore


def set_selected_config(r: Redis, configName: str):
    r.set("selectedConfig", configName)


def get_frozen_tree(r: Redis, configName: str):
    return r.get(format_key(configName, RedisName.TREE))


def set_frozen_tree(r: Redis, configName: str, tree: str):
    r.set(format_key(configName, RedisName.TREE), tree)


def get_module_status(r: Redis, module: str):
    """FASER specific"""
    return r.hget(RedisName.MODULE_STATUS, module) or 0


def get_frozen_config(r: Redis, configName: str):
    return r.get(format_key(configName, RedisName.CONFIG))


def set_frozen_config(r: Redis, configName: str, config: str):
    r.set(format_key(configName, RedisName.CONFIG), config)
