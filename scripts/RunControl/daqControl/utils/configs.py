#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

import json
import jsonref
import jsonschema
from pathlib import Path
from copy import deepcopy
import logging

logger = logging.getLogger("daqControl")



def corresponding_type(longType:str):
    """
    Returns the short version type of the module
    """
    shortType = ""
    if "monitor" in longType.lower(): shortType = "monitor"
    elif "archiver" in longType.lower(): shortType = "monitor"
    elif "filewriter" in longType.lower() : shortType = "FW"
    elif "eventbuilder" in longType.lower() : shortType = "EB"
    elif "receiver" in longType.lower() and "trigger" not in longType.lower() and "tracker" not in longType.lower() and "digitizer" not in longType.lower(): shortType = "receiver"
    elif "emulator" in longType.lower(): shortType = "emulator"
    elif "triggergenerator" in longType.lower(): shortType = "TG"
    elif "triggerreceiver" in longType.lower(): shortType = "TR"
    elif "trackerreceiver" in longType.lower() or "digitizer" in longType.lower() : shortType = "TKR"
    elif "readoutinterface" in longType.lower() : shortType = "receiver"
    elif "eventplayback" in longType.lower() : shortType = "EB"
    elif "compressionengine" in longType.lower() : shortType = "COE"
    else :  raise RuntimeError(f"{longType} : not a known type, please add it")
    return shortType


def validate_config(configName: str, configsDirPath: str):
    """
    configName : str : name of the configuration
    configsDirPath : str : path to the directory containing the configuration files
    NOTE : could be more general 
    """
    dirPath = Path(configsDirPath)
    with open(dirPath / (configName + ".json"), "r") as f:
        base_dir_uri = Path(configsDirPath).as_uri() + "/"
        loader = jsonref.jsonloader 
        loader.cache_results = False
        jsonref_obj = jsonref.load(f, base_uri=base_dir_uri, loader=loader)
        # json_obj = json.load(f)
    # print(jsonref_obj)
    configuration: dict = deepcopy(jsonref_obj.get("configuration", jsonref_obj))  # type: ignore

    with open(dirPath / "schemas" / "validation-schema.json", "r") as f: 
        json_obj = json.load(f)
    try : 
        jsonschema.validate(configuration, json_obj)
    except jsonschema.ValidationError as e :
        logger.error(f"Error while configuring validating validation-schema.json:  {e}")
        raise jsonschema.ValidationError(f"Error while configuring validating validation-schema.json:  {e}")

    for component in configuration["components"]:
        for module in component["modules"]:
            with open(dirPath / "schemas" / f"{module['type']}.schema", "r") as f: 
                moduleSchem = json.load(f)
            try: 
                jsonschema.validate(module, moduleSchem)
            except jsonschema.ValidationError as e : 
                logger.error(f"Error while validating config for {module['name']} : {e}")
                raise jsonschema.ValidationError(f"Error while validating config for {module['name']} : {e}")



def create_config_folder(configName: str , configsDirPath: str ):
    configDirPath = Path(configsDirPath) / configName
     
    tree = {"name": "Root", "children": []}
    sort = {}
    
    with open(Path(configsDirPath) / (configName + ".json"), "r") as f:
        base_dir_uri = Path(configsDirPath).as_uri() + "/"
        loader = jsonref.jsonloader
        loader.cache_results = False
        jsonref_obj = jsonref.load(f, base_uri=base_dir_uri, loader=loader)

    configuration: dict = deepcopy(jsonref_obj.get("configuration", jsonref_obj))  # type: ignore

    for comp in configuration["components"]:
        if comp["modules"][0]["type"] in sort.keys():
            sort[comp["modules"][0]["type"]].append(comp["modules"][0]["name"])
        else:
            sort[comp["modules"][0]["type"]] = []
            sort[comp["modules"][0]["type"]].append(comp["modules"][0]["name"])
        
    for key, item in sorted(sort.items()):
        if len(item) == 1:
            cat = {"name": item[0], "types": [{"type": corresponding_type(key)}]}
            tree["children"].append(cat)
            continue
        cat = {"name": key, "types": [{"type": corresponding_type(key)}], "children": []}

        for comp in item:
            cat["children"].append({"name": comp})
        tree["children"].append(cat)
    
    configDirPath.mkdir(exist_ok=True) 
    with open(configDirPath / "control-tree.json", "w") as f:
        jsonref.dump(tree, f, indent=4, sort_keys=True) 

    with open(configDirPath / "config-dict.json", "w") as f:
        jsonref.dump(
            {
                "tree": "control-tree.json",
                "fsm_rules": "../fsm-rules.json",
                "config": f"../{configName}.json",
                "grafana": "../grafana.json",
            },
            f, indent=4
        )
    

def validate_serverconfig(config:dict, schema:dict) -> None:
    """
    Validate the server configuration with the schema
    """
    jsonschema.validate(config, schema)
    

if __name__ == "__main__":
    from os import environ as env
    from jsonschema.exceptions import ValidationError
    try : 
        validate_config("digitizerTI12", env["DAQ_CONFIG_DIR"] )
    except ValidationError as e : 
        print(e)
