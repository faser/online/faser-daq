#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

import time
from flask import current_app
import logging
from .utils.constants import SocketioEvent, Action, ConfigState
import re
from .custom.faser_control import execute_action
from .faser_Daqhandler import FASERDaqHandler
from .dhManager import DhManager
from .utils.mattermost import MattermostAlert, error


def stateChecker(app) -> None:
    """
    Background job when there are daqHandlers to watch for.
    """
    logger = logging.getLogger("daqControl")
    with app.app_context():
        dhMan: DhManager = current_app.extensions["daqControl"].dhManager
        socketio = current_app.extensions["socketio"]
        CONFIG = current_app.config["daqControl"]

    states = {}
    toRemove = []

    mattNotif_crash = MattermostAlert(
        message_template=":warning: Module __{}__ has CRASHED",
        time_interval=CONFIG["persistent_notification_delay"],
        okAlerts=CONFIG["ok_alerts"],
    )
    mattNotif_error = MattermostAlert(
        message_template=":warning: Module __{}__ is in ERROR STATE",
        time_interval=CONFIG["persistent_notification_delay"],
        okAlerts=CONFIG["ok_alerts"],
    )

    while True:
        for configName, daqHand in dhMan.daqHandlers.items():
            latestStates = daqHand.getStates()
            changed = False
            try:
                changed = states[configName] != latestStates
            except KeyError:  # first loop
                states[configName] = latestStates

            if changed:
                socketio.emit(SocketioEvent.NODE_STATE_CHANGE, latestStates, to=configName)
                states[configName] = latestStates

                errorM, crashedM = daqHand.bad_modules()
                mattNotif_crash.check(crashedM)
                mattNotif_error.check(errorM)

                if (
                    CONFIG["automatic_restart"]
                    and daqHand.configState == ConfigState.RUN
                    and not daqHand.autoRestarting
                    and daqHand.runType == "Physics"
                ):
                    mod = ""
                    for crashedModule in crashedM:
                        if re.match(
                            CONFIG["automatic_restart_regex_crash"],
                            crashedModule,
                            re.IGNORECASE,
                        ):
                            mod = crashedModule
                            res = mod + " crashed. "
                            logger.error(res)
                            socketio.start_background_task(
                                automatic_restart, daqHand=daqHand, app=app, reason=res
                            )
                            break
                    if mod == "":
                        for errorModule in errorM:
                            # check if badModuleName matches the regex pattern ignore case
                            if re.match(
                                CONFIG["automatic_restart_regex_error"],
                                errorModule,
                                re.IGNORECASE,
                            ):
                                mod = errorModule
                                res = mod + " is in error state."
                                logger.error(res)
                                socketio.start_background_task(
                                    automatic_restart, daqHand=daqHand, app=app, reason=res
                                )
                                break
            mattNotif_crash.send_reminders()
            mattNotif_error.send_reminders()

            if configName != dhMan.selectedConfig and daqHand.elapsedTimeSinceLastCommand > CONFIG["time_before_remove_idle_config"] and daqHand.configState == ConfigState.DOWN:  # type: ignore
                toRemove.append(configName)

        for c in toRemove:
            logger.info("Removing idle config: " + c)
            dhMan.remove_daqHandler(c)
        toRemove.clear()
        socketio.sleep(0.3)  # type: ignore


def automatic_restart(daqHand: FASERDaqHandler, app, reason, time_secs_between_actions: int = 2):
    """Defines the routine for restarting a new run after crash of regex matching modules
    Each action has four elements :
    - the name of the action
    - The data to pass with the action
    - The required final state
    - The timeout associated with the action

    The logic is the following :
    - Action stop : if successfull shutdown and then : initialise, start.
    - if actions not sccessfull, force shutdown and restart from initialise
    """
    logger = logging.getLogger("daqControl")
    error("Automatic restart of the run : " + reason)
    daqHand.autoRestarting = True
    with app.app_context():
        socketio = current_app.extensions["socketio"]
        socketio.sleep(5)  # 5 seconds before the automatic restart starts
        ordered_actions = [
            (Action.INITIALISE, None, ConfigState.READY, 45),
            (
                Action.START,
                {
                    "runType": daqHand.runType,  # type: ignore
                    "runComment": "Automatic restart after : " + reason,
                },
                ConfigState.RUN,
                30,
            ),
            (
                Action.STOP,
                {
                    "runType": daqHand.runType,  # type: ignore
                    "runComment": "Automatic stop : " + reason,
                },
                ConfigState.READY,
                30,
            ),
            (Action.SHUTDOWN, None, ConfigState.DOWN, 45),
        ]
        step = 2  # first step to do is shutdown.
        while not daqHand.abortAutoRestart:
            force = False
            action = ordered_actions[step]
            logger.info(f"[Automatic restart] Executing action : {action[0]}")
            socketio.start_background_task(
                execute_action,
                data={"action": action[0], "configName": daqHand.configName, "args": action[1]},
                app=app,
            )
            startedTime = time.time()
            while daqHand.configState != action[2]:
                if time.time() - startedTime > action[3]:  # timeout associated with the action.
                    logger.warning(
                        f"[Automatic restart] (timeout) Failed to : {action[0]}. Force shutting down..."
                    )
                    force = True
                    break
                socketio.sleep(1)
            if force:  # if shutdown is due to failure of another action.
                step = 3
                continue  # go to shutdown

            if step == 3 and not force:  # if shutdown succeeded
                step = 0  # go to initialize step

            elif step == 1:  # started successfully
                break
            else:
                step += 1  # go to next step

            if (
                daqHand.abortAutoRestart
            ):  # if manual command is used (a fallback, the while condition should be enough)
                daqHand.abortAutoRestart = False
                break
            socketio.sleep(time_secs_between_actions)
    daqHand.autoRestarting = False
    return
