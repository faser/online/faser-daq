#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from copy import deepcopy
import json
from pathlib import Path
import time
from typing import Union
import jsonref
from os import environ as env
from anytree import RenderTree

from nodetree import NodeTree
from anytree.search import find_by_attr
from anytree.importer import DictImporter
from daqcontrol import daqcontrol as daqctrl
from .utils.configs import create_config_folder, validate_config
from .utils.constants import NodeState, Command, ConfigState
from .utils import redis_wrappers as rw
from redis import Redis
from os import environ as env
from os.path import getmtime


class DaqHandler:
    """
    Class handles the loaded Config
    """

    @property
    def frozenTree(self):
        r = rw.get_frozen_tree(self.redisStateDB, self.configName)
        return json.loads(r) if r is not None else None  # type: ignore

    @frozenTree.setter
    def frozenTree(self, tree: dict):
        rw.set_frozen_tree(self.redisStateDB, self.configName, json.dumps(tree))

    @property
    def frozenConfig(self):
        r = rw.get_frozen_config(self.redisStateDB, self.configName)
        return json.loads(r) if r is not None else None  # type: ignore

    @frozenConfig.setter
    def frozenConfig(self, config: dict):
        rw.set_frozen_config(self.redisStateDB, self.configName, json.dumps(config))

    @property
    def externalScripts(self):
        return [s["name"] for s in self.config.get("scripts", [])]  # type: ignore

    @property
    def runNumber(self):
        """
        Return an int representing the current run number.
        If no run number is found, returns None.
        """
        return rw.get_run_number(self.redisStateDB, self.configName)

    @runNumber.setter
    def runNumber(self, runNumber: int):
        """
        Set the run number.
        """
        rw.set_run_number(self.redisStateDB, self.configName, runNumber)

    @property
    def configState(self) -> str:
        return str(rw.get_config_state(self.redisStateDB, self.configName))

    @configState.setter
    def configState(self, configState: str):
        # TODO : PUBSUB if change
        rw.set_config_state(self.redisStateDB, self.configName, configState)

    @property
    def whoInterlocked(self):
        """Shortcut to get who is currently interlocking the configuration."""
        return rw.who_interlocked(self.redisStateDB, self.configName)

    @property
    def leaves(self):
        return self.nodeTree.leaves

    @property
    def elapsedTimeSinceLastCommand(self):
        """Returns elpased time since last Command in seconds"""
        return time.time() - self.timeLastExecutedCommand

    def __init__(
        self,
        configsDir: Path,
        configName: str,
        redis_state_db: Redis,
    ):
        """
        configDir : directory where all the configs are (generally env['DAQ_CONFIG_DIR'])
        configName : name of the configuration
        redis_state_db : redis database for the states of the RCGUI
        """
        self.redisStateDB = redis_state_db
        self.configsDir = configsDir
        self.configName = configName
        self.inTransition = False
        self.timeLastExecutedCommand = time.time()
        self.autoRestarting = False
        self.abortAutoRestart = False

        self.__init_config()

    def __init_config(self, force=False):

        configPath = self.configsDir / self.configName
        structureChanged = False
        if not (configPath).exists():
            try : 
                validate_config(self.configName, str(self.configsDir))
            except Exception as e : 
                raise Exception(f"Error : validating {self.configName}: {e}")
            create_config_folder(self.configName, str(self.configsDir))

        with (configPath / "config-dict.json").open("r") as f:
            configIndex = json.load(f)

        if self.configState == ConfigState.DOWN or force:
            try : 
                validate_config(self.configName, str(self.configsDir))
            except Exception as e : 
                raise Exception(f"Error : validating config {self.configName}: {e}")
            if getmtime(configPath / configIndex["config"]) > getmtime(
                configPath / configIndex["tree"]
            ):  # config structure has been modified.
                create_config_folder(self.configName, str(self.configsDir))
                structureChanged = True

            with (configPath / configIndex["tree"]).open("r") as f:
                self.tree = json.load(f)

            with (configPath / configIndex["config"]).open("r") as f:
                loader = jsonref.jsonloader
                loader.cache_results = False
                jsonref_obj = jsonref.load(
                    f, base_uri=self.configsDir.as_uri() + "/", loader=loader
                )
                self.config = deepcopy(jsonref_obj.get("configuration", jsonref_obj))  # type: ignore

        else:
            self.tree = self.frozenTree
            self.config = self.frozenConfig

        with (configPath / configIndex["fsm_rules"]).open("r") as f:
            self.fsmRules = json.load(f)

        build_dir = self.config.get("path", env["DAQ_BUILD_DIR"])  # type: ignore

        lib_path = (
            "LD_LIBRARY_PATH="
            + env["LD_LIBRARY_PATH"]
            + ":"
            + build_dir
            + "/lib/,TDAQ_ERS_STREAM_LIBS=DaqlingStreams"
        )
        self.dc = daqctrl(self.config["group"])  # type: ignore

        importer = DictImporter(nodecls=NodeTree)  # type: ignore

        self.nodeTree = importer.import_(self.tree)
        state_action = self.fsmRules["fsm"]
        order_rules = self.fsmRules["order"]

        for _, _, node in RenderTree(self.nodeTree):
            for c in self.config["components"]:  # type: ignore
                if node.name == c["name"]:  # type: ignore
                    node.configure(
                        order_rules,
                        state_action,
                        pconf=c,
                        exe="/bin/daqling",
                        dc=self.dc,
                        dir=build_dir,
                        lib_path=lib_path,
                    )
                else:
                    node.configure(order_rules, state_action)
            node.startStateCheckers()
        return structureChanged

    def reload_config(self):
        if self.nodeTree is not None:
            for _, _, node in RenderTree(self.nodeTree):
                node.stopStateCheckers()
        return self.__init_config(force=True)

    def __del__(self):
        # if exception occurred during  __init__ , nodeTree is still None
        if self.nodeTree is not None:
            for _, _, node in RenderTree(self.nodeTree):
                node.stopStateCheckers()

    def execute_command(self, command, node_name: Union[None, str] = None, arg=None):
        output = []
        nodeName = node_name
        if node_name is None:
            nodeName = self.nodeTree.name  # type: ignore
        node: NodeTree = find_by_attr(self.nodeTree, nodeName)  # type: ignore
        res = None
        if command == Command.INCLUDE:
            node.exclude()
            output = [None, True]  # (First element is the return values, second, is it succeeded, not really useful in this case)
        elif command == Command.EXCLUDE:
            node.include()
            output = [None, True]
        else:
            res = node.executeAction(command, arg=arg)  # type: ignore
            leaves_name = [n.name for n in node.leaves if n.included]
            output = self.__parse_command_output(command, leaves_name, res)
        self.timeLastExecutedCommand = time.time()
        return output

    def __parse_command_output(self, command, modulesName, output) -> list:
        """
        Handles, processes and formats the result given by daqtree.
        Several types of outputs are possible :
        - from command <add> : log location if processes added correctly.
        - from command <remove> : returns None if removed correctly.
        - from other commands : returns "Success" if exectuted command correctly
        """
        rc = [None, True]  # (returned data, success ?)
        if not isinstance(output, list):
            flattenRes = [output]
        else:
            flattenRes = [
                item
                for sublist in output
                for item in (sublist if isinstance(sublist, list) else [sublist])
            ]
        check_func = None
        if command == Command.ADD:
            check_func = lambda r: isinstance(r, tuple)
            flattenRes = list(filter(lambda x: isinstance(x, tuple), flattenRes))
            rc[0] = list(zip(modulesName, flattenRes))
        elif command == Command.REMOVE:
            check_func = lambda r: r is None
        else:
            check_func = lambda r: r == "Success"
        m = list(map(check_func, flattenRes))
        if all(m):
            rc[1] = True
        else:  # if some problem occures
            # NOTE: indices = np.where(not m) more efficient for large lists.
            rc[1] = False
            rc[0] = [(modulesName[i], flattenRes[i]) for i, x in enumerate(m) if not x]
        return rc

    def start_scripts(self, scripts: list[dict] = None):  # type: ignore
        """
        Start the scripts in the configuration
        """
        rv = []
        scriptsToAdd = []
        if scripts is not None:
            scriptsToAdd = scripts
        elif "scripts" in self.config:  # type: ignore
            scriptsToAdd = self.config["scripts"]  # type: ignore

        try:
            rv = self.dc.addScripts(scriptsToAdd, env["DAQ_SCRIPT_DIR"])  # type: ignore
        except Exception as e:
            raise e
        scripts_name = [s["name"] for s in scriptsToAdd]  # type: ignore

        return list(zip(scripts_name, rv))

    def stop_scripts(self, scripts: list[dict] = None):  # type: ignore
        configsToStop = []
        if scripts is not None:
            configsToStop = scripts
        elif "scripts" in self.config:  # type: ignore
            configsToStop = self.config["scripts"]  # type: ignore

        self.dc.removeProcesses(configsToStop)  # type: ignore

    def getStates(self) -> dict:
        """
        Returns the state of all modules in the nodeTree
        """
        states = {}
        for _, _, node in RenderTree(self.nodeTree):
            states[node.name] = {
                "state": (
                    node.state if isinstance(node.state, str) else node.state[0]
                ),  # NOTE does not support multiple modules for one node yet.
                "included": node.included,
                "inconsistent": node.inconsistent,
                "status": self._module_status(node),
            }

        return states

    def crashed(self, node: NodeTree) -> bool:
        """
        Tell if a <node> is in a bad state or not.
        """
        if (
            not self.inTransition  # not during a transition
            and self.configState not in [ConfigState.IN_TRANSITION, ConfigState.DOWN]
            and
            # (self.configState == ConfigState.RUN)  # type: ignore
            node.is_leaf
            and (node.state[0] == NodeState.ADDED)
        ) or node.state[0] == NodeState.ERROR:
            return True
        return False

    def _module_status(self, node: NodeTree) -> int:
        """
        Returns the status of the module :
         - 0 : normal
         - 3 : crashed

        """
        return 0 if not self.crashed(node) else 3

    def get_fsm(self) -> dict:
        return self.fsmRules["fsm"]

    def bad_modules(self) -> list[str]:  # -> list[Any]:
        badModules = []
        for _, _, node in RenderTree(self.nodeTree):
            if node.included and self.crashed(node):
                badModules.append(node.name)
        return badModules
