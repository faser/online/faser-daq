#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from pathlib import Path

from redis import Redis
from typing import Union
from .daqhandler import DaqHandler
from .faser_Daqhandler import FASERDaqHandler
from .utils import redis_wrappers as rw


class DhManager:

    @property
    def activeConfigs(self):
        activeConfigs = list(rw.get_active_configs(self.redis_state_db))  # type: ignore
        if len(activeConfigs) != 1 and self.single_run_mode:
            raise Exception("There should be only one active configuration in single mode")
        return activeConfigs

    @property
    def selectedConfig(self) -> Union[str, None]:
        """Useful for the single run mode. It returns the selected configuration."""
        return rw.get_selected_config(self.redis_state_db)  # type: ignore

    @selectedConfig.setter
    def selectedConfig(self, configName: str):
        """Useful for the single run mode. It sets the selected configuration."""
        rw.set_selected_config(self.redis_state_db, configName)  # type: ignore

    def __init__(
        self,
        configsDir: str,
        redis_state_db: Redis,
        single_run_mode: bool = False,
        redis_metrics_db: Union[Redis, None] = None,
    ):
        self.configsDir = configsDir
        self.redis_state_db = redis_state_db
        self.daqHandlers: "dict[str, type(DaqHandler)]" = {}  # type: ignore
        self.single_run_mode = single_run_mode
        self.redis_metrics_db = redis_metrics_db

    def __getitem__(self, key):
        """Allows to access the DaqHandler object by its configName
        Ex. DhManager["configName"] will return the DaqHandler object for the configName
        """
        return self.daqHandlers.get(key, None)

    def add_daqHandler(self, configName: str):
        """Adds a new DaqHandler object to the dictionary of DaqHandlers"""
        # NOTE : For now, the custom daqHandler has to be changed here.
        self.daqHandlers[configName] = FASERDaqHandler(
            Path(self.configsDir), configName, self.redis_state_db, redis_metrics_db=self.redis_metrics_db  # type: ignore
        )

    def remove_daqHandler(self, configName: str):
        """Removes a DaqHandler object from the dictionary of DaqHandlers"""
        del self.daqHandlers[configName]

    def __iter__(self):
        return iter(self.daqHandlers)
