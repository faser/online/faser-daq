{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "RCGUI configuration",
    "description": "Configuration for the RCGUI web interface server",
    "type": "object",
    "properties": {
        "SSO_enabled": {
            "type": "boolean",
            "description": "Enable or disable CERN Single Sign On.",
            "default": true
        },
        "serverlog_location_name": {
            "type": "string",
            "description": "The location of the server log file",
            "default": "./log/webinterfaceserver.log"
        },
        "timeout_session_expiration_mins": {
            "type": "integer",
            "default": 120,
            "description": "The time in minutes after which a session will expire. (for SSO)",
            "minimum": 0
        },
        "timeout_interlock_sec": {
            "type": "integer",
            "default": 10,
            "description": "The time in seconds after which the interlock will be released.",
            "minimum": 1
        },
        "persistent_notification_delay": {
            "type": "integer",
            "default": 1800,
            "description": "The time interval in seconds between two alerts on Mattermost when a module is in a bad state.",
            "minimum": 0
        },
        "logout_url": {
            "type": "string",
            "description": "The URL to redirect to after logout",
            "default": "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout"
        },
        "redis_metrics_db": {
            "type": "integer",
            "description": "The number of the Redis database to use for metrics",
            "default": 0,
            "minimum": 0,
            "maximum": 15
        },
        "redis_rcstates_db": {
            "type": "integer",
            "description": "The number of the Redis database to use for RC states",
            "default": 4,
            "minimum": 0,
            "maximum": 15
        },
        "excluded_configs": {
            "type": "array",
            "description": "List of configurations to exclude from the web interface",
            "items": {
                "type": "string"
            }
        },
        "time_before_remove_idle_config": {
            "type": "integer",
            "description": "The time in seconds after which a configuration is removed from daqhandler array if it is idle",
            "default": 30,
            "minimum": 0
        },
        "automatic_restart" : {
            "type": "boolean",
            "description": "Enable or disable automatic restart of a run when a module matching the given regex has crashed.",
            "default": false
        },
        "automatic_restart_regex_crash" : {
            "type": "string",
            "description": "The regex to match the crashed module name for automatic restart (case insensitive)",
            "default": ".*receiver.*"
        },
        "automatic_restart_regex_error" : {
            "type": "string",
            "description": "The regex to match the module name with error status for automatic restart (case insensitive).",
            "default": ".*receiver.*"
        },
        "only_localhost_scripts" : {
            "type" : "boolean",
            "description": "If true, the routes with the 'only_locahost_scripts' decorator are only available for scripts executed from the same machine as the rcgui server.",
            "default" : "true"
        }
    }
}
