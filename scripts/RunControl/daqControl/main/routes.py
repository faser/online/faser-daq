#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

import logging
from pathlib import Path
import socket

from flask import render_template, request, current_app, session, Response
import json
from ..utils import redis_wrappers as rw
from ..utils.constants import ConfigState, SocketioEvent
from . import main
from collections import deque
from ..dhManager import DhManager
from ..auth.utils import only_localhost_scripts

logger = logging.getLogger("daqControl")


@main.route("/", methods=["GET", "POST"])
def index():
    CONFIG = current_app.config["daqControl"]

    return render_template("index.html", localOnly=CONFIG["local_only"])


@main.route("/configs", methods=["GET"])
def get_configs():
    CONFIG = current_app.config["daqControl"]
    r_rcstates = current_app.extensions["daqControl"].r_rcstates

    try:
        configs = [
            x.stem
            for x in Path(CONFIG["DAQ_CONFIG_DIR"]).iterdir()
            if (x.is_file() and x.name not in CONFIG["excluded_configs"])
        ]
        return [(config, config in rw.get_active_configs(r_rcstates)) for config in sorted(configs)]

    except Exception as e:
        return str(e), 500


@main.route("/selectConfig", methods=["GET"])
def select_config():
    CONFIG = current_app.config["daqControl"]
    dhMan: DhManager = current_app.extensions["daqControl"].dhManager
    socketio = current_app.extensions["socketio"]
    configName: str = request.args["configName"]
    initiator: bool = request.args.get(
        "initiator", type=lambda x: x.lower() == "true", default=False
    )

    if initiator:
        configsDir = Path(CONFIG["DAQ_CONFIG_DIR"])
        logger.debug(f"Selecting config {configName} from {configsDir}")

        if configName not in dhMan:
            logger.debug(f"Creating DaqHandler for {configName}")
            try:
                dhMan.add_daqHandler(configName)
            except Exception as e:
                logger.error(f"Error creating DaqHandler for {configName}: {e}")
                return "Error : " + str(e)

        elif dhMan[configName].configState == ConfigState.DOWN:
            try : 
                dhMan[configName].reload_config()
            except Exception as e  : 
                logger.error(f"Error reloading config {configName} : {e}")
                return f"Error : reloading config {configName} : " + str(e)

        if CONFIG["single_run_mode"]:
            socketio.emit(SocketioEvent.CONFIG_CHANGE, configName)
            dhMan.selectedConfig = configName
    tree = dhMan[configName].tree
    states = dhMan[configName].getStates()
    fsm = dhMan[configName].get_fsm()

    return {
        "configName": configName,
        "tree": tree,
        "states": states,
        "fsm_rules": fsm,
        "externalScripts": dhMan[configName].externalScripts,
    }


@main.route("/status", methods=["GET"])
def get_status():
    # configName = request.args["configName"]
    CONFIG = current_app.config["daqControl"]
    dhMan = current_app.extensions["daqControl"].dhManager
    r_states = current_app.extensions["daqControl"].r_rcstates
    stats = {}
    stats["userinfo"] = session.get("user", "not_logged")
    stats["singleRunMode"] = CONFIG["single_run_mode"]
    stats["selectedConfig"] = dhMan.selectedConfig
    stats["sequencerState"] = r_states.hgetall("sequencerState")
    stats["lastSequencerState"] = r_states.hgetall("lastSequencerState")
    return stats


@main.route("/configStatus", methods=["GET"])
def config_status():
    """Return the status of the config
    - config state
    - who interlocked
    - run type
    - run comment
    """
    dhMan = current_app.extensions["daqControl"].dhManager

    configName = request.args["configName"]
    status = {}
    try:
        status["configState"] = dhMan[configName].configState
        status["whoInterlocked"] = dhMan[configName].whoInterlocked
        status["configRunType"] = dhMan[configName].runType
        status["configRunComment"] = dhMan[configName].runComment

    except KeyError:
        return "Config not found", 404
    return status


@main.route("/logs", methods=["GET"])
def rcgui_logs():
    CONFIG = current_app.config["daqControl"]

    with open(CONFIG["serverlog_location_name"], "r") as f:
        lines = deque(f, 20)
    return [json.loads(line) for line in lines]


@main.route("/log/<string:configName>/<string:module>")
def full_log_module(configName, module):
    """
    Full for the module or script as raw text that can be embedded in a panel.
    """
    r_rcstates = current_app.extensions["daqControl"].r_rcstates

    logPath = rw.get_log_path(r_rcstates, configName, module)
    try:
        with open(logPath, "r") as f:  # type: ignore
            data = f.read()
    except FileNotFoundError:
        data = f"File {logPath} not found"
    except Exception as e : 
        data = f"File {logPath} not found because of : {e}"
    return Response(data, mimetype="text/plain")


@main.route("/liveLogURL", methods=["GET"])
def live_log_url() -> str:
    dhMan = current_app.extensions["daqControl"].dhManager

    configName = request.args["configName"]
    processName = request.args["processName"]

    return f"http://{socket.gethostname()}:9001/logtail/{dhMan[configName].config['group']}:{processName}"


@main.route("/ping", methods=["GET"])
@only_localhost_scripts
def check_connection() :
    return "Success"
