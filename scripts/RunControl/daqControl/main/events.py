#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from flask_socketio import join_room, leave_room
from .. import socketio


@socketio.on("join")
def on_join(data):
    """
    When a user changes config, it will register it in a specific room, so only events related to the config are captured.
    """
    configName = data["room"]
    join_room(configName)


@socketio.on("leave")
def on_leave(data):
    configName = data["room"]
    leave_room(configName)
