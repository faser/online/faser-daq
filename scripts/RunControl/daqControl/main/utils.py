#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

import json
from pathlib import Path
from ..utils.constants import SnackbarLevel, SocketioEvent
from flask import current_app


def get_config_tree_from_file(configName: str) -> dict:
    """Return the tree representation of the configuration"""
    CONFIG = current_app.config["daqControl"]
    CONFIG_DIR = CONFIG["DAQ_CONFIG_DIR"]

    with (Path(CONFIG_DIR) / configName / "config-dict.json").open("r") as f:
        data = json.load(f)
        tree_path = data["tree"]

    with (Path(CONFIG_DIR) / configName / tree_path).open("r") as f:
        tree = json.load(f)

    return tree


def emit_snackbar(title: str = "", message: str = "", logLevel: str = SnackbarLevel.INFO):
    socketio = current_app.extensions["socketio"]
    socketio.emit(
        SocketioEvent.EMIT_SNACKBAR,  # type: ignore
        {"title": title, "body": message, "level": logLevel},
    )
