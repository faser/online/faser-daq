#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from typing import Union

from redis import Redis

from nodetree import NodeTree
from anytree import RenderTree
from .utils import redis_wrappers as rw
import logging
from os import environ as env


from .daqhandler import DaqHandler

logger = logging.getLogger("daqControl")


class FASERDaqHandler(DaqHandler):
    """
    Class handles the loaded Config
    """

    def __init__(self, *args, redis_metrics_db: Union[Redis, None] = None):
        super().__init__(*args)
        self.redisMetricsDB = redis_metrics_db
        

    @property
    def runComment(self) -> str:
        return str(rw.get_run_comment(self.redisStateDB, self.configName))

    @runComment.setter
    def runComment(self, runComment: str):
        rw.set_run_comment(self.redisStateDB, self.configName, runComment)

    @property
    def runType(self) -> str:
        return str(rw.get_run_type(self.redisStateDB, self.configName))

    @runType.setter
    def runType(self, runType: str):
        rw.set_run_type(self.redisStateDB, self.configName, runType)

    def detectorList(self):
        detList = []
        for comp in self.config["components"]:  # type: ignore
            compType = comp["modules"][0]["type"]  # type: ignore
            if compType == "TriggerReceiver":
                detList.append("TLB")
            elif compType == "DigitizerReceiver":
                detList.append("DIG00")
            elif compType == "TrackerReceiver":
                boardID = comp["modules"][0]["settings"]["BoardID"]  # type: ignore
                detList.append(f"TRB{boardID:02}")
        return detList

    # override
    def _module_status(self, node: NodeTree) -> int:
        """
        Returns the status of the module :
         - 0 : normal
         - 1 : warning
         - 2 : error
         - 3 : crashed

        0 to 2 are read from the redis database if provided.

        3 is determined by the __crashed method, independent of the redis database
        If no metrics database is provided when creating the daqHandler, the status can only be 0 (normal) or 3 (crashed).

        """
        status = super()._module_status(node)
        if status == 0 and self.redisMetricsDB is not None:
            status = rw.get_module_status(self.redisMetricsDB, node.name)
        return status  #  type: ignore

    # override
    def bad_modules(self) -> tuple[list[str], list[str]]:  # type: ignore
        """Return the list of modules with status 2 (error) and 3 (crashed)"""
        errorM, crashedM = [], []
        for _, _, node in RenderTree(self.nodeTree):
            if node.included and self._module_status(node) == 3:
                crashedM.append(node.name)
            elif node.included and self._module_status(node) == 2:
                errorM.append(node.name)
        return errorM, crashedM
