#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from functools import wraps
from flask import request, session, current_app
from ..utils.constants import SocketioEvent
from ..utils import redis_wrappers as rw
import socket


def only_localhost_scripts(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if (session.get("user") is None) and (
            current_app.config['daqControl'].get('only_localhost_scripts', True) # if not exists in config, -> True
        ) and (
            request.remote_addr != socket.gethostbyname(socket.gethostname())
        ) :
            return "Not authenticated", 401
        return f(*args, **kwargs)
    return decorated_function


def interlock_checker(ID: str, r_rcstates, socketio) -> None:
    while rw.who_interlocked(r_rcstates, ID) is not None:
        socketio.sleep(1)
    socketio.emit(SocketioEvent.CHANGED_INTERLOCK, None, to=ID)
