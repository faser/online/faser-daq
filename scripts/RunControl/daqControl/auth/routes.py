#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from flask import (
    jsonify,
    request,
    session,
    url_for,
    redirect,
    render_template,
    current_app,
)
import platform
from ..utils import redis_wrappers as rw
from ..utils.constants import SocketioEvent
from . import auth
from .utils import only_localhost_scripts, interlock_checker
import logging
from keycloak import Client


logger = logging.getLogger("daqControl")
kc = Client()


@auth.route("/login", methods=["GET", "POST"])
def login():
    CONFIG = current_app.config["daqControl"]

    if CONFIG["SSO_enabled"]:
        kc.callback_uri = request.url_root + "login/callback"
        authURL, session["state"] = kc.login()
        return redirect(authURL)
    else:
        # authentication based on IP address
        ip_addr = request.remote_addr
        session["user"] = ip_addr
    return redirect(url_for("main.index"))


@auth.route("/login/callback", methods=["GET"])
def login_callback():
    CONFIG = current_app.config["daqControl"]
    newState = request.args.get("state", "unknown")
    if session.pop("state", None) != newState:  # invalid state
        return render_template(
            "invalidState.html", url=f"http://{platform.node()}.cern.ch:{CONFIG['port']}"
        )
    code = request.args.get("code")
    response = kc.callback(code)
    userinfo = kc.fetch_userinfo(response["access_token"])
    session["user"] = userinfo["cern_upn"]
    logger.info(f"User {session['user']} logged in")
    return redirect(url_for("main.index"))


@auth.route("/logout", methods=["GET", "POST"])
@only_localhost_scripts
def logout():
    CONFIG = current_app.config["daqControl"]
    session.pop("user", None)
    if not CONFIG["SSO_enabled"]:
        return redirect(url_for("main.index"))
    return redirect(f"{CONFIG['logout_url']}?redirect_uri={url_for('main.index', _external=True)}")


@auth.route("/lock", methods=["POST"])
@only_localhost_scripts
def ask_interlock():
    CONFIG = current_app.config["daqControl"]
    r_rcstates = current_app.extensions["daqControl"].r_rcstates
    socketio = current_app.extensions["socketio"]
    data = request.get_json()
    ID: str = data["ID"]  # type: ignore
    user: str = data["user"]  # type: ignore

    if user != session["user"]:
        logger.warning(f"{session['user']} tried to interlock for {user}")
        return "You can't interlock for someone else", 403

    if rw.who_interlocked(r_rcstates, ID) is None:
        rw.set_interlock(r_rcstates, ID, session["user"], CONFIG["timeout_interlock_sec"])
        socketio.emit(SocketioEvent.CHANGED_INTERLOCK, session["user"])
        socketio.start_background_task(
            interlock_checker, ID=ID, r_rcstates=r_rcstates, socketio=socketio
        )
        return jsonify({})
    logger.debug(f"{session['user'] } already interlocked the file {ID}")
    return "Someone already interlocked the file", 403


@auth.route("/unlock", methods=["POST"])
@only_localhost_scripts
def release_interlock():
    r_rcstates = current_app.extensions["daqControl"].r_rcstates
    ID: str = request.json["ID"]  # type: ignore
    if rw.who_interlocked(r_rcstates, ID) == session["user"]:
        rw.remove_interlock(r_rcstates, ID)
        return jsonify({})
    logger.warning(f"{session['user']} tried to release interlock for {ID}")
    return "You don't have the rights to release interlock", 403
