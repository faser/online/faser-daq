#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from flask import Blueprint

auth = Blueprint('authentication',__name__)

from . import routes