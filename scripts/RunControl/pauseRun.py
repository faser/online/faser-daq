#!/usr/bin/env python3

#
#  Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

import sys, os, json, requests
import redis
import time
from runcontrol import RunControl

import logging

pauseLength=180
maxRate=100

cfgFile="combinedTI12"
runtype="Physics"
url="http://faser-daq-010.cern.ch:5000"
timeout = 10 

#for testing
#pauseLength=20
#maxRate=10
#runtype="Test"
#cfgFile="digitizerEHN1"
#url="http://faser-daq-002.cern.ch:5000"


mattermost_hook = None
timeout = 10

if os.access("/etc/faser-secrets.json", os.R_OK):
    with open("/etc/faser-secrets.json") as f:
        secrets = json.load(f)
        mattermost_hook = secrets.get("MATTERMOST", "")


def message(msg: str):
    """Send a basic message to mattermost"""
    if mattermost_hook is not None:
        try:
            req = requests.post(mattermost_hook, json={"text": msg}, timeout=timeout)
            if req.status_code != 200:
                print("Failed to post message below. Error code:", req.status_code)
                print(msg)
        except Exception as e:
            print("Got exception when posting message", e)
    else:
        print(msg)


def error(msg: str):
    """Send a basic error message to mattermost in the `faser-ops-alerts channel`"""
    if mattermost_hook is not None:
        try:
            req = requests.post(
                mattermost_hook, json={"text": msg, "channel": "faser-ops-alerts"}, timeout=timeout
            )
            if req.status_code != 200:
                print("Failed to post message below. Error code:", req.status_code)
                print(msg)
        except Exception as e:
            print("Got exception when posting message", e)
    else:
        print(msg)



r = redis.Redis(host='localhost', port=6379, db=0,
                charset="utf-8", decode_responses=True)

def getPhysicsRate():
    data=r.hget('eventbuilder01','Event_rate_Physics')
    if not data: return maxRate+1
    try:
        return int(data.split(":")[1])
    except ValueError:
        return maxRate+1

def stateCheck(state):
    if (state and
        state['runOngoing'] and
        state['loadedConfig'].startswith(cfgFile) and
        state['runState']=='RUN' and
        not state['crashedM'] and
        state['runType']==runtype):
        return True
    return False

if __name__ == '__main__':
    active=False
    rc=RunControl(baseUrl = url)
    print("Checking state of FASER running")
    state=rc.getState()
    if state and state['runState']=='PAUSED':
        print("Run is already paused - assuming failed to resume on last try, so will resume")
        rc.resume()
        sys.exit(0)
    if state and not state['runOngoing']:
        print("No FASER run on-going, so no need to pause")
        sys.exit(0)
    if not stateCheck(state):
        print("FASER is not physics running - please request manual pause")
        sys.exit(1)

    physicsRate=getPhysicsRate()
    if physicsRate>maxRate:
        print(f"Current FASER physics data taking rate is above {maxRate} Hz")
        print("Likely taking collision or special data, so will not pause  - please request manual pause")
        sys.exit(1)
        
    message("Pausing run for Formosa")
    print("Sending pause request")
    rc.pause()
    try:
        for ii in range(pauseLength):
            print(f"paused for {ii} of {pauseLength} seconds - press ctrl-c to resume early")
            time.sleep(1)
    except KeyboardInterrupt:
        print("User interruption - will resume now")
        pass
    print("Resuming run")
    rc.resume()
    time.sleep(2)
    print("Checking state")
    state=rc.getState()
    if not stateCheck(state):
        print("Something went wrong during resume - error message posted to FASER mattermost")
        error("Run doesn't seem to have resumed after being paused for FORMOSA, please check")
        sys.exit(1)
    message("Run resumed")

