#!/usr/bin/env python3

#
#  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
#

import sys, os, json, requests
import redis
import time
from runcontrol import RunControl
import logging

cfgFile="combinedTI12"
runtype="Physics"
startcomment="Restart for fill {0}"
endcomment="End of fill"
url="http://faser-daq-010.cern.ch:5000"
minRunTime=30
timeout = 10 

r = redis.Redis(host='localhost', port=6379, db=0,
                charset="utf-8", decode_responses=True)

mattermost_hook = None
if os.access("/etc/faser-secrets.json",os.R_OK):
    secrets=json.load(open("/etc/faser-secrets.json"))
    mattermost_hook=secrets.get("MATTERMOST","")

if mattermost_hook is None : 
    logging.error("No MATTERMOST hook found in secrets file")

def message(msg):
    if mattermost_hook:
        try:
            req = requests.post(mattermost_hook,json={"text": msg}, timeout=timeout)
            if req.status_code!=200:
                logging.error("Failed to post message below. Error code:",req.status_code)
                logging.error(msg)
        except Exception as e:
            logging.error("Got exception when posting message",e)
    else : 
        logging.error(msg)

def error(msg):
    if mattermost_hook:
        try:
            req = requests.post(mattermost_hook,json={"text": msg,"channel": "faser-ops-alerts"}, timeout=timeout)
            if req.status_code!=200:
                print("Failed to post message below. Error code:",req.status_code)
                print(msg)
        except Exception as e:
            print("Got exception when posting message",e)
    else:
        logging.error(msg)


class FillNo:

    def __init__(self):
        self.fillNo=self.getFillNo()

    def getFillNo(self):
        data=r.hget('digitizer01','LHC_fillnumber')
        if not data: return 0
        try:
            return int(data.split(":")[1])
        except ValueError:
            return 0


    def checkNewFill(self):
        fillno=self.getFillNo()
        if fillno>self.fillNo:
            self.fillNo=fillno
            return fillno
        return None

def stateCheck(state):
    if not state:
        error("Failed to get run state")
        return False
    if not state['loadedConfig'].startswith(cfgFile):
        message(f'Currently not running with combined run configuration ("{state["loadedConfig"]}" instead of "{cfgFile}*")')
        return False
    if state['runType']!=runtype:
        message(f'Current run is not a "{runtype}" run')
        return False
    if state['runState']!='RUN':
        error(f'DAQ is not in a running state, but instead in state "{state["runState"]}"')
        return False
    if state['crashedM']:
        error("DAQ has crashed modules - leave it as is")
        return False
    if time.time()-float(state['runStart'])<minRunTime:
        message(f"Restarted run less than {minRunTime} seconds ago")
        return False
    return True

if __name__ == '__main__':
    active=False
    test=False
    if "--active" in sys.argv:
        print("Running in active mode - will stop/start runs")
        active=True
    if "--test" in sys.argv:
        test=True
    fill=FillNo()
    print(f"Starting at fill number {fill.fillNo}")
    rc=RunControl(baseUrl = url)
    print("Starting DAQ state:",rc.getState())
    while True:
        newFillNo=fill.checkNewFill()
        if test:
            newFillNo=input("Enter new fill number: ")
        if newFillNo:
            message(f"LHC fill number has changed to {newFillNo}")
            state=rc.getState()
            if stateCheck(state):
                # do action to stop/start run if in combined run
                if active:
                    if not rc.stop(runtype,endcomment):
                        error("Failed to stop run cleanly - please check")
                    elif not rc.start(runtype,startcomment.format(newFillNo)):
                        error("Failed to start new run cleanly - please check")
                else:
                    print("should change run")

            else:
                message(f"No automatic run restart was done")


        time.sleep(5)
