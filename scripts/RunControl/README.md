
# Run Control Web GUI 

Table of content
---
1. [What's New](#whats-new)
2. [Installation](#installation)
   - [DAQ Servers](#daq-servers)
   - [Keycloak Setup](#keycloak-setup)
3. [Automatic Restart](#automatic-restart)
4. [Run Control Configuration](#run-control-configuration)
5. [Interface](#interface)
   - [Tree View](#tree-view)
   - [Configuration Selection](#configuration-selection)
   - [Command Buttons](#command-buttons)
   - [Actions](#actions)
---

## What's new

In the new version, DAQling allows to view the DAQ system as a tree with nodes. Some nodes have children (leaves). Contrary to the previous version, it is possible to control independently the different modules by choosing an command to send to a specific module (now and after, the term "command" will be used to designate the commands sent to a node, contrary to "action" which will be specific commands to FASER and the Run Control GUI).


The commands sent will be transmitted to all children nodes that are `included`. A node can also be `excluded`.
> Excluded nodes will not participate to the parent's status and will not receive commands from the parent node.

*(From DAQling documentation)*

In addition to the `included`/`excluded` flag, a node also has an `inconsistent` flag: 
> Inconsistent `[ True / False ]`. If the children of a node are in different state, the `inconsistent` flag is raised.

*(From DAQling documentation)*

Each state of the FSM can be navigated, as described in graph below : 

```mermaid
flowchart LR

s1[not_added]
s2[added]
s3[booted]
s4[ready]
s5[running]
s6([paused])


s1-->|add| s2 -->|boot|s3 -->|configure|s4 --> |start|s5 -. disableTrigger .-> s6 -. enableTrigger .-> s5
s6 -. ECR .-> s6


```
*The dashed and rounded elements are FASER specific.*

Instead of having only one configuration file, now a configuration setup needs 4 configuration files :

- \<nameofconfig>.json : the current config file.
- control-tree.json : the tree structure of the configuration. 
- fsm-rules.json : the possible states and order of starting and stopping  modules (should not change between configurations) 
- config-dict.json : paths of above configuration files. 

`fsm-rules.json` and `<nameofconfig>.json` can be in the `faser-daq/configs` directory, but a valid configuration needs to have a folder with the name of the configuration and inside it, a `config-dict.json`. 

For more informations, see the `DAQling` documentation ([link](https://gitlab.cern.ch/ep-dt-di/daq/daqling/-/blob/master/README.md)).

## Installation
### DAQ servers
Clone the project from Gitlab :
```bash
git clone --recurse-submodules https://gitlab.cern.ch/faser/online/faser-daq.git
cd faser-daq
```

Return to the faser-daq folder and build the project:
```bash
source setup.sh
mkdir build ; cd build
cmake ..
make -j4
```
The `setup.sh` script setups the correct environment variables, and setups the python environment for the web interface for the RCGUI (and the histogram monitoring).

We can now start the RCGUI server : 
```bash
./run.sh [-l]
```
- -l : localMode (doesn't communicate with run service server). Has to be used for testing. If the RCGUI runs in local mode, a red "LOCAL" should be present at the top of the interface.

To use the keycloak authentification, it is necessary to have a secret token. To have it, you have to create a new application at the CERN Application Portal.

### Keycloak setup

To use keycloak with CERN credentials, you have to register the run control application on the CERN application portal: https://application-portal.web.cern.ch/, via the "Add an Application" button.

_In the "Application details" tab_:
You have to enter the following information:
- "Application Identifier" : ID for the application (example: run_control_gui_daq002 )
- "Name" : ...
- "Home Page" : the url of the web app home page (example: http://faser-daqvm-000.cern.ch) (not a very important)
- "Description" : ...

_In the "SSO Registration" tab:_
- "Redirect URI" : The link after authenticating (example: http://faser-daqvm-000:5000/login/callback). In our case, only the host url should change, not the /login/callback part. 
- "base URL" : the root URL (example: http://faser-daqvm-000:5000)

The box : "My application will need to get tokens using its own client ID and secret" should be checked.

Once submitted, the new Client ID and the Client Secret are available. Both values have to be added to a `keycloak.json` file (if it doesn't exit, it has to be created in the `RunControl` folder and should have the following content).
```json
{
  "realm": "cern",
  "resource": <to be completed>,
  "client_id": <to be completed>,
  "credentials": {"secret": <to be completed>},
  "client_secret": <to be completed>,
  "redirect_uri": <to be completed>,
  "auth_server_url": "https://auth.cern.ch/auth",
  "issuer":"https://auth.cern.ch/auth/realms/cern",
  "authorization_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth",
  "token_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token",
  "token_introspection_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token/introspect",
  "userinfo_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo",
  "end_session_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout",
  "jwks_uri":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs"
}
```

## Automatic Restart

The autorestart feature performs an auto recovery of a run in case of a DAQ issue.
It allows the RCGUI to start (going trough shutdown) a new run automatically when the general state is "RUN" and one of the following conditions are met :
- A module transitions to an error state if its name matches the regex pattern defined in the `automatic_restart_regex_error` configuration field.
- A module crashes if its name matches the regex pattern defined in the `automatic_restart_regex_crash` configuration field.

## Run Control configuration

- **`SSO_enabled`**: boolean (`true` or `false`). Default: `false`. 
  Set to `true` to enable CERN authentication. Requires Keycloak.

- **`serverlog_location_name`**: string. Default: `./log/webinterfaceserver.jsonl`.
  Path to the server log file.

- **`timeout_session_expiration_mins`**: integer. Default: `120`.  
  Time (in minutes) after which a user session expires.

- **`timeout_interlock_secs`**: integer. Default: `120`.  
  Time (in seconds) before control of the rcgui is released due to inactivity.

- **`persistent_notification_delay`**: integer. Default: `7200`.  
  Time (in seconds) between two Mattermost reminders.

- **`ok_alerts`**: boolean. Default: `false`.
  Determines if "OK" alerts are sent to Mattermost when a module returns to a normal state.

- **`redis_metrics_db`**: integer. Default: `0`.
  Specifies the Redis database for monitoring metrics.

- **`redis_rcstates_db`**: integer. Default: `4`. 
  Specifies the Redis database used by the RCGUI.

- **`excluded_configs`**: array. Default : `["current.json", "fsm-rules.json", "grafana.json", "monitor_top.json", "top.json"]`  
  List of configuration files (located in the folder given by `DAQ_CONFIG_DIR`) that will not appear in the dropdown menu.

- **`time_before_remove_idle_config`**: integer.  Default: `900`.
  Time (in seconds) before unused configurations are unloaded. Should not be touched (will be removed in future versions).

- **`automatic_restart`**: boolean (`true` or `false`). Default: `true`.
  Enables automatic restart of run.

- **`automatic_restart_regex_crash`**: string. Default: `"(.*receiver.*|.*readout.*|.*digitizer\\d+|.*eventbuilder.*|.*filewriter.*|.*eventCompressor.*)"`.
  Regex pattern to enable automatic restart for crashed modules.

- **`automatic_restart_regex_error`**: string. Default: `"(.*receiver.*|.*readout.*|.*digitizer\\d+)"`
  Regex pattern to enable automatic restart for modules in an error state.

- **`single_run_mode`**: boolean (`true` or `false`). Default: `true`. 
  If enabled, only one configuration can be loaded at a time.

For a complete example configuration file, see [serverconfiguration.json](https://gitlab.cern.ch/faser/online/faser-daq/-/blob/new-rcgui/scripts/RunControl/daqControl/serverconfiguration.json)






## Interface

### Tree View 

On the left side is the treeview, where the structure of the control tree is displayed.
For each node, its state is displayed as a color code:
- not_added : grey
- added : brown
- booted : yellow
- ready : blue
- running : green
- paused : orange

It is also possible to hover over the color with the mouse to display the status.

When the daq system is in the general state "DOWN" it is also possible to include/exclude the node by means of the checkbox next to the node name. In all other general states this option is disabled.

When errors occur in nodes, an icon appears next to the node name and in the parent nodes. 
The colors for the errors correspond to different status of the module:

- orange : status = 1
- red : status = 2 

If there is at least one error in any node, the same icon appears at the top of the page.
Also, when a node is not in the same state as its parent node and the general state is not `IN TRANSITION`, an error for that module will be raised. 


### Configuration selection

The configurations displayed in the dropdown are taken from the folder specified by the `DAQ_CONFIG_DIR` environment variable.

### Command buttons 

Commands provided by the FSM can be directly applied to a node. Selecting a node in the treeview will display the command buttons below the root commands.  
The command buttons should only be used as a last resort, actions are preferable in any case. 

```mermaid
flowchart LR

n1([Node action]):::start

n1-->n2
subgraph g1 [" "]
direction LR
n2([send action]) --> |action| modules
end
n3([raise timeout error])
done[[Done]]:::stop
g1-->|success|done
g1-->|timeout|n3-->done


classDef start fill:#5cf754
classDef stop fill:#f2453f
```

### Actions

Actions are commands that will only be applied to the "root" node, no matter which module is selected. They should be seen as generalized commands. These are the commands to use to control the daq system, as they also communicate with external services in addition of commands. 


The root commands allow access to the different general states of the Run Control. 

```mermaid
flowchart LR
DOWN <-->|IN TRANSITION|READY<-->|IN TRANSITION|RUN<-->|IN TRANSITION|PAUSED
```

The following commands can be performed:

#### Initialise
*Loads the selected configuration, performs commands and transitions the general state to `READY`*.

Will perform the `add` and `configure` commands in this order on all modules (the `boot` command is done automatically).
<!-- Timeout: 120 seconds. -->
<!-- The command has a timeout, so if after 30 seconds all modules have not reached the `ready` state, (so the root node is in the `ready` state and is not inconsistent), the command is aborted.  -->

    
#### Start
*Starts a run. The general state transitions to `RUN`*.
The action displays a dialog window, allowing to define the run type and a comment (mandatory).
The information will be sent to the run service server, which will record this information and return the new run number.
The `start` command will then be sent to all modules.
<!-- Timeout: 15 seconds -->

<u>Note</u>: If we start the application with the `-l` argument, the run service server will not be used and the run will not be recorded (useful for testing). The resulting run number is 1000000000.

#### Stop
*Stops the run. The general state transitions to `READY`.*
A dialog window appears, allowing to specify the type of run if not already specified, as well as a comment at the end of the run. The information will be sent to the run service server for archiving. 
The `stop` command will then be sent to all modules. 
<!-- Timeout: 15 seconds. -->

<u>Note</u>: If we start the application with the `-l` argument, the application will not communicate with the run service server.

#### Shutdown
*Allows you to get into the general `DOWN` state.* 
It will perform the following commands in order: `unconfigure`, `shutdown` and `remove`.
If the timeout is exceeded (usually there is a problem), the `unconfigure` and `shutdown` steps will be skipped and only the `remove` command will be performed (force shutdown). 

<u>Note:</u> The `Shutdown` action is always accessible because it is the one that should be run when modules have errors. 

#### Pause

*Allows you to set the general state to `PAUSED`*.
The command will send the `disableTrigger` action to all modules. The unaffected modules will ignore it and simply set themselves to `paused` state.


#### Resume
*Allows the DAQ system to be switched back to the `RUN` state after being in the `PAUSED` state*.

The action will send the `enableTrigger` command to all modules. The unaffected modules will ignore it and return to the `running` state.

#### ECR
*Allows you to reset the event counter*.
Accessible only in `PAUSED` state, the action sends the `ECR` command to all modules. This action does not change the general state and remains in `PAUSED` state.

[![](https://mermaid.ink/img/pako:eNqVVc1u2zgQfhVCBQoHaAqT-rEjFF2gzR720gW66UnygZEomViJNEiq3cDKsW-yb9YX6ZC0bCqOW8QHQ_Ojme_7Zkjto0rWLMqjppPfqi1VBt19KEUpBF4Un6U06KPseyrqzVWe59pAAsRIsVCs5vpqA0a83--UrJjWU-7jI7iTxeLH9_-vrlyxbFFoJmpEK8Ol2IBTrIpe1kPHtK2xXhSKcs2Q4T2Tg0FMKalc3s1-38mKdlJ0D-gPVxovF0XLDFKD-DT098wnYgyoBoE0U195xRw2TM4b4zjsjJPLrXF61ruWghXFLfxvNk4QufNqoevr92hk_xnoxmrEhWEK3v13RIKc4pUXaBSxFQ5812jkghtOO4AwisQK51OhtGKok61GO2q2oyvjYyKzPfVw3yq626IWo2ICn6N4CQpUQA3Br-aKOeZuqtYjMl-e1oBihebOSoqGt4MCJCsfATalaA_o9VDZMY9WBO99C_gPrUextlk-ZtmBMbqFGcWNnaO1hRxhJpP1wDSYSy_gEjw-hpfvbLSM_vqUI_OwY69dGWTFY8Kgv7_c5afZlxHUwLZGKAl5iSSYBFhxHDAHluRZ7uQJd-wnc2T_zOpMisgdpKc-C84FDMKSgY1p5GHrTtpkLsnakxboNbIlJjEm7gKvntn0dbDpJ23iF2mzClCvA2k8Loj79jcXj1Hrec_1i0G-QL2bmXqTVjs62ENBliH65CXoyfJMFe_HgTKHTNsR7jR637E7xduWKWiNZ8sgCLlMMwFKwMQ9PT0Hiumht1TikEr6IirxBSrJGRXXkok5k2QWPQJK5tueWhKEuKcTiTMaaUgjC2mQ37BIL7DIzli4YzCI4E4i2Symt4Op5TdxFlCsl1-D_AO5EPMKFWXUSFUxNJUpo1_AXl2AvZ7BPmbPUKznEmfu6mjduQEc4a4QcrTabKZ_KaqOan3LGuQvw4Z3Xf4qrZpVmsyCcDf4WEOSNG78y9GbqGeqp7yGT_3eQY3MlvWsjHJ47Hi7NWVUikdIHHY1NezPmsMHKMob2mn2JqKDkf88iOro8Fm3nIKg_cH7-BPUl8zt)](https://mermaid.live/edit#pako:eNqVVc1u2zgQfhVCBQoHaAqT-rEjFF2gzR720gW66UnygZEomViJNEiq3cDKsW-yb9YX6ZC0bCqOW8QHQ_Ojme_7Zkjto0rWLMqjppPfqi1VBt19KEUpBF4Un6U06KPseyrqzVWe59pAAsRIsVCs5vpqA0a83--UrJjWU-7jI7iTxeLH9_-vrlyxbFFoJmpEK8Ol2IBTrIpe1kPHtK2xXhSKcs2Q4T2Tg0FMKalc3s1-38mKdlJ0D-gPVxovF0XLDFKD-DT098wnYgyoBoE0U195xRw2TM4b4zjsjJPLrXF61ruWghXFLfxvNk4QufNqoevr92hk_xnoxmrEhWEK3v13RIKc4pUXaBSxFQ5812jkghtOO4AwisQK51OhtGKok61GO2q2oyvjYyKzPfVw3yq626IWo2ICn6N4CQpUQA3Br-aKOeZuqtYjMl-e1oBihebOSoqGt4MCJCsfATalaA_o9VDZMY9WBO99C_gPrUextlk-ZtmBMbqFGcWNnaO1hRxhJpP1wDSYSy_gEjw-hpfvbLSM_vqUI_OwY69dGWTFY8Kgv7_c5afZlxHUwLZGKAl5iSSYBFhxHDAHluRZ7uQJd-wnc2T_zOpMisgdpKc-C84FDMKSgY1p5GHrTtpkLsnakxboNbIlJjEm7gKvntn0dbDpJ23iF2mzClCvA2k8Loj79jcXj1Hrec_1i0G-QL2bmXqTVjs62ENBliH65CXoyfJMFe_HgTKHTNsR7jR637E7xduWKWiNZ8sgCLlMMwFKwMQ9PT0Hiumht1TikEr6IirxBSrJGRXXkok5k2QWPQJK5tueWhKEuKcTiTMaaUgjC2mQ37BIL7DIzli4YzCI4E4i2Symt4Op5TdxFlCsl1-D_AO5EPMKFWXUSFUxNJUpo1_AXl2AvZ7BPmbPUKznEmfu6mjduQEc4a4QcrTabKZ_KaqOan3LGuQvw4Z3Xf4qrZpVmsyCcDf4WEOSNG78y9GbqGeqp7yGT_3eQY3MlvWsjHJ47Hi7NWVUikdIHHY1NezPmsMHKMob2mn2JqKDkf88iOro8Fm3nIKg_cH7-BPUl8zt)


### Log and info panels

It is possible to access the Run Control GUI logs and the logs/metrics of each module. 

When reloading the page, only the first 20 entries of the application logs are displayed. To access the entire log.

To access the logs of each module, you must first select the module in the treeview and then click on the `log` button. Two options are available :
- full log : all the log for a run is displayed, but you have to refresh the page to get the last updates. 
- live log: directly provided by supervisor, the display, it is updated live, but only the last lines of the log are displayed.


The live information for each module can be displayed after selecting a module in the treeview and clicking the "info" button. The information can also be displayed in a separate window by clicking on the icon in the upper right corner.


### Login and interlock

To perform actions on the daq system (and if Keycloak is enabled) you must log in with your CERN account by clicking on the silhouette icon in the upper right corner, and only then take control of the system by clicking on the lock icon. 

The CERN nickname will appear at the top next to the interlock icon.
Only one person at a time can take control of a configuration (or the all DAQ system, if `single_run_mode` is set to true).

Interlock has a timeout that is extended by the same amount of time if any action is taken on the system.
```mermaid
flowchart LR

n1([interlock]):::start-->n2{{locked ?}}
done[[Done]]:::stop
n2-->|yes|n3([raise already locked warning])-->done
n2-->|no|n4([lock config file])-->done


classDef start fill:#5cf754
classDef stop fill:#f2453f
```
```mermaid
flowchart LR

n1([login]):::start-->n2{{SSO ?}}
done[[Done]]:::stop
n2-->|yes|n3([reach keycloak])<-->n5[(keycloak)]
n3-->done
n2-->|no|n4([set username: offlineUser])-->done


classDef start fill:#5cf754
classDef stop fill:#f2453f
```
### Monitoring

On the monitoring panel, it is possible to see some live metrics of the eventbuilder module as well as time series plots of physics, calibration and monitoring events. 