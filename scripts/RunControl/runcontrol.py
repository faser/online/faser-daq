#
#  Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#
import time
from typing import Union
import requests
import logging


logger = logging.getLogger(__name__)


class RunControl:

    def __init__(self, baseUrl: str, requestTimeout: int = 60):
        self.__s = requests.Session()
        self.__baseUrl = baseUrl
        self.__requestTimeout = requestTimeout
        ret_code = self.send_request("GET", "/ping").status_code 
        if ret_code == 401 :
            logger.error("Failed to connect to the server, the provided URL points to an RCGUI server with the parameter 'only_localhost_scripts' set to True.")
            raise RuntimeError("Failed to connect to the server, the provided URL points to an RCGUI server with the parameter 'only_localhost_scripts' set to True.")
        elif ret_code == 200 : 
            pass
        else : 
            logger.error("An error occured while connecting to the server")
            raise RuntimeError("An error occured while connecting to the server")
        


    def send_request(self, typeR: str, route: str, args={}, timeout = None ) -> requests.Response:

        if typeR == "GET":
            r = self.__s.get(
                self.__baseUrl + route,
                params=args,
                timeout=self.__requestTimeout if timeout is None else timeout,
            )
        elif typeR == "POST":
            r = self.__s.post(
                self.__baseUrl + route,
                json=args,
                timeout=self.__requestTimeout,
            )
        else:
            raise ValueError("Invalid request type")
        return r

    def __send_action(self, **kwargs):
        try:
            r = self.send_request("POST", "/executeAction", args=kwargs)
        except requests.exceptions.Timeout:
            logger.error("Timeout while sending request")
            return False, {}
        except requests.exceptions.ConnectionError:
            logger.error("Connection Error")
            return False, {}
        return r.status_code == 200, r.json()

    def initialise(self) -> bool:
        status, _ = self.__send_action(action="INITIALISE")
        return status

    def start(self, runType, startComment, **kwargs) -> bool:
        status, _ = self.__send_action(
            action="START", args={"runType":runType, "runComment":startComment, **kwargs}
        )
        return status

    def pause(self) -> bool:
        status, _ = self.__send_action(action="PAUSE")
        return status

    def resume(self) -> bool:
        status, _ = self.__send_action(action="RESUME")
        return status

    def stop(self, runType, stopComment, **kwargs) -> bool:
        status, _ = self.__send_action(action="STOP", args={"runType":runType, "runComment":stopComment, **kwargs})
        return status

    def shutdown(self) -> bool:
        status, _ = self.__send_action(action="SHUTDOWN")
        return status

    def getState(self):
        """
        Return :
        - runOngoing : is it running ? (True/False) 
        - loadedConfig : the name of the current config
        - runState : State of the configuration
        - whoInterlocked : the user who has control
        - crashedM : crashed Modules
        - localOnly : if the run control is in local mode
        - runType : the type of the run
        - runComment : the current run comment
        - runNumber : the current run number
        - runStart : when the run started (timestamp)
        """
        try:
            r = self.send_request("GET", "/globalState")
        except requests.exceptions.Timeout:
            logger.error("Timeout while sending request")
            return {}
        except requests.exceptions.ConnectionError:
            logger.error("Connection Error")
            return {}
        return r.json() if r.status_code == 200 else {}

    def getInfo(self, module: str):
        """
        Return dictionary with all metrics for a given module
        """
        try : 
            r = self.send_request("GET", "/moduleInfo", {"module":module})
        except requests.exceptions.Timeout:
            logger.error("Timeout while sending request")
            return {}
        except requests.exceptions.ConnectionError:
            logger.error("Connection Error")
            return {}
        return r.json()

    def change_config(self, configName : str) -> Union[bool, dict]: 
        if self.getState()["runOngoing"] : 
            logger.error("Cannot change config while run is ongoing")
            return False
        try : 
            r = self.send_request("GET", "/selectConfig", {"configName":configName, "initiator": True})
        except requests.exceptions.Timeout:
            logger.error("Timeout while sending request")
            return False
        except requests.exceptions.ConnectionError:
            logger.error("Connection Error")
            return False
        return r.json() if r.status_code == 200 else False
    
    def send_log(self, source:str, message:str, level="INFO") : 
        try : 
            r = self.send_request("POST", "/addLog", {"message":message, "source": source, "level" : level}, timeout=5)
        except requests.exceptions.Timeout:
            logger.error(f"Timeout while sending the following log : message = {message}, source = {source}")
            return False
        return  r.status_code == 200
    
        
        
def example() : 
    rc = RunControl("http://localhost:8050")
    rc.change_config("emulatorLocalhost")
    time.sleep(2)
    print(rc.getState())

    rc.initialise()
    time.sleep(2)
    rc.start(startComment="Start from runcontrol class", runType="Test")  
    print(rc.getState())
    time.sleep(10)
    print(rc.getInfo("eventbuilder01"))
    rc.stop(stopComment="Stop from runcontrol class", runType="Test")
    time.sleep(2)
    rc.shutdown()
    time.sleep(2)
    print(rc.getState())

if __name__ == "__main__" : 
    example()
