#
# Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from daqControl import create_app, socketio
import daqControl.metricsHandler
import sys

PORT = 5000
try:
    localOnly = (sys.argv[1] == "-l") or (sys.argv[1] == "--local")
except IndexError:
    localOnly = False
app = create_app("./serverconfiguration.json", port=PORT, localOnly=localOnly, debug=True)

if __name__ == "__main__":
    metrics = daqControl.metricsHandler.Metrics(app.logger)
    socketio.run(app, host="0.0.0.0", port=PORT, use_reloader=False, log_output=True)
    metrics.stop()
