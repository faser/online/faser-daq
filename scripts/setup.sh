#!/bin/bash

# - Creates a python 3.9 environment for web related dependencies if not already created
# - Activates the python environment
# - Installs the required dependencies if the environment just has been created

DIR_ENV=$FASERTOP/scripts

if  [ -d $DIR_ENV/env/ ] ; then
    source $DIR_ENV/env/bin/activate
else
    python3 -m venv $DIR_ENV/env/
    source $DIR_ENV/env/bin/activate
    echo "Upgrading and install dependencies..."
    python3 -m pip install --upgrade pip
    python3 -m pip install -r $FASERTOP/scripts/requirements_web.txt
fi

# creating default log directory if not already created 
mkdir -p $FASERTOP/scripts/RunControl/log



