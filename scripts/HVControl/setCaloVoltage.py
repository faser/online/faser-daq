#!/usr/bin/env python3

#
#  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
#


import os
import subprocess
import sys
import time


#gain is wrt to above voltages

if len(sys.argv)==2 and sys.argv[1]=="default":
    voltLowE=[905,910,940,940]
    voltHighE=[940,1000,892,905]
elif len(sys.argv)!=3:
    print("Usage: setCaloVoltage.py <voltageLowE> <voltageHighE>")
    sys.exit(1)
else:
    voltLowE=[float(sys.argv[1])]*4
    voltHighE=[float(sys.argv[2])]*4

if min(voltLowE[0],voltHighE[0])<100 or max(voltLowE[0],voltHighE[0])>1600:
    print(f"{voltLowE} or {voltHighE} V is out of range")
    sys.exit(1)

newTargets={}

for ch in range(4):
    newTargets[ch]=voltLowE[ch]
    rc=os.system(f"snmpset -v 2c -m +WIENER-CRATE-MIB -c guru faser-mpod-00 outputVoltage.u9{ch:02d} F {voltLowE[ch]}")
    if rc:
        print("ERROR in low E channel ",ch)
        sys.exit(1)

    newTargets[ch+4]=voltHighE[ch]
    rc=os.system(f"snmpset -v 2c -m +WIENER-CRATE-MIB -c guru faser-mpod-00 outputVoltage.u8{ch:02d} F {voltHighE[ch]}")
    if rc:
        print("ERROR in high E channel ",ch)
        sys.exit(1)

time.sleep(5)
redo=True
while redo:
    redo=False
    for ch in range(8):
        volt=newTargets[ch]
        chid=f"9{ch:02d}"
        if ch>3:
            chid=f"8{ch-4:02d}"
        rc,output=subprocess.getstatusoutput(f"snmpget -v 2c -m +WIENER-CRATE-MIB -c guru faser-mpod-00 outputMeasurementSenseVoltage.u{chid}")
        print(output)
        if rc:
            print("ERROR in channel ",chid)
            sys.exit(1)
        newValue=-float(output.split()[-2])
        if abs(volt-newValue)>5:
            print("Not ready yet:",volt,newValue)
            redo=True
            time.sleep(1)


