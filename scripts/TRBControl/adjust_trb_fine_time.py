#!/usr/bin/env python3

#
#  Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
#

from handleTRBSettings import TRBSettings
import argparse

# CHECK PATH
TRBJSON_IN  = "/home/shifter/software/faser-daq/configs/Templates/TRB.json"
#TRBJSON_IN  = "/home/cantel/faser-daq/configs/Templates/TRB.json"
TRBJSON_OUT = TRBJSON_IN #"/home/cantel/faser-daq/configs/Templates/TRBNEW.json"

# base fine time setting per TRB "TRBReceiver<N>": [<HW delay>, <fine time delay>] 
TRB_reference_settings = {
                          "TRBReceiver11": [1,28],
                          "TRBReceiver12": [1,28],
                          "TRBReceiver13": [1,28],
                          "TRBReceiver00": [1,45],
                          "TRBReceiver01": [1,39],
                          "TRBReceiver02": [1,41],
                          "TRBReceiver03": [2,53],
                          "TRBReceiver04": [2,50],
                          "TRBReceiver05": [2,52],
                          "TRBReceiver06": [2,62],
                          "TRBReceiver07": [2,63],
                          "TRBReceiver08": [2,63]
                         }

Station2TRB = {
               "St0" : "TRBReceiver11,TRBReceiver12,TRBReceiver13",
               "St1" : "TRBReceiver00,TRBReceiver01,TRBReceiver02",
               "St2" : "TRBReceiver03,TRBReceiver04,TRBReceiver05",
               "St3" : "TRBReceiver06,TRBReceiver07,TRBReceiver08"
              }

kPHASEJUMP = 48

def main():
    
    parser = argparse.ArgumentParser(description='Adjust TRB SCT timing settings w.r.t. to a reference given a fine time adjustment')
    parser.add_argument('--trb', type=str, help="Name of TRB in TRB.json. Specify 'all' if want to modify all TRBs in all 4 stations.")
    parser.add_argument('--adjust', type=int, help='Clock fine time adjustment from reference.')
    args=parser.parse_args()
    
    if not args.trb:
        print("ERROR TRB argument must be provided. Provide name of TRB as given in json config file or specify 'all'")
        exit()

    trb_string = str(args.trb)

    for st_id, trbs in Station2TRB.items():
        trb_string = trb_string.replace(st_id,trbs)

    if trb_string.endswith(","):
        trb_string = trb_string[:-1]

    trb_list = (trb_string).split(',')
    fine_adjustment = args.adjust
    
    if len(trb_list)==1 and trb_list[0]=="all":
        all_trbs=[]
        for boardID in list(range(9))+list(range(11,14)):
            trb_name_i = f"TRBReceiver{str(0)*(boardID<10)}{boardID}"
            all_trbs.append(trb_name_i)
        trb_list = all_trbs
    
    ##
    # Calculate and set new Clk coarse and fine time setting:
    ##
    if fine_adjustment is None:
        print(f"ERROR value set for fine adjustment is invalid: {fine_adjustment}. Can't continue.")
        exit()
    print(f"\nINFO Applying fine time adjustment {fine_adjustment}")
    for trb_name in trb_list:
        if trb_name == "":
            print("ERROR no TRB name given. Provide name of TRB receiver, e.g. --trb TRBReceiver00")
            exit() 
        if not trb_name in TRB_reference_settings:
            print(f"ERROR TRB name not recognised: {trb_name}!")
            exit()
    
        print(f"INFO: Updating TRB {trb_name}")
        ref_coarse_time, ref_fine_time = TRB_reference_settings[trb_name]
        coarse_adjust, clk_fine_time = divmod(ref_fine_time+fine_adjustment, 64)
        coarse_jump_adjust = bool(ref_fine_time//kPHASEJUMP) ^ bool(clk_fine_time//kPHASEJUMP)  # take into account phase > 3/4 jump only if it was overstepped.
        coarse_jump_adjust = -1*(ref_fine_time//kPHASEJUMP)*coarse_jump_adjust + (clk_fine_time//kPHASEJUMP)*coarse_jump_adjust # negative jump if overstepping to left
        hw_delay = abs(ref_coarse_time + coarse_adjust + coarse_jump_adjust) # avoid negative values
        hw_delay = abs(min(2,hw_delay) - (hw_delay//3)) # hw_delay must be between 0 and 2 incl. Start subtracting for larger values
        print(f"INFO Computed new values:  HW delay={hw_delay}, Clk fine phase={clk_fine_time}")
        trb_settings = TRBSettings(TRBJSON_IN)
        trb_settings.set_output(TRBJSON_OUT)
        trb_settings.update(trb_name, ["HWDelayClk0","HWDelayClk1","FinePhaseClk0","FinePhaseClk1"],[hw_delay,hw_delay,clk_fine_time,clk_fine_time])

if __name__ == "__main__":
	main()
