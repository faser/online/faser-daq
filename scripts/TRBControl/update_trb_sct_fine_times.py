#!/usr/bin/env python3

#
#  Copyright (C) 2019-2023 CERN for the benefit of the FASER collaboration
#
import subprocess


TRB_offsets_ns = {
                          "TRBReceiver11": 0.2,
                          "TRBReceiver12": 0.2,
                          "TRBReceiver13": 0.5,
                          "TRBReceiver00": 1.7,
                          "TRBReceiver01": -0.8,
                          "TRBReceiver02": 0.1,
                          "TRBReceiver03": 2.2,
                          "TRBReceiver04": 1.3,
                          "TRBReceiver05": 2.1,
                          "TRBReceiver06": -0.9,
                          "TRBReceiver07": -0.3,
                          "TRBReceiver08": -0.5
                         }

k_NS_per_STEP = 25./64.


for name,offset_ns in TRB_offsets_ns.items():
  
    adjustment_step = round(offset_ns/k_NS_per_STEP)
    print(f"INFO: Converted {offset_ns} ns to {adjustment_step} steps for TRB {name}")


    #./adjust_trb_fine_time.py --trb TRBReceiver00,TRBReceiver03,TRBReceiver06,TRBReceiver11 --adjust

    subprocess.run(["./adjust_trb_fine_time.py","--trb",name,"--adjust",str(adjustment_step)])


print("Done")
