## SCT fine time scans

We run SCT fine time scans in order to center SCT physics hit pulses in the readout window, to maximise "010" and "011" physics hit patterns.
SCT fine time scans need to be performed during collisions, where the hit timing coresponds to "signal", i.e. to muons arriving from Interaction Point 1 (IP1).

SCT fine time scans are run using the faser-daq `trbFineTimeScan.json` sequence, which uses `adjust_trb_fine_time.py` to control the tracker hit times in the readout window.
The hit time is in fact adjusted by changing the TLB L1A delay in 25 ns steps (the _coarse_ time delay) and the TLB input clock phase adjustable in 390 ps step sizes (the _fine_ time delay).

A scan will take data in a time range spanning roughly 42 ns, or a little under 2 bunch crossings. At each fine time step, roughly 10k good collision events are collected.
The scan data is used in an offline analysis later, whereby a "top hat" distribution is plotted: The fraction of good hits, i.e. (01X)/(1XX|X1X|XX1), is plotted as a function of the time setting. The optimal coarse + fine time setting is the center of the top hat.

The delay settings are adjustable via TRB configurations. The _coarse_ time delay is set by `HWDelayClk0` and `HWDelayClk1` for (half plane) modules 0-3 and 4-7 respectively, ranging from 0 to 2, each step corresponding to 25 ns.
The additional _fine_ time delay is set by  `FinePhaseClk0` and `FinePhaseClk1` for the respective group of modules, ranging from 0 to 63, each step corresponding to 25 ns/64 steps = 390 ps.
There is one further _coarse_ time delay setting (another L1A signal delay in 25 ns steps) configured on the TLB, `TrackerDelay`. It has a much larger range, but results in an adjustment for _all_ tracker layers simultaneously.


**SCT fine time scan instructions and notes:**
1. Check the reference times specified as `TRB_reference_settings` in `adjust_trb_fine_time.py`. 
    - The sequence runs all tracker station planes in parallel, adjusting the clock fine phase relative to the reference point for each plane.
    - References are usually the time-tuned settings used during most recent data taking period.
2. Check the input and output TRB json files specified at the top of `adjust_trb_fine_time.py`: They should both point to the `TRB.json` used in the faser-daq sequence runs
    - The configs are overwritten with new delay values during the scan.
3. Tracker stations furthest downstream will require the largest HWDelay (L1A signal delay) -or vice versa. For a fine time scan on all tracker layers in parallel and reference times at the optimal timing, this may put the last few layers on the edge of the possible delay window (HWDelay = 2) and the top hat may appear incomplete.
3. It might be tricky obtaining complete top hat distributions for a scan running on all layers simultaneously. This is because the most downstream tracker station will need a larger time delay than the most upstream stations to center the top hat distribution in the scan but we have a limited `HWDelay` range (0-2). One can try
    - Adjust the global `TrackerDelay` L1A delay from the TLB to shift delays to make most use of the available range.
    - Else, shift the ref time away from center for stations on edge to complete as much of the top hat as possible.
    - A separate scan for front and back stations.
4. Check the `trbFineTimeScan.json` sequence "finalise" command. Ideally it sets the SCT fine time values back to physics data taking settings.

## SCT LED safe phase scans

Two 40 MHz input clock/cmd signal lines are distributed to the SCT modules: CLK0, distributed to modules 0-3 and CLK1, distributed to modules 4-7. That is, each tracker half plane is served by a different clock input.
Each SCT module sends data out on 2 "LED" (data communication) lines, one main and one back up: LED and LEDx. Due to propagation delays, there will be a phase difference between the original clock on FPGA and the LED and LEDx data output lines. This phase difference needs to be accounted for to avoid data latching failures in the RX Data Module Manager, which manages the received SCT module data and the level-1 FIFOs in which the data is stored. To account for the phase difference, a second set of clock signals, LED0 and LED1, are generated for the RX Data Module Manager for SCT modules 0-3 and modules 4-7, respectively.
The phases of the CLK0 and CLK1 and LED0 and LED1 clocks can all be adjusted separately in 390 ps step sizes. 
This phase adjust firstly allows us to time in our SCT hits in the readout window with a +/- 390 ps precision.
It secondly allows us to compensate for the phase difference between CLK0(1) and LED0(1) by tuning the LED0/1 clock phases so that the LED(x) data is safely latched onto the FPGA clock signal in the RX data manager.

In order to ensure safe latching, we make use of a pre-configured SCT module mode: SCT modules after a powercycle will transmit the input _clock_ signal at half frequency. 
A "safe phase detection" bit on the FPGA will latch onto every second input clock cycle, meaning this bit always has the same value.
We use a script that probes the "safe phase detection" bit on pre-confgured SCT modules, to detect the transition of the LED(x) data from 0 to 1 or 1 to 0 and determine a maximally safe LED phase setting at 180 degrees to the transition point.

The input CLK fine phase delay setting per TRB is denoted `FinePhaseDelayClk0` (modules 0-3) and `FinePhaseDelayClk1` (modules 4-7). The corresponding LED clock phase settings are denoted `FinePhaseDelayLed0` and `FinePhaseDelayLed1`. The safe phase scan scripts track both in parallel.

### When are safe phase scans needed?

A scan of the LED fine phase values is rarely needed. A fresh scan is needed if
- The TRB firmware version is updated.
- A module or module cable is replaced.

**But note** that the LED fine phase values need to be adjusted accordingly (via the same amount of steps in the same direction), if the input clock fine phase values are changed.
The `TRBSettings` class in `handleTRBSettings.py` - used by `adjust_trb_fine_time.py` during the SCT fine time scan for example - automatically takes care of adjusting the LED fine phase values whenever the clock fine phase values are adjusted.

**Therefore always use the TRBSettings class when updating the input clock fine phases, FinePhaseDelayClk0 and FinePhaseDelayClk1!**

<details closed>
<summary>An example safe phase detection scan can be seen here.</summary>
<br>

```
----------------
Results for CLK fine phase 16:
 step 00 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 10 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 20 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 30 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 40 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 50 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 60 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 70 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 80 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 90 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 10 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 11 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 12 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 13 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 14 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 15 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 16 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 17 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 18 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 19 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 20 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [1]
 step 21 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [0]
 step 22 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [0]
 step 23 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [0]
 step 24 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [0]
 step 25 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [0]
 step 26 => SafePhaseDetect [0] = [1] ; SafePhaseDetect [1] = [0]
 step 27 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 28 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 29 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 30 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 31 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 32 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 33 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 34 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 35 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 36 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 37 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 38 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 39 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 40 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 41 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 42 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 43 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 44 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 45 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 46 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 47 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 48 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 49 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 50 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 51 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 52 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 53 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 54 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 55 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 56 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 57 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 58 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 59 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 60 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 61 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 62 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
 step 63 => SafePhaseDetect [0] = [0] ; SafePhaseDetect [1] = [0]
Transition for Led0 at : 26
Transition for Led1 at : 20

---------------------------------------------
Optimal finePhase led 0 for input CLK finePhase 16 = 58
---------------------------------------------
---------------------------------------------
Optimal finePhase led 1 for input CLK finePhase 16 = 52
---------------------------------------------
```

</details>


### Running a scan
#### Set up


```
cd faser-daq
source setup.sh
```

First we need to compile standalone GPIODrivers to compile its executable scripts.
Here it's assumed we just use the GPIODrivers checked out as a submodule in faser-daq.
```
cd gpiodrivers/
mkdir -p build; cd build
cmake3 ..
make -j4
cd $FASERTOP
```
Next, we always ensure to source the gpiodrivers setup script.
```
cd scripts/TRBControl
source ../../gpiodrivers/setup.sh 
```
The `gpiodrivers/setup.sh` script loads the gpiodrivers apps such as the tracker SCT `safePhaseDetection` executable. 

#### Run

The basic steps are:
1. **Powercycle the SCT modules on the plane**: LV OFF->ON. HV is not needed.
2. Extract the current TRB clock fine phases used in data taking.
3. Run the `safePhaseDetection` script on the extracted clock fine phases.
4. Update the TRB LED fine phase values with the results of the script.

The gpiodrivers `safePhaseDetection` script requires TRB board communication info (i.e. board IP address and board ID) as well as the current clock fine phase settings for a given TRB in order to communicate with it and run a LED phase transition scan at our current operating input clock fine phase values.

First we want to dump the current clock phases and communication details for the TRB. Use the `handleTRBSetting` python module in `--dump` mode to dump the required info for all TRBs defined in our main faser-daq TRB config file (`config/Templates/TRB/json`).
```bash
 python3 -m handleTRBSettings -i ../../configs/Templates/TRB.json --trb all -p BoardID,BoardIP,FinePhaseClk0,FinePhaseClk1 --dump > FineTimeCfgs_2024_AllTRBs.json
```
The `FineTimeCfgs_2024_AllTRBs.json` file content should look like
```json
{
    "TRBReceiver00": {
        "BoardID": 0,
        "BoardIP": "faser-trb-00",
        "FinePhaseClk0": 45,
        "FinePhaseClk1": 45
    },
    "TRBReceiver01": {
        "BoardID": 1,
        "BoardIP": "faser-trb-01",
        "FinePhaseClk0": 39,
        "FinePhaseClk1": 39
    },
    ...
```

Next we execute gpiodrivers `safePhaseDetection` executable for each TRB, providing the relevant values from the json file.
**NOTE: Remember the hardware instructions - SCT modules need to first be powercycled!**

The following script will run an LED phase transition detection scan on each TRB.
```bash
#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Need an input json file. Exiting..."
    exit
fi

TRBNames=$(jq -r 'keys[]' $1)

for trbname in $TRBNames; do
    boardIP=$(jq -r .${trbname}.BoardIP $1)
    boardID=$(jq .${trbname}.BoardID $1)
    finephaseClk0=$(jq .${trbname}.FinePhaseClk0 $1)
    finephaseClk1=$(jq .${trbname}.FinePhaseClk1 $1)

    JSONOUTFILE="safeLedFinePhaseResults_${trbname}.json"

    cmd="safePhaseDetection -b ${boardID} -i ${boardIP} --clk0 $finephaseClk0 --clk1 $finephaseClk1 $JSONOUTFILE"
    echo "executing $cmd"
    $cmd
    # append TRB name as key to output json
    echo "{\"$trbname\": $(cat $JSONOUTFILE)}" > $JSONOUTFILE
done
```
(using jq for json parsing).

The script would be executed as 
```bash
./run_safe_phase_scan.sh FineTimeCfgs_2024_AllTRBs.json | tee log.out
```

**Import: It's important to check the bit results of each scan in log.out. One expects a wave of '1111111's then '0000000's (or vice versa) while a oscillating pattern (011101011011)
indicates that something is wrong.**
If the pattern is oscillating, the "safe" LED phase results for that scan will not make sense! One will have to judge sensible settings from the scan results on other half planes.

The script outputs a list of `safeLedFinePhaseResults_TRBReceiver*.json` files.
```bash
[user@faser-daq-011 TRBControl]$ tree
.
├── adjust_trb_fine_time.py
├── FineTimeCfgs_2024_AllTRBs.json
├── handleTRBSettings.py
├── README.md
├── run_safe_phase_scan.sh
├── safeLedFinePhaseResults_TRBReceiver00.json
├── safeLedFinePhaseResults_TRBReceiver01.json
├── safeLedFinePhaseResults_TRBReceiver02.json
├── safeLedFinePhaseResults_TRBReceiver03.json
├── safeLedFinePhaseResults_TRBReceiver04.json
├── safeLedFinePhaseResults_TRBReceiver05.json
├── safeLedFinePhaseResults_TRBReceiver06.json
├── safeLedFinePhaseResults_TRBReceiver07.json
├── safeLedFinePhaseResults_TRBReceiver08.json
├── safeLedFinePhaseResults_TRBReceiver11.json
├── safeLedFinePhaseResults_TRBReceiver12.json
├── safeLedFinePhaseResults_TRBReceiver13.json
├── update_sct_configs_from_dir.py
└── update_trb_sct_fine_times.py
```

Next, we need to update the `TRB.json` configs, this time using `handleTRBSettings` in `--update` mode and providing it the json file list of LED safe phase results. 
We can first choose to write the updated configs to a separate TRB json file (`-o TRB_UPDATED.json`) to compare to the current file.

```bash
 python3 -m handleTRBSettings --update --json-values safeLedFinePhaseResults_TRBReceiver*json -i ../../configs/Templates/TRB.json -o TRB_UPDATED.json --dump
 diff TRB_UPDATED.json ../../configs/Templates/TRB.json 
```

(remove `--dump` to avoid the dump of updated configs at the end). A diff will show us what's been updated with respect to the main `TRB.json` file.

