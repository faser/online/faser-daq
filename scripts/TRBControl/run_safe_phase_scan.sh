#!/bin/bash

# See README on when and how to run this script!

if [ $# -eq 0 ]
  then
    echo "Need an input json file. Exiting..."
    exit
fi

TRBNames=$(jq -r 'keys[]' $1)

for trbname in $TRBNames; do
    boardIP=$(jq -r .${trbname}.BoardIP $1)
    boardID=$(jq .${trbname}.BoardID $1)
    finephaseClk0=$(jq .${trbname}.FinePhaseClk0 $1)
    finephaseClk1=$(jq .${trbname}.FinePhaseClk1 $1)

    JSONOUTFILE="safeLedFinePhaseResults_${trbname}.json"

    cmd="safePhaseDetection -b ${boardID} -i ${boardIP} --clk0 $finephaseClk0 --clk1 $finephaseClk1 $JSONOUTFILE"
    echo "executing $cmd"
    $cmd
    # append TRB name as key to output json
    echo "{\"$trbname\": $(cat $JSONOUTFILE)}" > $JSONOUTFILE
done
