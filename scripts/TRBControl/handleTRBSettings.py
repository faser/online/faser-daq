#!/usr/bin/env python3

#
#  Copyright (C) 2019-2025 CERN for the benefit of the FASER collaboration
#

# Defines a TRBSettings class to handle TRB config settings
# Two handling modes are defined: "update" of settings and "dump"-ing of settings

import json
import time
import copy
import os
import glob

_TRBNAME_PREFIX = 'TRBReceiver'


class TRBSettings:
    def __init__(self, json_in):
        self.TRBjson_str = json_in
        if not os.path.isfile(self.TRBjson_str):
            raise ValueError(f"Input json config: {self.TRBjson_str} does not exist!")
        self.TRBjson_out_str = self.TRBjson_str
        with open(self.TRBjson_str, 'r') as TRBjson_file:
            data=TRBjson_file.read()
        TRBjson_file.close()
        self.trb_objs = json.loads(data) # FIXME does not resolve refs
        self.trb_list = [key for key in self.trb_objs.keys() if _TRBNAME_PREFIX in key]

    def set_output(self,json_out):
        self.TRBjson_out_str = json_out

    def _write_output(self):
        print("INFO *** writing new settings to %s ***"%(self.TRBjson_out_str))
        with open(self.TRBjson_out_str,'w') as trb_json_out_file:
            json.dump(self.trb_objs,trb_json_out_file, indent = 4, separators=(',', ': '))
    
    def update(self,trb_name, names,values, val_indices=None):

        if trb_name == "":
          print("ERROR no TRB name given. Provide name of TRB receiver, e.g. --trb TRBReceiver00")
          return
        total_updates = len(names)
        if not total_updates == len(values):
            print(f"ERROR Number of field names ({total_updates}) does not match number of values ({len(values)}). Provide a matching list, e.g. --name FinePhaseClk0,FinePhaseClk1 --value 16,32")
            return

        trb_obj = copy.deepcopy(self.trb_objs[trb_name])
        
        for idx in range(total_updates):
            name = names[idx]
            value = values[idx]
            if val_indices:
                try:
                   val_idx = val_indices[idx]
                except: 
                   print("ERROR Not enough value indices provided. Number of value indices ({len(val_indices)}) must equal values ({len(values)}). Will not update property.")
                   return
                if not isinstance(trb_obj["modules"][0]["settings"][name],list):
                    print("ERROR a value index is given, but property is not a list type. Will not update property.")
                    return
                print(f"INFO Updating TRB {trb_name}: Setting {name}, index {val_idx} to value {value}")
                trb_obj["modules"][0]["settings"][name][val_idx] = value
            else:
                if isinstance(trb_obj["modules"][0]["settings"][name],list):
                    print("ERROR This property is a list type, but no index for the value to be updated given.  Will not update property.")
                    return
                print(f"INFO Updating TRB {trb_name}: Setting {name} to value {value}")
                trb_obj["modules"][0]["settings"][name] = value
            if "FinePhaseClk" in name:  # if Clk fine phase is updated, LED fine phase is to be updated accordingly
                ledphase_name = "FinePhaseLed"+name[-1]
                old_val = self.trb_objs[trb_name]["modules"][0]["settings"][name]
                old_ledphase = trb_obj["modules"][0]["settings"][ledphase_name]
                ledphase = (old_ledphase+value-old_val)%64
                print(f"INFO [automatic update] Updating TRB {trb_name}: Setting {ledphase_name} to value {ledphase}")
                trb_obj["modules"][0]["settings"][ledphase_name] = ledphase
            time.sleep(0.1)
        self.trb_objs[trb_name] = trb_obj
        self._write_output()

    def update_via_json(self,json_updates):

        for trb_name_key in json_updates.keys():
            print(f"INFO Updating configs for TRB {trb_name_key}")
            if not trb_name_key in self.trb_list:
                raise ValueError(f"{trb_name_key} does not exist in {self.TRBjson_str}!")
            json_update_trb_level = json_updates[trb_name_key]
            modules_level = False
            settings = self.trb_objs[trb_name_key]['modules'][0]['settings']
            if 'modules' in  json_update_trb_level.keys():
                json_settings_update = json_update_trb_level['modules'][0]['settings']
            else:
                json_settings_update = json_update_trb_level
            #print("DEBUG Attempting to update with : \n", json_settings_update)
            settings.update(json_settings_update)
            self.trb_objs[trb_name_key]['modules'][0]['settings'] = settings

        self._write_output()

    def dump(self,trb_names, properties):

        for trb_name in trb_names:
            if trb_name == "":
              print("ERROR no TRB name given. Provide name(s) of TRB receiver(s), e.g. --trb TRBReceiver00")
              return
            if (trb_name).lower() == 'all':
                trb_list = self.trb_list
            else:
                trb_list = [trb_name]

            trb_dict = {}
            for trb_name in trb_list:
                if not trb_name in self.trb_objs:
                    print(f"ERROR {trb_name} not found!")
                    return
                trb_settings = self.trb_objs[trb_name]["modules"][0]["settings"]
                trb_dict[trb_name] = {}
                for prop in properties:
                    if not prop in trb_settings:
                        value = None
                    else:
                        value = trb_settings[prop]
                    trb_dict[trb_name][prop]=value

            # dump
            print(json.dumps(trb_dict,indent = 4, separators=(',', ': ')))


def unpack_json_updates(trb_name, json_files):

    #print("DEBUG json update files = ", json_files)
    json_updates = []
    for fname in json_files:
        if not os.path.isfile(fname):
            raise ValueError(f"Input json update file {fname} does not exist!")
        with open(fname) as f:
            json_updates.append(json.load(f))

    json_update_full = {}
    trb_names = []

    for json_update in json_updates:
        jkeys = list(json_update.keys())
        #print("DEBUG json keys = ", jkeys)
        if not len(jkeys):
            print("WARNING Empty json!")
            continue
        if not _TRBNAME_PREFIX in jkeys[0]:
            if trb_name == "":
                raise ValueError("No TRB name to update provided - neither in the input update json nor via --trb!")
            json_update_full[trb_name] = json_update
            properties = list(json_update.keys())
            trb_names += [trb_name]
        else:
            for jkey in jkeys:
                json_update_full[jkey] = json_update[jkey]
                properties = list(json_update[jkey].keys())
            trb_names += jkeys

    return trb_names,properties,json_update_full

    
def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str, required=True, help="input json file for current TRB settings")
    parser.add_argument('-o', '--output', type=str, default="", help="output json file to write updated TRB json config to instead of overwriting the input json.")
    parser.add_argument('--update', action='store_true', help='Update property(ies) with given value(s) set by --value or --json-values')
    parser.add_argument('--dump', action='store_true', help='Dump properties and values: If values to be updated are provided, dumped values are the updated values.')
    parser.add_argument('--trb', type=str, default="", help='Name of TRB in TRB.json to be manipulated. Value "all" will fetch all TRB objects.')
    parser.add_argument('-p', '--property', type=str, default=None, help='Property name(s) to be set. Can be comma-separated list.')
    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-j', '--json-values', nargs='+', default=None, help='Property value(s) in the form of a JSON object to be set if update=true. The TRB name is used if no TRB key names are found in the JSON. Can be a list of json files. ')
    group.add_argument('-v', '--value', default=None, help='Property value(s) to be set if update=true. Can be comma-separated list.')
    parser.add_argument('--idx', type=int, default=None, help='Index of value(s) in case property is an array. Can be comma-separated list.')
    args=parser.parse_args()

    if not args.update and not args.dump:
        print("ERROR Need to do at least one of available actions: --update, --dump")
        exit()

    trb_names = (args.trb).split(',')
    properties=[]
    if args.property:
        properties = (args.property).split(',')

    trb_settings = TRBSettings(args.input)
    trb_settings.set_output(args.output) if args.output != "" else args.input

    if args.update:
        if args.json_values:
            print("INFO *** Updating TRB values with given json inputs ***")
            trb_names,properties,json_updates = unpack_json_updates(trb_names,args.json_values)
            #print("DEBUG unpack_json_updates: ", trb_names, properties, json_updates)
            trb_settings.update_via_json(json_updates)
        else:
            values = (args.value).split(',')
            val_indices = (args.idx).split(',') if args.idx else None
            trb_settings.set_output(args.output) if args.output != "" else args.input
            for trb_name in trb_names:
                print(f"INFO *** Updating {trb_name} with given property/value lists ***")
                trb_settings.update(trb_name,properties,values,val_indices)
    if args.dump:
        trb_settings.dump(trb_names,properties)

if __name__ == "__main__":
	main()
