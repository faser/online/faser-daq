# Installing and running the online histogram monitoring app

Once in the `faser-daq/`, source the faser-daq `setup.sh` script : 
```console
source setup.sh
```
This will create a new and activate a dedicated python environment. 

Go to the `faser-daq/scripts/Monitoring/Dashboard` folder to start the application : 

```console
python run.py
```

If an exception of type `[...] address already in use [...]`, this means an instance of the app is already running on the server. 




