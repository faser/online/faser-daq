#!/usr/bin/env python3

#
#  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
#

import getopt
import json
import os
import requests
import sys
import time
from os import environ as env
import redis
sys.path.append("../RunControl")
import subprocess
from daqControl.utils import mattermost as mm 
from runcontrol import RunControl


# mattermost_hook=""

r = redis.Redis(host='localhost', port=6379, db=4,charset="utf-8", decode_responses=True)

def check_stopFlag() -> bool : 
    """
    Returns True if the stop flag  (redis key "stopSequencer") is set, False otherwise
    """
    return r.get("stopSequencer") is not None

def log(msg:str, level:str, rc:RunControl, logger=None) : 
    if logger is None : 
        rc.send_log("sequencer", msg, level)
        print(f"{level} : {msg}")
        return 
    message = f"[Sequencer] : {msg}"
    if level.lower() == "debug" :
        logger.debug(message)
    elif level.lower() == "info" :
        logger.info(message)
    elif level.lower() == "warning" :
        logger.warning(message)
    elif level.lower() == "error" :
        logger.error(message)
    elif level.lower() == "critical" :
        logger.critical(message)
    else :
        logger.error(f"[Sequencer] Invalid log level for log : {msg}")


class runner:
    def __init__(self,config,runControl:RunControl,seqnumber,seqstep, socketio = None, logger = None):
        self.cfgFile=config["cfgFile"]
        self.runtype=config["runtype"]
        self.startcomment=config["startcomment"]
        self.endcomment=config["endcomment"]
        self.maxRunTime=int(config["maxRunTime"])
        self.maxEvents=int(config["maxEvents"])
        self.preCommand=config["preCommand"]
        self.postCommand=config["postCommand"]
        self.seqnumber=seqnumber
        self.seqstep=seqstep
        self.runnumber=None
        self.runnumber = 1000000000 #NOTE : For testing only ? None wouldnt't work for REDIS
        self.rc=runControl
        self.socketio = socketio
        self.logger = logger
    
    def sleep(self,seconds :int) -> None:
        """
        If the sequencer script is not used as a standalone script (without the rcgui), it will use the regular "time.sleep()" function,
        instead of the sleep function provided by the flask-socketio package.
        """
        if self.socketio is not None: self.socketio.sleep(seconds)
        else : time.sleep(seconds)
    
 
            
        
    def initialize(self):
        self.sleep(1)
        status=self.rc.change_config(self.cfgFile.replace(".json",""))
        log(f"Reloaded/changed config to {self.cfgFile.replace('.json','')} after status {status}","debug", self.rc, self.logger)
        if not status: 
            return False
        return self.rc.initialise()

    def start(self):
        return self.rc.start(self.runtype,
                             self.startcomment,
                             seqnumber = self.seqnumber,
                             seqstep = self.seqstep,
                             seqsubstep = 0 # seqsubstep - not supported yet
                         )

    def stop(self):
        return self.rc.stop(self.runtype,self.endcomment)

    def shutdown(self):
        return self.rc.shutdown()


    def checkState(self):
     state=self.rc.getState()
     return state["runState"],state["runNumber"]

    def getEvents(self):
        data=self.rc.getInfo("eventbuilder01")
        if "Events_sent_Physics" in data:
            return int(data["Events_sent_Physics"])
        else:
            return 0

    def waitStop(self):
        now=time.time()
        while(True):
            # checking if stop flag is raised
            if r.get("stopSequencer") is not None:
                print("Stopping current run for the current step")
                return True                 

            state,runnumber=self.checkState()
            if state!="RUN":
                print(f"State changed to '{state}' - bailing out")
                return False
            self.runnumber=runnumber
            events=self.getEvents()
            if events>=self.maxEvents: return True
            if time.time()-now>self.maxRunTime: return True
            log(f'Run {runnumber}, State: {state}, events: {events}, time: {time.time()-now} seconds', "info", self.rc, self.logger)
            self.sleep(5)

    def run(self):
        
        if self.checkState()[0]!="DOWN":
            log(f"System is not shutdown - will not start run","error", self.rc, self.logger) 
            return False

        if self.preCommand:
            log(f"Running pre-command","info", self.rc, self.logger) 
            try :
                run_commands(self.preCommand, self.rc , self.logger)
            except RuntimeError :
                log(f"Failed to pre-command","error", self.rc, self.logger) 
                return False

        log(f"Initializing...","info", self.rc, self.logger) 
        rc=self.initialize()
        if not rc:
            log(f"Failed to initialize","error", self.rc, self.logger) 
            return False
        self.sleep(5)
        log(f"Starting...","info", self.rc, self.logger) 
        rc=self.start()
        if not rc:
            log(f"Failed to start run","info", self.rc, self.logger) 
            # print("Failed to start run")
            return False
        print("Running...") 
        self.sleep(5)
        rc=self.waitStop()
        if not rc:
            log(f"Failed to reach run conditions","error", self.rc, self.logger) 
            return False
        log(f"Stopping...","info", self.rc, self.logger) 
        rc=self.stop()
        if not rc:
            log(f"Failed to stop","error", self.rc, self.logger) 
            return False
        self.sleep(5)
        log(f"Shutting down...","info", self.rc, self.logger) 
        rc=self.shutdown()
        if not rc:
            log(f"Failed to shutdown - retry again in 10 seconds","warning", self.rc, self.logger) 
            self.sleep(10)
            rc=self.shutdown()
            if not rc:
                log(f"Failed to shutdown - giving up","error", self.rc, self.logger) 
                return False
        for ii in range(5):
            if self.checkState()[0]=="DOWN": break
            self.sleep(1)
        if self.checkState()[0]!="DOWN":
            log(f"Failed to shutdown {self.checkState()}","error", self.rc, self.logger) 
            return False
        if self.postCommand:
            log(f"Running post-command","info", self.rc, self.logger) 
            try :
                run_commands(self.postCommand, self.rc, self.logger)
            except RuntimeError :
                log(f"Failed to post-command","error", self.rc, self.logger) 
                return False
        return True

def usage():
    print("./sequencer.py [-S <seqNumber> -s <stepNumber> ] <configuration.json>")
    sys.exit(1)

def load_steps(configName:str) :  
    """
    Loads the configuration from the provided configuration filename and builds the steps from the config. 
    If there is a problem during the process, it will throw an RunTimeError -> best is to wrap the function with an try except block.  
    -> Requires the env variable DAQ_SEQUENCE_CONFIG_DIR. 
    
    Return
    ------
    - config :  dictionnary of the used config
    - cfgs : array of the differents steps of the sequence.  
    """ 
    with open(os.path.join(env["DAQ_SEQUENCE_CONFIG_DIR"],configName), "r") as fp: 
        config=json.load(fp)

    cfgs=[]
    if "template" in config:
        if "steps" in config:
            print("ERROR: cannot specify both 'template' and 'steps'")
            raise RuntimeError("Cannot specify both 'template' and 'steps'")
        steps=[]
        tempVars={}
        varLen=0
        for var in config['template']['vars']:
            varDef=config['template']['vars'][var]
            if type(varDef)==str:
                varDef=list(range(*eval(varDef)))
            if type(varDef)!=list:
                print(f"Template variable {var} not properly defined")
                raise RuntimeError(f"Template variable {var} not properly defined")
            if varLen and len(varDef)!=varLen:
                print(f"Template variable {var} does not have the proper number of entries")
                raise RuntimeError(f"Template variable {var} does not have the proper number of entries")
            varLen=len(varDef)
            tempVars[var]=varDef
        for idx in range(varLen):
            values={}
            for var in tempVars:
                values[var]=tempVars[var][idx]
            step={}
            for item in config['template']['step']:
                if isinstance(config['template']['step'][item], list) : 
                    step[item] = [el.format(**values) for el in config['template']['step'][item]]
                else:
                    step[item] = config['template']['step'][item].format(**values)
            steps.append(step)
        config["steps"]=steps

    for cfg in config["steps"]:
        for par in ["cfgFile",
                    "runtype",
                    "startcomment",
                    "endcomment",
                    "maxRunTime",
                    "maxEvents",
                    "preCommand",
                    "postCommand"]:
            if not par in cfg:
                if not par in config['defaults']:
                    print(f"Did not find value for '{par}'")
                    raise RuntimeError(f"Did not find value for '{par}'")
                cfg[par]=config['defaults'][par]
        cfgs.append(cfg)
    return config, cfgs


def update_rcgui(rc:RunControl, sequenceName:str, totalStepsNumber:int, stepNumber = 0, seqNumber = 0, end = False) -> None:
    """
    Emits "sequenceUpdate" event to RCGUI and update infos in REDIS.
    If 'end' argument is False, the function will just delete the entry in REDIS.
    The function also stores permanently the last state of the Sequencer with the key : "lastSequencerState". In contrary, "sequencerState" is destroyed when sequencer finished. 
    """
    if end :
        r.delete("sequencerState")
        rc.send_request("POST", "/sequencerUpdate", {})
        return

    updateDict  = {
        "sequenceName" : sequenceName,
        "seqNumber" : seqNumber,
        "stepNumber" : stepNumber,
        "totalStepsNumber" : totalStepsNumber,
    } 
    r.hmset("sequencerState", updateDict)
    r.hmset("lastSequencerState", updateDict) # the key stores the last state of the sequencer. 
    rc.send_request("POST", "/sequencerUpdate", updateDict)
    return
 
def run_commands(cmds, rc:RunControl, logger = None, stop_if_failure = True) : 
    failed_commands = []
    if not isinstance(cmds,list) : 
        cmds = [cmds] # converts the command to a list with one entry 
    for cmd in cmds :
        log(f"Executing command : {cmd}","info", rc, logger)
        process = subprocess.run(cmd,shell=True, text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if process.returncode != 0 : 
            log(process.stdout, "error", rc, logger)
            if stop_if_failure : 
                raise RuntimeError(f"Failed to run command : {cmd}")
            failed_commands.append(cmd)
        else :
            log(process.stdout, "info",rc, logger) # NOTE : if the script prints nothing, it will log a blank line 
   
    if len(failed_commands) != 0 :
        msg = "The following command(s) failed :\n"
        for cmd in failed_commands :
            msg+= f"//// {cmd}\n"
        log(msg, "error", rc, logger)
        raise RuntimeError()
    
def finalize_and_clean( finalizeCommands:list, runcontrol,  **args): 
    """
    if finalizeCommand is True : it will try to run the finalizeCommands. 
    **args : 
     - socketio
     - logger
    """
    if finalizeCommands is not None : 
        try :
            run_commands(finalizeCommands, rc=runcontrol,logger=args.get("logger"),stop_if_failure=False)
        
        except RuntimeError : 
            log(f"Failed in finalize command","error",runcontrol, args.get("logger")) 
            mm.message("Failed in finalize Command after error - please check")
            log(f"Failed at sequencer stop - please check","error", runcontrol, args.get("logger")) 
    update_rcgui(runcontrol, "",0,0,0, end=True) 
        
    
    
def main(args, socketio=None, logger = None):
    """
    Runs the sequencer.
    The parameter socketio is used when the function is called directly from the RunControl, which represents the socketio instance. 
    startStep begins at 1.
    """
    r.delete("stopSequencer") # delete any previous stop Flags
    startStep=1
    seqnumber=0 
    try:
        opts, args = getopt.getopt(args,"s:S:",[])
    except getopt.GetoptError:
        usage()
    for opt,arg in opts:    # type: ignore
        if opt=="-s":
            startStep=int(arg)
        if opt=="-S":
            seqnumber=int(arg)
    if len(args)!=1:
        usage()
    if startStep!=1 and seqnumber==0:
        usage() #FIXME, maybe one should be able to look up last step?
    
    config, cfgs = load_steps(args[0])

    hostUrl=config["hostUrl"]
    maxTransitionTime=config["maxTransitionTime"]
    runControl=RunControl(hostUrl,maxTransitionTime)
    currentStatus = runControl.getState()    
    initialConfig = currentStatus["loadedConfig"]
    if currentStatus['runOngoing']:
        log("Run already on-going - bailing out","error",runControl, logger)
        return 1
    update_rcgui(runControl, args[0], len(cfgs), 0, seqnumber)
    if "initCommand" in config:
        log("Running init command","info", runControl, logger)
        try :
            run_commands(config["initCommand"], runControl, logger)
        except RuntimeError :
            log("Failed in init command, giving up...","error", runControl, logger)
            finalize_and_clean(config.get("finalizeCommand"), runControl, logger = logger)
            return 1

    mm.message(f"Starting a sequence run ({args[0]}) with {len(cfgs)-startStep+1} steps")
    log(f"Starting a sequence run ({args[0]}) with {len(cfgs)-startStep+1} steps","info",  runControl, logger) 
    for step in range(startStep-1,len(cfgs)):

        # checking for stop flag
        if check_stopFlag():
            log(f"Stopping at step {step + 1}","info", runControl, logger)
            break

        update_rcgui(runControl, args[0],len(cfgs), step+1,seqnumber) #type: ignore
        print(f"Running step {step+1}")
        log(f"Running step {step+1}","info",runControl, logger) 
        cfg=cfgs[step]
        run=runner(cfg,runControl,seqnumber,step+1, socketio, logger)
        rc=run.run()
        if rc:
            log(f"Successful run","info", runControl , logger) 
        else:
            mm.message("Failed to start run from sequencer - please check")
            log(f"Failed to start run from sequencer - please check ;\nTo redo from this step, start a new sequence with the following parameters : Sequence : {args[0]}, Sequence Number : {seqnumber}, Step : {step+1}. It is also possible to start a new run using the command : ./sequencer.py -S {seqnumber} -s {step+1} {args[0]}","error", runControl, logger) 
            finalize_and_clean(config.get("finalizeCommand"), runControl, logger = logger)
            runControl.change_config(initialConfig)
            return 1
        if seqnumber==0:
            seqnumber=run.runnumber

    if "finalizeCommand" in config:
        log(f"Running finalize command", "info", runControl, logger) 

        try :
            run_commands(config["finalizeCommand"], runControl, logger)
        except RuntimeError :
            log(f"Failed in finalize command","error",runControl, logger) 
            mm.message("Failed at sequencer stop - please check")
            log(f"Failed at sequencer stop - please check","error", runControl, logger) 
            runControl.change_config(initialConfig)
            update_rcgui(runControl,"",0,0,0,end=True) 
            return 1
    if "analysisCommand" in config:
        log(f"Running analysis command","info", runControl , logger)
        try :
            run_commands(config["analysisCommand"]+f" {seqnumber}", runControl, logger)
            if "resultsLink" in config:
                log(f"Analysis is done - check at {config['resultsLink']}", "info", runControl, logger)
            else:
                log(f"Analysis is done", "info", runControl, logger)
        except RuntimeError :
            log(f"Failed in analysis command", "error", runControl, logger)
    
    runControl.change_config(initialConfig)
    update_rcgui(runControl, args[0],len(cfgs),0, seqnumber, end=True) # type: ignore
    if r.get("stopSequencer") is not None:
        mm.message("Sequence run completed after manual stop")
        log(f"Sequence run completed after manual stop","info", runControl, logger) 
    else : 
        mm.message("Sequence run completed")
        log(f"Sequence run completed","info", runControl, logger) 
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))


