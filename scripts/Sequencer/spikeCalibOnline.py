#!/usr/bin/env python3

#
#  Copyright (C) 2019-2023 CERN for the benefit of the FASER collaboration
#

import dateutil.parser
import json
import os
import platform
import requests
import subprocess
import sys
import urllib3

import analysisUtils

influxDB=json.load(open("/etc/faser-secrets.json"))
urllib3.disable_warnings()


def getSpikeRate(hists,deltaT):
    charges=[]
    for ch in range(6,16):
        hist=hists['digitizermonitor01'][f'h_h_peak_ch{ch:02d}']
        charges.append(analysisUtils.histAbove(hist,1000)/deltaT)
    return charges

expectedConfigs=["tlbDigiTI12PMTs","tlbDigiTI12"]

if __name__ == '__main__':
    store=True
    seqNumber=int(sys.argv[1])
    if len(sys.argv)>2: store=True
    runs=analysisUtils.getSequence(seqNumber)
    if len(runs)>10:
        print("Incorrent number of runs detected - no analysis done")
        sys.exit(0)
    rates=[]
    for num,run in enumerate(runs):
        info=analysisUtils.getRunInfo(run)
        if num==0:
            ts=dateutil.parser.isoparse(info['starttime']+"Z")
        if num>6: continue
        deltaT=(dateutil.parser.isoparse(info['stoptime']+"Z")-dateutil.parser.isoparse(info['starttime']+"Z")).total_seconds()
        if info['configName'].replace(".json","") not in expectedConfigs:
            raise Exception(f"Found wrong config for run {run}: {info['configName']} - expected {expectedConfigs[num]}")
#            print("warning wrong config")
        hists=analysisUtils.getHistograms(int(run))
        rates.append(getSpikeRate(hists,deltaT))

    host=platform.node()
    calibData=[]
    for ch in range(6,16):
        rateData=[]
        for vv,rate in enumerate(rates):
            rateData.append(f'rate{vv*100}V={rate[ch-6]}')
        rateData=','.join(rateData)
        calibData.append(f'PMTSpikeCalibration,Host={host},ch={ch},seq={seqNumber} {rateData} {int(ts.timestamp())}000000000')
#    print("\n".join(calibData))
   
    if store:
        r=requests.post(f'https://dbod-faser-influx-prod.cern.ch:8080/write?db={influxDB["INFLUXDB"]}',
                        auth=(influxDB["INFLUXUSER"],influxDB["INFLUXPW"]),
                        data="\n".join(calibData),
                        verify=False)
        if r.status_code!=204:
            print("ERROR: Failed to post calibration to influxdb: "+r.text)
            sys.exit(1)

