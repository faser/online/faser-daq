#!/usr/bin/env python

#
#  Copyright (C) 2019-2023 CERN for the benefit of the FASER collaboration
#

import dateutil.parser
import json
import platform
import requests
import subprocess
import sys
import urllib3

import analysisUtils

influxDB=json.load(open("/etc/faser-secrets.json"))
urllib3.disable_warnings()

def getMeanCharges(hists):
    charges=[]
    for ch in range(24):
        hist=hists['digitizermonitor01'][f'h_h_charge_ch{ch:02d}']
        charges.append(analysisUtils.histMean(hist))
    return charges

expectedConfigs=["tlbDigiTI12LEDAll"]

if __name__ == '__main__':
    seqNumber=int(sys.argv[1])
    runs=analysisUtils.getSequence(seqNumber)
    if len(runs)!=1:
        print("Incorrent number of runs detected - no analysis done")
        sys.exit(0)
    charges=[]
    for num,run in enumerate(runs):
        info=analysisUtils.getRunInfo(run)
        if num==0:
            ts=dateutil.parser.isoparse(info['starttime']+"Z")
        if info['configName'].replace(".json","")!=expectedConfigs[num]:
            raise Exception(f"Found wrong config for run {run}: {info['configName']} - expected {expectedConfigs[num]}")
        hists=analysisUtils.getHistograms(int(run))
        charges.append(getMeanCharges(hists))

    host=platform.node()
    calibData=[]
    for ch in range(24):
        meanCharge=charges[0][ch]
        calibData.append(f'PMTLEDCalibration,Host={host},ch={ch},seq={seqNumber} charge={meanCharge} {int(ts.timestamp())}000000000')

    r=requests.post(f'https://dbod-faser-influx-prod.cern.ch:8080/write?db={influxDB["INFLUXDB"]}',
                    auth=(influxDB["INFLUXUSER"],influxDB["INFLUXPW"]),
                    data="\n".join(calibData),
                    verify=False)
    if r.status_code!=204:
        print("ERROR: Failed to post calibration to influxdb: "+r.text)
        sys.exit(1)

