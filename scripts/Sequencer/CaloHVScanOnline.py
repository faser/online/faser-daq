#!/usr/bin/env python

#
#  Copyright (C) 2019-2023 CERN for the benefit of the FASER collaboration
#

import dateutil.parser
import json
import platform
import re
import requests
import subprocess
import sys
import urllib3

import analysisUtils

influxDB=json.load(open("/etc/faser-secrets.json"))
urllib3.disable_warnings()

def getMeanCharges(hists):
    charges=[]
    for ch in list(range(4))+list(range(16,20)):
        hist=hists['digitizermonitor01'][f'h_h_charge_ch{ch:02d}']
        charges.append(analysisUtils.histMean(hist))
    return charges

if __name__ == '__main__':
    seqNumber=int(sys.argv[1])
    runs=analysisUtils.getSequence(seqNumber)
    if len(runs)!=10:
        print("Incorrent number of runs detected - no analysis done")
#        sys.exit(0)
    charges=[]
    for num,run in enumerate(runs):
        info=analysisUtils.getRunInfo(run)
        comment=info['startcomment']
        HV=re.match(".+HV=(\d+)V",comment).group(1)
        if num==0:
            ts=dateutil.parser.isoparse(info['starttime']+"Z")
        if info['configName'].replace(".json","")!="tlbDigiTI12LED":
            raise Exception(f"Found wrong config for run {run}: {info['configName']} - expected {expectedConfigs[num]}")
#            print("warning wrong config")
        hists=analysisUtils.getHistograms(int(run))
        charges.append(getMeanCharges(hists))

        print(HV,charges[-1])

#    calibData.append(f'CaloLEDCalibration,Host={host},ch={ch},seq={seqNumber} highQ={meanHigh},lowQ={lowReg},precQ={lowPrec},ratio={ratioReg},precRatio={ratioPrec} {int(ts.timestamp())}000000000')
    #print("\n".join(calibData))
#    r=requests.post(f'https://dbod-faser-influx-prod.cern.ch:8080/write?db={influxDB["INFLUXDB"]}',

#                    auth=(influxDB["INFLUXUSER"],influxDB["INFLUXPW"]),
#                    data="\n".join(calibData),
#                    verify=False)
#    if r.status_code!=204:
#        print("ERROR: Failed to post calibration to influxdb: "+r.text)
#        sys.exit(1)

