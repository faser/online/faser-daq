/*
  Copyright (C) 2019-2023 CERN for the benefit of the FASER collaboration
*/
#pragma once

#include "Commons/FaserProcess.hpp"
#include "Utils/Binary.hpp"
#include "folly/ProducerConsumerQueue.h"
#include "Utils/Ers.hpp"
#include "Utils/Common.hpp"
#include "Utils/ReusableThread.hpp"
#include "EventFormats/CompressData.hpp"

#include <condition_variable>
#include <map>
#include <memory>
#include <queue>
#include <tuple>
#include <string>

ERS_DECLARE_ISSUE(EventCompressorIssues,                                                             // Namespace
                  CompressionFailed,                                                   // Class name
                  "Failed to compress event", // Message
                  ERS_EMPTY)

ERS_DECLARE_ISSUE(
    EventCompressorIssues,                                                            
    CompressionEngineIssue,                                                    
    message,
    ((std::string) message)
)

class  CompressionEngineModule : public FaserProcess {
 public:
  CompressionEngineModule(const std::string& n);
  ~CompressionEngineModule();
  void configure(); // optional (configuration can be handled in the constructor)
  void start(unsigned);
  void stop();
  void runner() noexcept;
  

  private:
  struct ThreadContext {
    ThreadContext(std::array<unsigned int, 2> tids) : consumer(tids[0]), producer(tids[1]) {}
    daqling::utilities::ReusableThread consumer;
    daqling::utilities::ReusableThread producer;
  };
  using PayloadQueue = folly::ProducerConsumerQueue<DataFragment<daqling::utilities::Binary>>;
  using Context = std::tuple<PayloadQueue, ThreadContext>;

  size_t m_buffer_size;
  int m_stop_timeout;
  std::string compressionLevel;
  std::string Compressor;
  std::atomic<bool> m_start_completed;
  std::atomic<bool> m_stopWriters;
  std::atomic<size_t> m_events_sent = 0;
  std::atomic<size_t> m_events_received = 0;
  std::atomic<float> m_compression_ratio = 0;
  std::atomic<size_t> m_payload_queue_size = 0;
  std::map<uint64_t, Context> m_compressionContexts;
  void flusher(PayloadQueue &pq,std::string Compressor,const size_t max_buffer_size);

};


