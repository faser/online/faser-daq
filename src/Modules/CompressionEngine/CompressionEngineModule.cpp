/*
  Copyright (C) 2019-2023 CERN for the benefit of the FASER collaboration
*/
#include <chrono>
#include <ctime>
#include <sstream>

#include "CompressionEngineModule.hpp"

using namespace std::chrono_literals;
namespace daqutils = daqling::utilities;
using namespace EventCompressorIssues;

CompressionEngineModule::CompressionEngineModule(const std::string &n) : FaserProcess(n), m_stopWriters{false}
{
  ERS_INFO("The Compression Engine is created");
}

CompressionEngineModule::~CompressionEngineModule()
{
  INFO("With config: " << m_config.dump());
}

// optional (configuration can be handled in the constructor)
void CompressionEngineModule::configure()
{
  FaserProcess::configure();
  ERS_INFO("Starting to initilize compressor");
  auto cfg = getModuleSettings();
  Compressor = cfg.value("Compressor", "ZSTD");
  compressionLevel = getModuleSettings()["level"];
  //m_stop_timeout = 1200;
  m_stop_timeout = getModuleSettings().value("stop_timeout_ms", 1200);
  m_buffer_size = getModuleSettings().value<unsigned int>("buffer_size", 100);
  if (Compressor == "ZSTD" || Compressor == "Zlib" || Compressor == "LZ4")
    {
      DEBUG("A Valid Compressor is selected");
    }
    else
    {
      ERROR("An Invalid Compressor is selected");
      m_status = STATUS_ERROR;
      throw CompressionEngineIssue(ERS_HERE,"Bad Compressor");
    }
  INFO("Setting Compression level to "+compressionLevel);

  registerVariable(m_compression_ratio,"Event_Compression_ratio");
  registerVariable(m_events_received,"Events_recieved_for_compression");
  registerVariable(m_events_sent,"Compressed_events_sent_out");
  registerVariable(m_payload_queue_size,"PayloadQueueSizeInternal");
  ERS_INFO("Configured module");
}

void CompressionEngineModule::start(unsigned run_num)
{
  m_status = STATUS_OK;
  m_stopWriters.store(false);
  unsigned int threadid = 22222;       // CCC: magic
  constexpr size_t queue_size = 10000; // CCC: magic
    // For the channel 0, construct a context of a payload queue, a consumer thread, and a producer
    // thread.
    std::array<unsigned int, 2> tids = {threadid++, threadid++};
    const auto & [ it, success ] =
        m_compressionContexts.emplace(0, std::forward_as_tuple(queue_size, std::move(tids)));
    assert(success);

    // Start the context's consumer thread.
    std::get<ThreadContext>(it->second)
    .consumer.set_work(&CompressionEngineModule::flusher,this, // Threaded function
    std::ref(std::get<PayloadQueue>(it->second)), // Primary
    Compressor,m_buffer_size // Auxiliary Argumets;
    );
  assert(m_compressionContexts.size() == 1); // Only one context may be created
  FaserProcess::start(run_num); 
  
}

void CompressionEngineModule::stop()
{
  std::this_thread::sleep_for(std::chrono::milliseconds(m_stop_timeout));
  FaserProcess::stop();
  m_stopWriters.store(true);
  for (auto & [ chid, ctx ] : m_compressionContexts) {
    while (!std::get<ThreadContext>(ctx).consumer.get_readiness()) {
      std::this_thread::sleep_for(1ms);
    }
  }
  m_compressionContexts.clear();
  INFO("Events Recieved by module  "<<m_events_received);
  INFO("Events Sent by module  "<<m_events_sent);
}

void CompressionEngineModule::runner() noexcept
  {
    ERS_INFO("Running...");
    for (auto & [ chid, ctx ] : m_compressionContexts) 
    {
      std::get<ThreadContext>(ctx).producer.set_work([&]()
      {
        auto &pq = std::get<PayloadQueue>(ctx);
        auto receiverChannel = m_config.getConnections(m_name)["receivers"][0]["chid"];
        while (m_run)
        {
          DataFragment<daqling::utilities::Binary> blob;
          auto chid = receiverChannel; // set to 0 for Physics data
          while (!m_connections.receive(chid, blob) && m_run)
          {
            if (m_statistics) 
            {
              m_payload_queue_size = pq.sizeGuess();
            }
            std::this_thread::sleep_for(1ms);
          }
          DEBUG(" Received " << blob.size() << "B payload on channel: " << chid);
          if (pq.sizeGuess() > 990)
          {
            WARNING("Payload Queue is filling up !!");
            m_status = STATUS_WARN;
          }
          while (m_run && !pq.write(blob));
          if (m_statistics && blob.size()) 
          {
            m_events_received++;
          }
        }
      });
    }
    while (m_run) 
    {
      std::this_thread::sleep_for(1ms);
    };
    ERS_INFO("Runner stopped");
  }

  void CompressionEngineModule::flusher(PayloadQueue &pq,std::string Compressor,const size_t max_buffer_size)
  {
    std::unique_ptr<DataCompression> compressorUsedFlush = nullptr;
    std::unique_ptr<DAQFormats::EventFull> eventGot;
    std::vector<DataFragment<daqutils::Binary>> eventBuffer;
    size_t maxBufferedEvents = max_buffer_size;
    if (Compressor == "ZSTD")
    {
      compressorUsedFlush = std::make_unique<ZSTDCompress>();
    }else if (Compressor == "Zlib")
    {
      compressorUsedFlush = std::make_unique<ZlibCompress>();
    }
    else if (Compressor == "LZ4")
    {
      compressorUsedFlush = std::make_unique<LZ4Compress>();
    }
    compressorUsedFlush->setCompressionLevel(compressionLevel);
    auto senderChannel = m_config.getConnections(m_name)["senders"][0]["chid"];
    const auto flushVector = [&](std::vector<DataFragment<daqutils::Binary>> &eventBufferVector) {
      float uncompressed_size = 0;
      float compressed_size = 0;
      for (auto & blob : eventBufferVector)
      {
        eventGot = std::make_unique<DAQFormats::EventFull>(blob.data<uint8_t *>(), blob.size());
        uncompressed_size+=eventGot->payload_size();
        if(compressorUsedFlush->compress(eventGot) == false)
        {
          m_status = STATUS_ERROR;
        }
        compressed_size+=eventGot->payload_size();
        unsigned int channel = senderChannel; // Channel to send out compressed data 
        auto *bytestream = eventGot->raw();
        DataFragment<daqling::utilities::Binary> binData(bytestream->data(), bytestream->size());
        DEBUG("Sending event " << eventGot->event_id() << " - " << eventGot->size() << " bytes on channel " << channel);
        m_events_sent++;
        m_connections.send(channel, binData); // to file writer
        delete bytestream;
      }
      eventBufferVector.clear();
      if (compressed_size!=0)
      {
        m_compression_ratio.store(uncompressed_size/compressed_size);
      }
    };
  while (!m_stopWriters) {
    while (pq.isEmpty() && !m_stopWriters) { // wait until we have something to write
      flushVector(eventBuffer);
      std::this_thread::sleep_for(1ms);
    };

  if (m_stopWriters) {
    flushVector(eventBuffer);
    return;
  }

  auto payload = pq.frontPtr();
  DataFragment<daqutils::Binary> eventProcess(payload->data(), payload->size());
  eventBuffer.push_back(eventProcess);
  eventProcess = DataFragment<daqutils::Binary>();
  if(eventBuffer.size() > maxBufferedEvents )
  {
    flushVector(eventBuffer);
  }
  pq.popFront();
  
  }
}
