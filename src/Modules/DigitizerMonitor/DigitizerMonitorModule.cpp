/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
/// \cond
#include <algorithm>  // std::fill
#include <chrono>
#include <fstream>   // std::ofstream
#include <iostream>  // std::flush
#include <map>
#include <sstream>  // std::ostringstream
/// \endcond

#include <EventFormats/DAQFormats.hpp>

#include "DigitizerMonitorModule.hpp"

using namespace std::chrono_literals;
using namespace std::chrono;

DigitizerMonitorModule::DigitizerMonitorModule(const std::string& n) : MonitorBaseModule(n) {
    INFO("Instantiating ...");
}

DigitizerMonitorModule::~DigitizerMonitorModule() {
    INFO("With config: " << m_config.dump());
}

void DigitizerMonitorModule::configure(){
    INFO("Configuring...");

    auto cfg = getModuleSettings();
    m_cfg_min_collisions = cfg["min_collisions"];
    ;
    if (cfg.contains("nb_digitizers")) {
        m_nb_digitizers = cfg["nb_digitizers"].get<size_t>();
    } else {
        m_nb_digitizers = 1;
    }
    if (m_nb_digitizers > NDIGITIZERS) {
        m_status = STATUS_ERROR;
        throw MonitorBase::ConfigurationIssue(ERS_HERE,"nb_digitizers is set to a value greater than the maximum number of digitizers allowed");
    }

    if (cfg.contains("scale")) {
        m_scale = cfg["scale"].get<std::vector<float>>();  // vector of size 2 * 16
    } else {
        m_scale = std::vector<float>(16, 2.0);
    }

    if (cfg.contains("mapping_channels")) {
        m_chan_map = cfg["mapping_channels"].get<std::unordered_map<std::string, size_t>>();
        m_with_collisions = true;
	m_clock_channel = m_chan_map["input_clock"];
    } else {
        m_chan_map = m_default_channels;
        m_with_collisions = false;
	m_clock_channel = 10000; // treat all channels as regular channels
    }
    m_cfg_nominal_t0 = cfg["nominal_t0"];
    if (m_with_collisions){
	if (m_cfg_nominal_t0 == nullptr || m_cfg_nominal_t0.size() < NCHANNELS * m_nb_digitizers){
	    ERROR("Digitizer monitor configuration error: 'nominal_t0' configuration is invalid!");
            m_status = STATUS_ERROR;
	    throw MonitorBase::ConfigurationIssue(ERS_HERE,"'nominal_t0' configuration is invalid!");
	}
    }
 
    MonitorBaseModule::configure();

}

void DigitizerMonitorModule::start(unsigned int run_num) {
    // sets to 0 all values inside the array (8 values)
    for (int inputBit = 0; inputBit < 8; inputBit++) {
        m_intime[inputBit] = 0;
        m_late[inputBit] = 0;
        m_early[inputBit] = 0;
    }

    MonitorBaseModule::start(run_num);
}

void DigitizerMonitorModule::monitor(DataFragment<daqling::utilities::Binary>& eventBuilderBinary) {
    DEBUG("Digitizer monitoring");
    // the m_event object is populated with the event binary here
    auto evtHeaderUnpackStatus = unpack_event_header(eventBuilderBinary);
    if (evtHeaderUnpackStatus) return;

    // consistency check that the event received is the type of data we are configured to take
    if (m_event->event_tag() != m_eventTag) {
        ERROR("Event tag does not match filter tag. Are the module's filter settings correct?");
        return;
    }

    // storing the data from all channels
    // channels with no data are represented by a empty vector (size 0)
    std::vector<uint32_t> time_tag(m_nb_digitizers);
    size_t indexChan = 0;
    for (size_t i = 0; i < m_nb_digitizers; i++) {
        auto fragmentUnpackStatus = unpack_full_fragment(eventBuilderBinary, SourceIDs::PMTSourceID | i);
        if (fragmentUnpackStatus) {
            ERROR("Error in unpacking");
            return;
        }
        if (!m_pmtdataFragment->valid()) {
            ERROR("Invalid PMT fragment. Skipping event!");
            return;
        }

        DEBUG("EventSize : " << m_pmtdataFragment->event_size());
        uint16_t payloadSize = m_fragment->payload_size();
	time_tag[i]=m_pmtdataFragment->trigger_time_tag();
        m_histogrammanager->fill("h_digitizer_payloadsize_" + i, payloadSize);
        m_metric_payload = payloadSize;
        for (size_t iChan = 0; iChan < NCHANNELS; iChan++) {
            if (!m_pmtdataFragment->channel_has_data(iChan)) {
                indexChan++;
                continue;
            }
            m_channels_data[indexChan] = m_pmtdataFragment->channel_adc_counts(iChan);
            indexChan++;
        }
    }

    // Now, all channel data are in the m_channels_data vector, with index starting at 0 and going until 31 (for 2 digitizers)
    // 0 -> 15 : first digitizer
    // 16 -> 31 : second digitizer

    std::vector<float> peaks(NCHANNELS * m_nb_digitizers);
    std::vector<float> tzeros(NCHANNELS * m_nb_digitizers);
    std::vector<std::vector<float>> signals(NCHANNELS * m_nb_digitizers);

    // anything worth doing to all channels
    bool saturated = false;
    for (size_t iChan = 0; iChan < NCHANNELS * m_nb_digitizers; iChan++) {  // for all channels that have data
        if (m_channels_data[iChan].empty()) continue;
        std::string chStr = std::to_string(iChan);
        if (iChan < 10) chStr = "0" + chStr;

        std::vector<u_int16_t>& v = m_channels_data[iChan];

        // mean and rms for monitoring a channel that goes out of wack
        float avg = GetPedestalMean(v, 0, 50);
        float rms = GetPedestalRMS(v, 0, 50);

        m_avg[iChan] = avg;
        if (m_rms[iChan] == 0) m_rms[iChan] = rms;        // initialize on first event
        m_rms[iChan] = 0.02 * rms + 0.98 * m_rms[iChan];  // exponential moving average

        // convert to mV and find peaks
        std::vector<float>& signal = signals[iChan];
        signal.resize(v.size());
        float min_value = 0;
        float max_value = 0;
        size_t max_idx = 0;
        for (size_t ii = 0; ii < v.size(); ii++) {
            if (v[ii] < 10 && iChan != m_clock_channel) saturated = true;
            float value = (avg - v[ii]) * m_scale[iChan] / 16.384;
            if (value > max_value) {
                max_value = value;
                max_idx = ii;
            }
            if (value < min_value) min_value = value;
            signal[ii] = value;
        }
        // find t0
        const int delta = 3;
        const float k = 0.3;
        max_idx = std::min(max_idx, v.size() - delta - 2);  // protect against overflow
        max_idx += delta;
        while ((k * signal[max_idx] - signal[max_idx - delta] < 0) && max_idx != delta) max_idx--;
        double y1 = k * signal[max_idx] - signal[max_idx - delta];
        double y2 = k * signal[max_idx + 1] - signal[max_idx - delta + 1];
        double dt = y1 / (y1 - y2);
        double t0 = 2. * (max_idx + dt - v.size() + 300);
        tzeros[iChan] = t0;
        // example pulse
        if ((-min_value) > m_display_thresh || (max_value > m_display_thresh)) {
            FillChannelPulse("h_pulse_ch" + chStr, signal);
        }
        float peak = max_value;
        peaks[iChan] = peak;
        if ((-min_value) > peak) peak = min_value;

        if (m_charge_range != 0 && iChan != m_clock_channel) {
            size_t startBin = 380;
            if (v.size() < 400) startBin = 80;
            if (startBin + m_charge_range <= signal.size()) {
                float charge = Integral(signal, startBin, m_charge_range);
                m_histogrammanager->fill("h_charge_ch" + chStr, charge, 1.0);
            }
        }

        m_histogrammanager->fill("h_peak_ch" + chStr, peak, 1.0);
        for (int ii = 0; ii < THRESHOLDS; ii++) {
            if (m_thresholds[iChan][ii] && peak > m_thresholds[iChan][ii])
                m_thresh_counts[iChan][ii]++;
        }
    }

    // select collision events without saturated channels
    if (m_with_collisions) {
        auto tlb = get_tlb_data_fragment(eventBuilderBinary);
        if (peaks[m_chan_map["fasernu_ch0"]] > 50 && peaks[m_chan_map["fasernu_ch1"]] > 50 && peaks[m_chan_map["veto_st2_ch0"]] > 100 && peaks[m_chan_map["veto_st2_ch1"]] > 100 && ((peaks[m_chan_map["btm_timing_ch0"]] > 25 && peaks[m_chan_map["btm_timing_ch1"]] > 25) || (peaks[m_chan_map["top_timing_ch0"]] > 25 && peaks[m_chan_map["top_timing_ch1"]] > 25)) && m_cfg_min_collisions != nullptr) {
            m_collisionLike++;
            if (saturated) m_saturatedCollisions++;
            if (!m_channels_data[m_clock_channel].empty() && !saturated) {  // assume that clock data is here
                float phase = FFTPhase(signals[m_clock_channel]);
                if (phase < -5) phase += 25;
		if (m_clock_channel>15) {
		  if (time_tag[0]<time_tag[1]) phase +=16; // board with clock was read out one 16ns cycle later
		}
                m_histogrammanager->fill("h_clockphase", phase);

                for (size_t iChan = 0; iChan < NCHANNELS * m_nb_digitizers; iChan++) {
                    if (iChan == m_clock_channel) continue;
                    if (peaks[iChan] < 5) continue;  // Need a minimum signal

                    std::string chStr = std::to_string(iChan);
                    if (iChan < 10) chStr = "0" + chStr;

                    float t0 = tzeros[iChan] - phase - float(m_cfg_nominal_t0[iChan]);
                    m_histogrammanager->fill("h_time_ch" + chStr, t0);
                    if (fabs(t0 - m_t0[iChan]) < 10)                   // reject outliers, relying on being centered at 0
                        m_t0[iChan] = 0.02 * t0 + 0.98 * m_t0[iChan];  // exponential moving average
                }
            }
            auto inputBitsNext = tlb.input_bits_next_clk();
            auto inputBits = tlb.input_bits();
            bool early = false;
            for (size_t inputBit = 0; inputBit < 8; inputBit++) {
                if (((inputBits & (1 << inputBit)) == inputBits) && inputBitsNext != 0) {
                    early = true;
                    m_early[inputBit]++;
                }
            }
            if (!early && !saturated) {
                for (size_t inputBit = 0; inputBit < 8; inputBit++) {
                    if ((inputBitsNext & (1 << inputBit)) != 0) {
                        // FIXME: hardcoded combinations
                        float missedSignal = 0;
                        if (inputBit < 2)
                            missedSignal = std::max(peaks[inputBit * 2], peaks[inputBit * 2 + 1]);
                        else if (inputBit > 6)
                            missedSignal = peaks[inputBit * 2];
                        else
                            missedSignal = std::min(peaks[inputBit * 2], peaks[inputBit * 2 + 1]);
                        m_late[inputBit]++;
                        m_histogrammanager->fill("h_late_bit" + std::to_string(inputBit), missedSignal);
                    }
                }
            }
            for (int inputBit = 0; inputBit < 8; inputBit++) {
                if ((inputBits & (1 << inputBit)) != 0) {
                    m_intime[inputBit]++;
                    if (m_intime[inputBit] + m_late[inputBit] >= int(m_cfg_min_collisions)) {
                        m_lateTrig[inputBit] = 1. * m_late[inputBit] / (m_late[inputBit] + m_intime[inputBit]);
                        m_earlyTrig[inputBit] = 1. * m_early[inputBit] / (m_late[inputBit] + m_intime[inputBit]);
                    }
                }
            }
        }
    }
}

void DigitizerMonitorModule::register_hists() {
    INFO(" ... registering histograms in DigitizerMonitor ... ");
    auto cfg = getModuleSettings();


    m_display_thresh = cfg["display_thresh"].get<float>();

    // payload size for both digitizers
    for (size_t i = 0; i < m_nb_digitizers; i++) {
        m_histogrammanager->registerHistogram("h_digitizer_payloadsize_" + i, "payload size [bytes]", -0.5, 545.5, 275, m_PUBINT);
    }

    // payload status
    const std::vector<std::string> categories = {"Ok", "Unclassified", "BCIDMistmatch", "TagMismatch", "Timeout", "Overflow", "Corrupted", "Dummy", "Missing", "Empty", "Duplicate", "DataUnpack"};
    m_histogrammanager->registerHistogram("h_digitizer_errorcount", "error type", categories, m_PUBINT);

    // synthesis common for all channels
    size_t buffer_length = cfg["buffer_length"].get<size_t>();

    m_charge_range = 0;
    float max_charge = 0;
    if (cfg.contains("charge_integral_range")) {
        m_charge_range = cfg["charge_integral_range"];
        max_charge = cfg["charge_maximum"];
    }
    auto thresholds = cfg["rate_thresholds"];
    //
    for (size_t iChan = 0; iChan < NCHANNELS * m_nb_digitizers; iChan++) {
        if (thresholds[iChan] == 0) {  // or  == [0] ?
            continue;                  // unused channel, skipping metrics registration
        }
        std::string chStr = std::to_string(iChan);
        if (iChan < 10) chStr = "0" + chStr;
        // example pulse
        m_histogrammanager->registerHistogram("h_pulse_ch" + chStr, "ADC Pulse ch" + std::to_string(iChan) + " Sample Number", "Inverted signal [mV]", -0.5, buffer_length - 0.5, buffer_length, m_PUBINT);
        m_histogrammanager->registerHistogram("h_peak_ch" + chStr, "Peak signal [mV]", -200, 2000, 550, m_PUBINT);
        if (iChan == m_clock_channel) continue;
        m_histogrammanager->registerHistogram("h_time_ch" + chStr, "Peak timing [ns]", -30, 30, 300, m_PUBINT);
        if (m_charge_range) {
            m_histogrammanager->registerHistogram("h_charge_ch" + chStr, "Charge [pC]", -5, max_charge, 1000, m_PUBINT);
        }
    }
    for (int inputBit = 0; inputBit < 8; inputBit++) {
        m_histogrammanager->registerHistogram("h_late_bit" + std::to_string(inputBit), "Peak signal for late triggers [mV]", 0, 500, 250, m_PUBINT);
    }
    m_histogrammanager->registerHistogram("h_clockphase", "Clock phase [ns]", -50, 50, 500, m_PUBINT);
    INFO(" ... done registering histograms ... ");
    return;
}

void DigitizerMonitorModule::register_metrics() {
    INFO("... registering metrics in DigitizerMonitorModule ... ");
    auto cfg = getModuleSettings();

    m_metric_payload = 0;
    registerVariable(m_metric_payload, "payload");

    json thresholds = cfg["rate_thresholds"];
    for (size_t iChan = 0; iChan < NCHANNELS * m_nb_digitizers; iChan++) {
        if (thresholds[iChan] == 0) {  // or  == [0] ?
            continue;                  // unused channel, skipping metrics registration
        }

        std::string chStr = std::to_string(iChan);
        if (iChan < 10) chStr = "0" + chStr;

        json ch_thresh = thresholds[iChan];
        if (ch_thresh.size() > THRESHOLDS) {
            ERROR("Too many thresholds specified for channel " + chStr);
        }
        for (unsigned int ii = 0; ii < THRESHOLDS; ii++) {
            m_thresholds[iChan][ii] = 0;
            if (ii < ch_thresh.size()) {
                float thresh = ch_thresh[ii];
                if (thresh == 0) continue;
                registerVariable(m_thresh_counts[iChan][ii], "rate_ch" + chStr + "_" + std::to_string(thresh) + "mV", daqling::core::metrics::RATE);
                m_thresholds[iChan][ii] = thresh;
            }
        }
        registerVariable(m_avg[iChan], "pedestal_mean_ch" + chStr);
        registerVariable(m_rms[iChan], "pedestal_rms_ch" + chStr);
        if (iChan != m_clock_channel)
            registerVariable(m_t0[iChan], "signal_timing_ch" + chStr);
    }
    for (int inputBit = 0; inputBit < 8; inputBit++) {
        registerVariable(m_lateTrig[inputBit], "Late_trigger_fraction_bit" + std::to_string(inputBit));
        registerVariable(m_earlyTrig[inputBit], "Early_trigger_fraction_bit" + std::to_string(inputBit));
    }
    registerVariable(m_collisionLike, "Collision_events");
    registerVariable(m_saturatedCollisions, "Saturated_collision_events");
    return;
}

float DigitizerMonitorModule::GetPedestalMean(const std::vector<uint16_t>& input, size_t start, size_t end) {
    CheckBounds(input, start, end);

    float sum = 0.0;
    float count = 0;

    for (size_t i = start; i < end; i++) {
        sum += input.at(i);
        count++;
    }

    if (count <= 0)
        return -1;

    return sum / count;
}

float DigitizerMonitorModule::FFTPhase(const std::vector<float>& data) {
    size_t N = data.size();
    float sumRe = 0;
    float sumIm = 0;
    int k = int(40.08 / 500. * N);  // clock/digitizer frequency
    for (size_t ii = 0; ii < N; ii++) {
        sumRe += data[ii] * cos(2 * M_PI * ii * k / N);
        sumIm += data[ii] * sin(2 * M_PI * ii * k / N);
    }
    return 25 * atan2(sumIm, sumRe) / 2 / M_PI;
}

float DigitizerMonitorModule::GetPedestalRMS(const std::vector<uint16_t>& input, size_t start, size_t end) {
    CheckBounds(input, start, end);

    float mean = GetPedestalMean(input, start, end);

    float sum_rms = 0;
    int count = 0;

    for (size_t i = start; i < end; i++) {
        sum_rms += (input.at(i) - mean) * (input.at(i) - mean);
        count++;
    }

    if (count <= 0)
        return -1;

    return pow(sum_rms / count, 0.5);
}

float DigitizerMonitorModule::Integral(const std::vector<float>& data, size_t startIdx, size_t num) {
    float sum = 0;
    for (size_t ii = startIdx; ii < startIdx + num; ii++) {
        sum += data[ii];
    }
    return sum / 25.;
}

void DigitizerMonitorModule::CheckBounds(const std::vector<uint16_t>& input, size_t& start, size_t& end) {
    // if the number of samples is less than the desired pedestal sampling length, then take the full duration
    if (start >= end) {
        WARNING("Pedestal calculation has a start after and end : [" << start << "," << end << "]");
        end = start;
    }

    // if the number of samples is less than the desired pedestal sampling length, then take the full duration
    if (end > input.size()) {
        WARNING("The pulse length is not long enough for a pedestal calculation of : [" << start << "," << end << "]");
        end = input.size();
    }
}

void DigitizerMonitorModule::FillChannelPulse(std::string histogram_name, std::vector<float>& values) {
    m_histogrammanager->reset(histogram_name);
    m_histogrammanager->fill(histogram_name, 0, 1, values);
}
