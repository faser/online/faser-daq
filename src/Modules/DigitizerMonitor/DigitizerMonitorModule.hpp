/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
#pragma once

#include "Modules/MonitorBase/MonitorBaseModule.hpp"
#include <cmath>
#include <cstdint>
#include <array>

#define NCHANNELS 16
#define NDIGITIZERS 2  // max number of digitizers that we can have in the setup
#define THRESHOLDS 4

class DigitizerMonitorModule : public MonitorBaseModule {
 public:
  DigitizerMonitorModule(const std::string&);
  ~DigitizerMonitorModule();
  void configure();
  void start(unsigned int);

  float GetPedestalMean(const std::vector<uint16_t>& input, size_t start, size_t end);
  float GetPedestalRMS(const std::vector<uint16_t>& input, size_t start, size_t end);
  float Integral(const std::vector<float>& data, size_t startIdx, size_t num);
  void CheckBounds(const std::vector<uint16_t> &input, size_t& start, size_t& end);

  float FFTPhase(const std::vector<float>& data);

  void FillChannelPulse(std::string histogram_name, std::vector<float>& values);
  float m_display_thresh;
 protected:

  void monitor(DataFragment<daqling::utilities::Binary> &eventBuilderBinary);
  void register_hists();
  void register_metrics();
  float m_thresholds[NCHANNELS*NDIGITIZERS][THRESHOLDS];
  int m_intime[8];
  int m_late[8];
  int m_early[8];

  std::atomic<float> m_avg[NCHANNELS*NDIGITIZERS];
  std::atomic<float> m_rms[NCHANNELS*NDIGITIZERS];
  std::atomic<float> m_t0[NCHANNELS*NDIGITIZERS];
  std::atomic<float> m_lateTrig[8];
  std::atomic<float> m_earlyTrig[8];
  std::atomic<int> m_thresh_counts[NCHANNELS*NDIGITIZERS][THRESHOLDS];
  std::atomic<int> m_collisionLike;
  std::atomic<int> m_saturatedCollisions;

  nlohmann::json m_cfg_min_collisions;
  nlohmann::json m_cfg_nominal_t0;
  size_t m_charge_range;
  std::array<std::vector<u_int16_t>,NCHANNELS*NDIGITIZERS > m_channels_data;

  std::vector<float> m_scale;
  size_t m_nb_digitizers; // # digitizers specified in the configuration file
  bool m_with_collisions;
  size_t m_clock_channel;
  std::unordered_map<std::string, size_t> m_chan_map; 
  std::unordered_map<std::string, size_t> m_default_channels = {
    {"fasernu_ch0", 4},
    {"fasernu_ch1", 5},
    {"veto_st2_ch0", 6},
    {"veto_st2_ch1", 7},
    {"btm_timing_ch0", 8},
    {"btm_timing_ch1", 9},
    {"top_timing_ch0", 10},
    {"top_timing_ch1", 11},
    {"input_clock", 15}
  };
};
