/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

#pragma once

// helper functions that do the actual communication with the digitizer
#include "Comm_vx1730.h"
#include "digitizerHandler.h"
#include "Helper.h"
// needed for external tools
#include <ers/Issue.h>

#include "Commons/FaserProcess.hpp"
#include "EventFormats/BOBRDataFragment.hpp"
#include "EventFormats/DAQFormats.hpp"
// needed for sendECR() and runner() protection
#include <mutex>
#include <array>
#include <memory>

constexpr int NDIGITIZERS = 2; // max number of digitizers in the setup 

ERS_DECLARE_ISSUE(
    DigitizerReceiver,       // namespace
    DigitizerHardwareIssue,  // issue name
    ERS_EMPTY,               // message
    ERS_EMPTY)
ERS_DECLARE_ISSUE_BASE(DigitizerReceiver,                                                             // namespace name
                       HostNameOrIPTooLong,                                                           // issue name
                       DigitizerReceiver::DigitizerHardwareIssue,                                     // base issue name
                       "This is too long of a name: " << ip << "Max length of IP hostname: " << len,  // message
                       ERS_EMPTY,                                                                     // base class attributes
                       ((std::string)ip)((unsigned)len)                                               // this class attributes
)
ERS_DECLARE_ISSUE_BASE(DigitizerReceiver,                          // namespace name
                       InvalidIP,                                  // issue name
                       DigitizerReceiver::DigitizerHardwareIssue,  // base issue name
                       "Invalid IP address: " << ip,               // message
                       ERS_EMPTY,                                  // base class attributes
                       ((std::string)ip)                           // this class attributes
)

class DigitizerReceiverModule : public FaserProcess {
   public:
    DigitizerReceiverModule(const std::string &);
    ~DigitizerReceiverModule();

    ///////////////////////////////////////////
    // Methods needed for FASER/DAQ
    ///////////////////////////////////////////
    void configure();  // optional (configuration can be handled in the constructor)
    void enableTrigger(const std::string &arg);
    void disableTrigger(const std::string &arg);
    void start(unsigned int);
    void stop();
    void sendECR();
    void runner() noexcept;

    ///////////////////////////////////////////
    // Digitizer specific methods and members
    ///////////////////////////////////////////

    // read BOBR data into data member
    bool readBOBR();

    BOBRDataFormat::BOBREventV1 m_bobrdata;

    // the digitizers Handler interface class
    // digitizerHandler *m_digiHand;
    std::unique_ptr<digitizerHandler> m_digiHand;
    std::array<MonitoringValuesHandler, NDIGITIZERS> m_monitoring_objs;

    // common to both digitizers

    size_t m_n_digitizers;

    // used to protect against the ECR and runner() from reading out at the same time
    std::mutex m_lock;

    // used for the trigger time to BCID conversion
    float m_ttt_converter;

    // software BCID fix for the digitizer
    float m_bcid_ttt_fix;

    // monitoring metrics
    std::atomic<int> m_info_udp_receive_timeout_counter;
    std::atomic<int> m_info_wrong_cmd_ack_counter;
    std::atomic<int> m_info_wrong_received_nof_bytes_counter;
    std::atomic<int> m_info_wrong_received_packet_id_counter;

    std::atomic<int> m_info_clear_UdpReceiveBuffer_counter;
    std::atomic<int> m_info_read_dma_packet_reorder_counter;

    std::atomic<int> m_udp_single_read_receive_ack_retry_counter;
    std::atomic<int> m_udp_single_read_req_retry_counter;

    std::atomic<int> m_udp_single_write_receive_ack_retry_counter;
    std::atomic<int> m_udp_single_write_req_retry_counter;

    std::atomic<int> m_udp_dma_read_receive_ack_retry_counter;
    std::atomic<int> m_udp_dma_read_req_retry_counter;

    std::atomic<int> m_udp_dma_write_receive_ack_retry_counter;
    std::atomic<int> m_udp_dma_write_req_retry_counter;

    std::atomic<int> m_loss_of_lock;

    std::atomic<int> m_bobr_statusword;
    std::atomic<int> m_bobr_timing;
    std::atomic<int> m_lhc_turncount;
    std::atomic<int> m_lhc_fillnumber;
    std::atomic<int> m_lhc_machinemode;
    std::atomic<float> m_lhc_beamenergy;
    std::atomic<int> m_lhc_intensity1;
    std::atomic<int> m_lhc_intensity2;
    std::atomic<float> m_lhc_frequency;

    unsigned int m_prev_seconds;
    unsigned int m_prev_microseconds;
    unsigned int m_prev_turncount;

    std::vector<uint64_t> m_prev_event_ids;

    // for SW trigger sending
    int m_sw_count;
    float m_software_trigger_rate;

    bool m_bobr;
    std::atomic<int> m_trigger_disabled;

};
