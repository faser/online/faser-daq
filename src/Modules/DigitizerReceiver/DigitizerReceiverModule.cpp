/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

#include "DigitizerReceiverModule.hpp"

#include <algorithm>
#include <cmath>

#include "BOBR.h"

#define HOSTNAME_MAX_LENGTH 100
#define NCHANNELS 16
using namespace DigitizerReceiver;
using std::to_string;
DigitizerReceiverModule::DigitizerReceiverModule(const std::string &n) : FaserProcess(n) {
    INFO("");
    INFO("DigitizerReceiverModule Constructor");

    auto cfg = getModuleSettings();

    // retrieve the ip address of the sis3153 master board
    // this is configured via the arp and route commands on the network switch
    INFO("Getting IP Address");
    char ip_addr_string[HOSTNAME_MAX_LENGTH];
    auto cfg_ip = cfg["host_pc"];
    if (cfg_ip != "" && cfg_ip != nullptr) {
        // check to make sure you can copy over the config
        int length = strlen(std::string(cfg_ip).c_str());
        if (length > HOSTNAME_MAX_LENGTH) {
            throw HostNameOrIPTooLong(ERS_HERE, std::string(cfg_ip), HOSTNAME_MAX_LENGTH);
        }

        // lookup the IP address dynamically and determines if its an IP address or a hostname
        std::string ip_str = get_ip_address(cfg["host_pc"].get<string>());
        strcpy(ip_addr_string, ip_str.c_str());
        INFO("Input address/host  : " << cfg["host_pc"]);
        INFO("Returned IP Address : " << ip_addr_string);

        // check to make sure the thing is an IP address
        if (!is_ip_address(ip_addr_string)) {
            strcpy(ip_addr_string, "0.0.0.0");
            throw InvalidIP(ERS_HERE, ip_addr_string);
        }
    } else {
        throw DigitizerHardwareIssue(ERS_HERE, "No valid IP address setting in the digitizer configuration.");
    }

    INFO("Digitizers IP Address : " << ip_addr_string);

    // make a new digitizers instance
    try {
        m_digiHand = std::make_unique<digitizerHandler>(ip_addr_string, cfg);
    } catch (const std::runtime_error &e) {
        throw DigitizerHardwareIssue(ERS_HERE, e.what());
    }

    m_n_digitizers = m_digiHand->n_digitizers();
    if (m_n_digitizers > NDIGITIZERS) {
      throw DigitizerHardwareIssue(ERS_HERE, "nb_digitizers is set to a value greater than the maximum number of digitizers allowed");
    }

    // test interface and board communication for both digitizers
    m_digiHand->test_communication();

    m_digiHand->configure_ethernet_transmission();

    // ethernet speed test
    // these can be removed if the bootup is too slow
    m_digiHand->perform_interface_speedtest(4);

    // store local run settings
    if (!cfg.contains("trigger_software_rate")) {
        INFO("You did not specify a SW trigger rate - we will set it to 0 to be safe.");
        m_software_trigger_rate = 0;
    } else {
        m_software_trigger_rate = cfg["trigger_software_rate"].get<float>();
    }
    INFO("Trigger rate for SW triggers at : " << m_software_trigger_rate);

    // check for consistency of SW triggers and acquisition
    if (m_software_trigger_rate != 0) {
        if (!cfg["trigger_acquisition"]["software"].get<bool>()) {
            WARNING("Inconsistent settings - you have enabled SW triggers to be sent but not acquiring on SW triggers.");
        }
    }

    // for the TLB conversion factor on the trigger time tag
    if (!cfg["parsing"].contains("ttt_converter")) {
        INFO("You did not specify the TTT converter, setting it to the LHC 40.08 MHz");
        m_ttt_converter = 40.08;
    } else {
        m_ttt_converter = cfg["parsing"]["ttt_converter"].get<float>();
    }
    INFO("Setting TLB-Digitizer TTT clock to : " << m_ttt_converter);

    // BCID matching parameter performs a software "delay" on the calculated BCID
    // by delaying the TriggerTimeTag by some fixed amount.  This "delay" can be positive
    // or negative and is tuned to match the BCID from the TLB
    if (!cfg["parsing"].contains("bcid_ttt_fix")) {
        INFO("You did not specify the BCID fix, setting it to 0.");
        m_bcid_ttt_fix = 0;
    } else {
        m_bcid_ttt_fix = cfg["parsing"]["bcid_ttt_fix"].get<float>();  // perhaps there is a better way to do this
    }
    INFO("Setting Digitizer BCID fix to : " << m_bcid_ttt_fix);

    if (cfg["useBOBR"]) {
        m_bobr = true;
        BOBR *bobr = new BOBR(m_digiHand->m_digitizers[0]->m_crate, false);
        int return_code = bobr->initPFC();
        if (return_code) {
            throw DigitizerHardwareIssue(ERS_HERE, "Failed to initialize PFC in BOBR");
        }
        unsigned char cdata;
        int reg = 0;  // TTC timing is specified in register 0
        return_code = bobr->readTTCRegister(reg, &cdata);
        if (return_code) {
            throw DigitizerHardwareIssue(ERS_HERE, "Failed to read TTC register in BOBR");
        }
        int m = cdata & 0x0F;
        int n = (cdata & 0xF0) >> 4;
        int K = (m * 15 + n * 16 + 30) % 240;
        INFO("BOBR: TTC delay was set to " << K * 0.10417 << " ns");
        auto cfg_ttc_delay = cfg["BOBR_delay"];
        if (cfg_ttc_delay == nullptr) {
            INFO("BOBR: No TTC delay specified - will keep old setting");
        } else {
	    float delay_value = static_cast<float>(cfg_ttc_delay);
            K = (static_cast<int>(delay_value / 0.10417)) % 240;
            n = K % 15;
            m = (K / 15 - n + 14) % 16;
            int nm = n * 16 + m;
            INFO("BOBR: writing TTC value K=" << K << " or delay=" << K * 0.10417 << " ns");
            return_code = bobr->writeTTCRegister(reg, nm);
            if (return_code) {
                throw DigitizerHardwareIssue(ERS_HERE, "Failed to write TTC register in BOBR");
            }
            return_code = bobr->readTTCRegister(reg, &cdata);
            if (return_code) {
                throw DigitizerHardwareIssue(ERS_HERE, "Failed to re-read TTC register in BOBR");
            }
            m = cdata & 0x0F;
            n = (cdata & 0xF0) >> 4;
            K = (m * 15 + n * 16 + 30) % 240;
            INFO("BOBR: TTC delay is now set to " << K * 0.10417 << " ns");
        }

    } else {
        m_bobr = false;
    }
}

DigitizerReceiverModule::~DigitizerReceiverModule() {
    INFO("DigitizerReceiverModule::Destructor");
}

// optional (configuration can be handled in the constructor)
void DigitizerReceiverModule::configure() {
    FaserProcess::configure();

    // register the metrics for monitoring data
    INFO("Configuring monitoring metrics");
    INFO("Number of monitored metrics : " << m_monitoring_objs.size());

    for (size_t i = 0; i < m_n_digitizers; i++) {
        registerVariable(m_monitoring_objs[i].hw_buffer_space, "BufferSpace_" + to_string(i));
        registerVariable(m_monitoring_objs[i].hw_buffer_occupancy, "HWBufferOccupancy_" + to_string(i));

        registerVariable(m_monitoring_objs[i].triggers, "TriggeredEvents_" + to_string(i));
        registerVariable(m_monitoring_objs[i].triggers, "TriggeredRate_" + to_string(i), metrics::RATE);
        registerVariable(m_monitoring_objs[i].time_read, "time_RetrieveEvents_" + to_string(i));
        registerVariable(m_monitoring_objs[i].time_parse, "time_ParseEvents_" + to_string(i));
        registerVariable(m_monitoring_objs[i].time_overhead, "time_Overhead_" + to_string(i));
        registerVariable(m_monitoring_objs[i].corrupted_events, "CorruptedEvents_" + to_string(i));
        registerVariable(m_monitoring_objs[i].empty_events, "EmptyEvents_" + to_string(i));

        // for every channels
        for (uint8_t chan = 0; chan < 10; chan++) {
            registerVariable(m_monitoring_objs[i].pedestal[chan], "pedestal_ch0" + to_string(chan) + "_" + to_string(i));
            registerVariable(m_monitoring_objs[i].temps[chan], "temp_ch0" + to_string(chan) + "_" + to_string(i));
        }
        for (uint8_t chan = 10; chan < NCHANNELS; chan++) {
            registerVariable(m_monitoring_objs[i].pedestal[chan], "pedestal_ch" + to_string(chan) + "_" + to_string(i));
            registerVariable(m_monitoring_objs[i].temps[chan], "temp_ch" + to_string(chan) + "_" + to_string(i));
        }
    }

    registerVariable(m_info_udp_receive_timeout_counter, "info_udp_receive_timeout_counter");
    registerVariable(m_info_wrong_cmd_ack_counter, "info_wrong_cmd_ack_counter");
    registerVariable(m_info_wrong_received_nof_bytes_counter, "info_wrong_received_nof_bytes_counter");
    registerVariable(m_info_wrong_received_packet_id_counter, "info_wrong_received_packet_id_counter");

    registerVariable(m_info_clear_UdpReceiveBuffer_counter, "info_clear_UdpReceiveBuffer_counter");
    registerVariable(m_info_read_dma_packet_reorder_counter, "info_read_dma_packet_reorder_counter");

    registerVariable(m_udp_single_read_receive_ack_retry_counter, "udp_single_read_receive_ack_retry_counter");
    registerVariable(m_udp_single_read_req_retry_counter, "udp_single_read_req_retry_counter");

    registerVariable(m_udp_single_write_receive_ack_retry_counter, "udp_single_write_receive_ack_retry_counter");
    registerVariable(m_udp_single_write_req_retry_counter, "udp_single_write_req_retry_counter");

    registerVariable(m_udp_dma_read_receive_ack_retry_counter, "udp_dma_read_receive_ack_retry_counter");
    registerVariable(m_udp_dma_read_req_retry_counter, "udp_dma_read_req_retry_counter");

    registerVariable(m_udp_dma_write_receive_ack_retry_counter, "udp_dma_write_receive_ack_retry_counter");
    registerVariable(m_udp_dma_write_req_retry_counter, "udp_dma_write_req_retry_counter");

    m_loss_of_lock = 1;
    registerVariable(m_loss_of_lock, "FClock_lock_status");
    if (m_bobr) {
        registerVariable(m_bobr_statusword, "BOBR_status_word");
        registerVariable(m_bobr_timing, "BOBR_timing_status");
        registerVariable(m_lhc_turncount, "LHC_turncount");
        registerVariable(m_lhc_fillnumber, "LHC_fillnumber");
        registerVariable(m_lhc_machinemode, "LHC_machinemode");
        registerVariable(m_lhc_beamenergy, "LHC_beamenergy");
        registerVariable(m_lhc_intensity1, "LHC_intensity1");
        registerVariable(m_lhc_intensity2, "LHC_intensity2");
        registerVariable(m_lhc_frequency, "LHC_frequency", metrics::LAST_VALUE, false);  // We don't reset this metric between runs
        m_lhc_frequency = 40.079;
    }

    // configuration of hardware
    INFO("Configuring ...");
    m_digiHand->configure();

    for (size_t i = 0; i < m_n_digitizers; i++) {
        for (uint8_t chan = 0; chan < NCHANNELS; chan++)
            m_monitoring_objs[i].pedestal[chan] = m_digiHand->m_digitizers[i]->m_pedestal[chan];  // these are pedestals used to calculate thresholds
    }

    INFO("Finished configuring - the settings of the digitizers are :");
    // dumping config for both digitizers
    m_digiHand->dump_config();
    if (m_bobr) {
        if (!readBOBR()) {
            ERROR("Failed to read BOBR data at configure");
            m_status = STATUS_ERROR;
        }
        usleep(500000);
        readBOBR();  // Second read to calculate meaningfull LHC frequency
    }
    INFO("End of configure()");
}

void DigitizerReceiverModule::start(unsigned int run_num) {
    INFO("Starting ...");

    // register the metrics for monitoring data
    INFO("Initializing monitoring metrics");
    m_sw_count = 0;
    
    // starting of acquisition in hardware
    m_trigger_disabled=0;
    m_digiHand->start_acquisition();

    FaserProcess::start(run_num);
}

void DigitizerReceiverModule::enableTrigger(const std::string &arg) {
  INFO("Got enableTrigger command with argument "<<arg);
  m_trigger_disabled=0;
}

void DigitizerReceiverModule::disableTrigger(const std::string &arg) {
  INFO("Got disableTrigger command with argument "<<arg);
  m_trigger_disabled=1;
}

void DigitizerReceiverModule::stop() {
    std::this_thread::sleep_for(std::chrono::microseconds(100000));  // wait for events
    FaserProcess::stop();
    INFO("Stopping ...");
    m_digiHand->stop_acquisition();
}

void DigitizerReceiverModule::sendECR() {
    // should send ECR to electronics here. In case of failure, set m_status to STATUS_ERROR
    DEBUG("Sending ECR");

    // ensure no data readout going on in parallel
    m_lock.lock();

    // stop acquisition
    m_digiHand->stop_acquisition();

    // restart acquisition to reset event counter
    m_digiHand->start_acquisition();
    for (auto &pei : m_prev_event_ids)
      pei = static_cast<unsigned int>(m_ECRcount) << 24;
    // release the lock
    m_lock.unlock();
}

static unsigned int BOBRWord(unsigned int *data, int address, int len = 4) {
    unsigned int result = 0;
    for (int ii = 0; ii < len; ii++) {
        result |= (data[address + ii] & 0xFF) << (8 * ii);
    }
    return result;
}

bool DigitizerReceiverModule::readBOBR() {
    // BOBR readout - should be moved to digitizer-readout code?
    unsigned int vme_base_address = 0x00B00000;
    m_bobrdata.m_header = BOBRDataFormat::BOBR_HEADER_V1;

    sis3153eth *vme_crate = m_digiHand->m_digitizers[0]->m_crate;
    unsigned int data = 0;

    int rc = vme_crate->udp_sis3153_register_read(SIS3153USB_LEMO_IO_CTRL_REG, &data);
    if (rc != 0) {
        WARNING("Failed to read clock status, status code: " << std::hex << rc << std::dec);
    }
    m_loss_of_lock = (data & (1 << 20)) != 0;  // on input 1

    unsigned int addr = vme_base_address + 0x10;
    int return_code = vme_crate->vme_A24D32_read(addr, &data);
    if (return_code != 0) {
        INFO("Failed to read BOBR status code: " << std::hex << return_code << std::dec);
        return false;
    }
    m_bobrdata.m_status = (data & 0xFF00) | static_cast<unsigned int>(m_loss_of_lock);
    m_bobr_statusword = data & 0xFFFF;
    m_bobr_timing = (data & 0x0F00) == 0x0F00;

    // request update of data
    unsigned int statusControl = data;
    DEBUG("BOBR status: 0x" << std::hex << data);
    statusControl &= ~(1u << 7);
    return_code = vme_crate->vme_A24D32_write(addr, statusControl);
    if (return_code != 0) {
        INFO("Failed to write BOBR control code: " << std::hex << return_code << std::dec);
        return false;
    }

    // FIXME: this is inefficient way of reading out BOBR data
    addr = vme_base_address + 0xC00;
    UINT addrs[40];
    UINT datawords[40];
    for (size_t word = 0; word < 40; word++) {
        addr = vme_base_address + 0xC00 + word * 4;
        addrs[word] = addr;
        datawords[word] = 0xFFFFFFFF;
    }

    return_code = vme_crate->vme_A24D32_sgl_random_burst_read(40, addrs, datawords);

    if (return_code != 0) {
        INFO("Failed to read BOBR data: " << std::hex << return_code << std::dec);
        return false;
    }
    unsigned int microseconds = BOBRWord(datawords, 0);
    unsigned int seconds = BOBRWord(datawords, 4);
    unsigned int turncount = BOBRWord(datawords, 18);
    unsigned int lhc_fillnumber = BOBRWord(datawords, 22);
    unsigned int lhc_machinemode = BOBRWord(datawords, 26, 2);
    unsigned int lhc_momentum = BOBRWord(datawords, 30, 2);
    unsigned int lhc_intensity1 = BOBRWord(datawords, 32, 2);
    unsigned int lhc_intensity2 = BOBRWord(datawords, 36, 2);
    m_lhc_turncount = static_cast<int>(turncount);
    m_lhc_fillnumber = static_cast<int>(lhc_fillnumber);
    m_lhc_machinemode = static_cast<int>(lhc_machinemode);
    m_lhc_beamenergy = lhc_momentum * 0.12;
    m_lhc_intensity1 = static_cast<int>(lhc_intensity1);
    m_lhc_intensity2 = static_cast<int>(lhc_intensity2);

    m_bobrdata.m_gpstime_seconds = seconds;
    m_bobrdata.m_gpstime_useconds = microseconds;
    m_bobrdata.m_turncount = turncount;
    m_bobrdata.m_fillnumber = lhc_fillnumber;
    m_bobrdata.m_machinemode = lhc_machinemode;
    m_bobrdata.m_beam_momentum = lhc_momentum;
    m_bobrdata.m_beam1_intensity = lhc_intensity1;
    m_bobrdata.m_beam2_intensity = lhc_intensity2;

    if (m_prev_seconds != 0) {
        size_t deltaT = (seconds - m_prev_seconds) * 1000000 + (microseconds - m_prev_microseconds);
        size_t deltaTurn = turncount - m_prev_turncount;
        float freq = 3564.0 * deltaTurn / deltaT;
	if (turncount>m_prev_turncount)
	  m_lhc_frequency = freq;
    }
    m_prev_seconds = seconds;
    m_prev_microseconds = microseconds;
    m_prev_turncount = turncount;

    addr = vme_base_address + 0x10;
    statusControl |= 1 << 2 | 1 << 7;
    return_code = vme_crate->vme_A24D32_write(addr, statusControl);
    if (return_code != 0) {
        INFO("Failed to reset BOBR control code: " << std::hex << return_code << std::dec);
        return false;
    }

    return true;
}

void DigitizerReceiverModule::runner() noexcept {
    INFO("Running...");
    size_t readout_blt = getModuleSettings()["readout"]["readout_blt"].get<size_t>();
    std::vector<size_t> eventSizes = m_digiHand->get_event_sizes();
    std::vector<READOUT> readout_v(m_n_digitizers);
    for (size_t i = 0; i < m_n_digitizers; i++) {
        readout_v[i].eventSize = eventSizes[i];
        // creating the payload array with the maximum size a payload can have : size of one event * the number of events we want to read.
        // memory freed when READOUT destructor is called
        readout_v[i].raw_payload = new UINT[readout_blt * eventSizes[i]];
    }

    m_prev_event_ids.resize(m_n_digitizers);
    std::fill(m_prev_event_ids.begin(), m_prev_event_ids.end(), 0);
    for (size_t i = 0; i < m_n_digitizers; i++) {
        INFO("Digitizer " << i << ": Expected event size: " << readout_v[i].eventSize << " words");
    }

    // start time
    auto time_start = chrono::high_resolution_clock::now();
    int last_check_count = 0;

    // for BOBR
    m_prev_seconds = 0;
    m_prev_microseconds = 0;
    m_prev_turncount = 0;

    if (m_bobr && !readBOBR()) {
        ERROR("Failed to read BOBR data at start");
        m_status = STATUS_ERROR;
    }
    uint32_t bobr_id = 0;
    // the polling loop
    while (m_run) {
        if (m_trigger_disabled) { // when we pause a run stop polling the VME bus as it interferes with Formosa
	  usleep(50000);
	  if (m_trigger_disabled>20)   // wait 1s before actually pausing readout to be really sure the TLB has stopped accepting events
	    continue;
	  //complicated logic to avoid keep counting if counter has been reset in the mean time - could also use lock
	  int waitcnt=m_trigger_disabled;
	  int nextcnt=waitcnt+1;
	  if (waitcnt!=0)
	    m_trigger_disabled.compare_exchange_strong(waitcnt,nextcnt);
	}
        // send software triggers for development if enabled
        // these are sent with a pause after the sending to have a rate limit
        if (m_software_trigger_rate) {
            m_sw_count++;
            INFO("Software trigger sending trigger number : " << m_sw_count);
            m_digiHand->send_sw_trigger();
            usleep((1.0 / m_software_trigger_rate) * 1000000);
        }

        // lock to prevent accidental double reading with the sendECR() call
        m_lock.lock();

        // polling of the hardware - if any number of events is detected
        // then all events in the buffer are read out
        std::vector<size_t> occupancies(m_n_digitizers);
        size_t occ = 0;

        for (size_t i = 0; i < m_n_digitizers; i++) {
            try {
                occ = m_digiHand->dump_event_count(false, i);
            } catch (DigitizerHardwareException &e) {
                static int numErrors = 100;
                ERROR("Failed to read number of events: " << e.what());
                m_status = STATUS_WARN;
                if (numErrors-- == 0) {
		  m_status = STATUS_ERROR;
		  ERROR("Too many read errors - bailing out");
		  //throw e;
		  m_run=false;
                }
            }

            // how much space is left in the buffer (number of buffers 1024 is hardcoded, but should change only if nb of samples for event > 640)
            m_monitoring_objs[i].hw_buffer_space = 1024 - occ;
            m_monitoring_objs[i].hw_buffer_occupancy = occ;  // for monitoring
            occupancies[i] = occ;                            // will be decremented when reading the events from the buffers
        }

        bool emptyBuffers = std::all_of(occupancies.begin(), occupancies.end(), [](int i) { return i == 0; });

        bool shouldsleep = (emptyBuffers == true);
        while (!emptyBuffers) {
            // DEBUG("[Running] - Reading events : totalEvents = " << std::dec << n_events_present << "  eventsRequested = " << std::dec << m_n_events_requested);
            DEBUG("With m_ECRcount : " << m_ECRcount);

            // clear the monitoring map which will retrieve the info to pass to monitoring metrics

            // get the data from the board into the software buffer
            // int nwords_obtained = 0;
            // int nerrors = 0;
            for (size_t d_id = 0; d_id < m_n_digitizers; d_id++) {
                if (occupancies[d_id] == 0)
                    continue;

                size_t n_events_to_do = std::min(occupancies[d_id], readout_blt);
                READOUT &r = readout_v[d_id];
                MonitoringValuesHandler &mon = m_monitoring_objs[d_id];

                m_digiHand->read_single_event(d_id, r, n_events_to_do, false);
                mon.intermediate_read_time += r.monitoring["block_readout_time"];
                mon.intermediate_receivedEvents += n_events_to_do;

                if (r.nwordsObtained == 0) {
                    mon.empty_events++;
                    if (mon.empty_events == 5)
                        ERROR("Failed to retrieve at least 5 events - network connection unstable?");

                    if (mon.empty_events >= 50) {
                        ERROR("Failed to retrieve at least 50 events - problem communicating with digitizer - Call Brian...");
                        m_status = STATUS_ERROR;
                    }

                    continue;
                }

                if ((r.nwordsObtained != r.eventSize * n_events_to_do) && (r.nerrors == 0)) {
                    WARNING("Got " << r.nwordsObtained << " words while expecting " << r.eventSize * n_events_to_do << " words, but no errors?");
                    r.nerrors = 1;
                }

                // count triggers sent
                mon.triggers += n_events_to_do;

                for (size_t i_evnt = 0; i_evnt < n_events_to_do; i_evnt++) {
                    auto fragment = m_digiHand->m_digitizers[d_id]->ParseEventSingle(r.raw_payload + i_evnt * r.eventSize, r.eventSize, r.monitoring, m_ECRcount, m_ttt_converter, m_bcid_ttt_fix, r.nerrors, false, d_id);

                    mon.intermediate_parse_time += r.monitoring["time_parse_time"];

                    // send vent to event builder
                    if (fragment->event_id() != m_prev_event_ids[d_id] + 1)
                        WARNING("Got fragment " << fragment->event_id() << " was expecting: " << m_prev_event_ids[d_id] + 1);

                    m_prev_event_ids[d_id] = fragment->event_id();
                    if (fragment->status()) {
                        mon.corrupted_events++;
                        if (mon.corrupted_events == 5) {
                            ERROR("Got several corrupted events");
                            m_status = STATUS_WARN;
                        }
                        if (mon.corrupted_events >= 50) {
                            ERROR("Got at least 50 events - problem communicating with digitizer - Call Brian...");
                            m_status = STATUS_ERROR;
                        }
                    }

                    // place the raw binary event fragment on the output port
                    std::unique_ptr<const byteVector> bytestream(fragment->raw());
                    DataFragment<daqling::utilities::Binary> binData(bytestream->data(), bytestream->size());
                    m_connections.send(d_id, binData);
                }
                occupancies[d_id] -= n_events_to_do;
            }  // end for loop over digitizers
            emptyBuffers = std::all_of(occupancies.begin(), occupancies.end(), [](int i) { return i == 0; });
        }  // while buffer not empty
        m_lock.unlock();

        // sleep for 20 milliseconds. This time is chosen to ensure that the polling
        // happens at a rate where the buffer doesn't have time to fill up
        if (shouldsleep) {
            usleep(20000);
        } else {
            for (auto &mon : m_monitoring_objs) {
                if (mon.intermediate_receivedEvents > 0) {
                    mon.time_read = mon.intermediate_read_time / mon.intermediate_receivedEvents;
                    mon.time_parse = mon.intermediate_parse_time / mon.intermediate_receivedEvents;
                }
            }
        }
        m_info_udp_receive_timeout_counter = static_cast<int>(m_digiHand->m_digitizers[0]->m_crate->info_udp_receive_timeout_counter);

        m_info_wrong_cmd_ack_counter = static_cast<int>(m_digiHand->m_digitizers[0]->m_crate->info_wrong_cmd_ack_counter);
	m_info_wrong_received_nof_bytes_counter = static_cast<int>(m_digiHand->m_digitizers[0]->m_crate->info_wrong_received_nof_bytes_counter);
	m_info_wrong_received_packet_id_counter = static_cast<int>(m_digiHand->m_digitizers[0]->m_crate->info_wrong_received_packet_id_counter);

	m_info_clear_UdpReceiveBuffer_counter = static_cast<int>(m_digiHand->m_digitizers[0]->m_crate->info_clear_UdpReceiveBuffer_counter);
	m_info_read_dma_packet_reorder_counter = static_cast<int>(m_digiHand->m_digitizers[0]->m_crate->info_read_dma_packet_reorder_counter);

        m_udp_single_read_receive_ack_retry_counter = m_digiHand->m_digitizers[0]->m_crate->udp_single_read_receive_ack_retry_counter;
        m_udp_single_read_req_retry_counter = m_digiHand->m_digitizers[0]->m_crate->udp_single_read_req_retry_counter;

        m_udp_single_write_receive_ack_retry_counter = m_digiHand->m_digitizers[0]->m_crate->udp_single_write_receive_ack_retry_counter;
        m_udp_single_write_req_retry_counter = m_digiHand->m_digitizers[0]->m_crate->udp_single_write_req_retry_counter;

        m_udp_dma_read_receive_ack_retry_counter = m_digiHand->m_digitizers[0]->m_crate->udp_dma_read_receive_ack_retry_counter;
        m_udp_dma_read_req_retry_counter = m_digiHand->m_digitizers[0]->m_crate->udp_dma_read_req_retry_counter;

        m_udp_dma_write_receive_ack_retry_counter = m_digiHand->m_digitizers[0]->m_crate->udp_dma_write_receive_ack_retry_counter;
        m_udp_dma_write_req_retry_counter = m_digiHand->m_digitizers[0]->m_crate->udp_dma_write_req_retry_counter;

        // only get the temperatures once per second to cut down on the VME data rate
        // amount of time in loop in milliseconds
        int time_now = (chrono::duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now() - time_start).count() * 1e-6) / 1000;
        if (time_now > last_check_count) {
            // update where you are in time counting
            last_check_count = time_now;

            // monitoring of temperature on ADCs
            for (size_t i = 0; i < m_n_digitizers; i++) {
                std::vector<int> adc_temp;
                try {
                    adc_temp = m_digiHand->m_digitizers[i]->GetADCTemperature(false);
                } catch (DigitizerHardwareException &e) {
                    ERROR("Failed to read ADC temperatures");
                }
                if (adc_temp.size() == NCHANNELS) {
                    for (size_t j = 0; j < adc_temp.size(); j++) {
                        m_monitoring_objs[i].temps[j] = adc_temp[j];
                    }
                } else {
                    ERROR("Temperature monitoring picked up incorrect number of channels : " << NCHANNELS);
                }
            }

            if (m_bobr) {
                if (!readBOBR()) {
                    ERROR("Failed to read BOBR data");
                } else {
                    std::unique_ptr<EventFragment> fragment(new EventFragment(EventTags::MonitoringTag,
                                                                              SourceIDs::BOBRSourceID,
                                                                              bobr_id++, 0, &m_bobrdata, sizeof(m_bobrdata)));
                    std::unique_ptr<const byteVector> bytestream(fragment->raw());
                    DataFragment<daqling::utilities::Binary> binData(bytestream->data(), bytestream->size());
                    m_connections.send(0, binData);
                }
            }
        }
    }

    INFO("Runner stopped");
}
